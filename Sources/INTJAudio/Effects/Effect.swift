//
//  Effect.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

/// A protocol for objects that can process audio in-place

public protocol Effect: AnyObject
{
    /// The name to display to users
    
    var displayName: String { get }
    
    /// Will the effect affect the audio being passed to it?
    
    var isEnabled: Bool { get set }
    
    /// Prepares the resources necessary for playback
    ///
    /// - Parameters:
    ///   - dimensions: The expected number of frames and channels that will be requested to render.

    func prepare(for dimensions: AudioDimensions) throws

    /// Asks the receiver to modify the audio contents in the provided buffer
    ///
    /// - Parameters:
    ///   - buffer: The buffer to render the audio contents into
    ///   - position: The start of the buffer represents the audio at this sample frame

    func process(_ audio: [AudioBuffer], for: SamplePosition)
}
