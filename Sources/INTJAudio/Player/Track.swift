//
//  Track.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

public protocol TrackProtocol
{
    func prepare(for dimensions: AudioDimensions) throws
    func render(into output: [AudioBuffer], for position: SamplePosition)
}

/// An object that references portions of `Source`s  for playback.
///
/// `Track`s also can contain `Effect` objects that process the audio after it has been rendered from the `Source`s
/// `Track`s

public final class Track: TrackProtocol
{
    /// `Track` objects that render into the same `AudioBuffer` and share the same `Effect` processes.
    ///
    /// An example would be a Master track that combines many tracks together and adds a reverb `Effect` for all.
    /// Subtracks are rendered *after* (and possibly replacing) the receiver's own `Source`s.
    
    public var subtracks = [TrackProtocol]()
    
    /// Objects that process the audio in-place
    ///
    /// `Effect` processes are applied after all `Source`s and subtracks have been rendered.
    
    public var effects = [Effect]()
    
    /// Will the track process and output audio?
    
    public var isEnabled = true

    private var fragments = [SourceFragment]()
    private var buffer = [AudioBuffer]()
    
    public init() { }
    
    /// Adds a `Source` to the receiver for playback
    ///
    /// The entire `Source` content is referenced from beginning to end.
    ///
    /// - Parameters:
    ///   - source: The `Source` to be played on this track.
    ///   - position: The position to begin playback of the `Source` on the `Track`.
    
    public func addSource(_ source: SourceProtocol, at position: SamplePosition = 0)
    {
        addRange(0 ..< source.end, of: source, at: position)
    }
    
    /// Adds a portion of a `Source` to the receiver for playback
    ///
    /// - Parameters:
    ///   - range: The range of the `Source` to reference for playback.
    ///   - source: The `Source` to be played on this track.
    ///   - location: The location to begin playback of the portion of the `Source` on the `Track`.
    
    public func addRange(_ range: TimeRange, of source: SourceProtocol, at location: TimeLocation)
    {
        addRange(SampleRange(range: range),
                 of: source,
                 at: SamplePosition(location: location))
    }
    
    /// Adds a portion of a `Source` to the receiver for playback
    ///
    /// - Parameters:
    ///   - range: The range of the `Source` to reference for playback.
    ///   - source: The `Source` to be played on this track.
    ///   - position: The position to begin playback of the portion of the `Source` on the `Track`.
    
    public func addRange(_ range: SampleRange, of source: SourceProtocol, at position: SamplePosition)
    {
        fragments.append(SourceFragment(trackRange: position ..< position.advanced(by: range.count),
                                        source: source,
                                        sourceStart: range.start))
    }
    
    //
    // MARK: -
    //
    
    /// Prepares the resources necessary for playback
    ///
    /// - Parameters:
    ///   - size: The expected number of frames that will be requested to render.
    ///   - channels: The number of channels that will be requested to render.

    public func prepare(for dimensions: AudioDimensions) throws
    {
        buffer = [AudioBuffer](with: dimensions)

        fragments.forEach { $0.prepare(for: dimensions) }
        try subtracks.forEach { try $0.prepare(for: dimensions) }
        try effects.forEach { try $0.prepare(for: dimensions) }
    }
    
    /// Asks the receiver to render the audio contents into the provided buffer
    ///
    /// - Parameters:
    ///   - buffer: The buffer to render the audio contents into
    ///   - position: The start of the buffer represents the audio at this sample frame

    public func render(into output: [AudioBuffer], for position: SamplePosition)
    {
        guard isEnabled else { return }
        
        buffer.forEach { $0.clear() }
        
        fragments.forEach { $0.render(into: buffer, for: position) }
        subtracks.forEach { $0.render(into: buffer, for: position) }
        effects.forEach { $0.process(buffer, for: position) }
        
        for (trackChannel, outputChannel) in zip(buffer, output)
        {
            outputChannel.copy(from: trackChannel, mix: true)
        }
    }
}
