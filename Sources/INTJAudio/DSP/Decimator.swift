//
//  Decimator.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/23/24.
//

import Accelerate
import Foundation

public class Decimator
{
    public enum Density: String, Codable
    {
        case oneOctave
        case thirdOctave
        case sixthOctave
        case twelfthOctave
        case twentyFourthOctave
        case fourtyEighthOctave
        case ISO15
        case ISO31
        case critical
        case view
    }

    public let frequencies: [Hertz]

    private let density: Density
    private var calculating = [Float]()

    public init(density: Density,
                count: Int?,
                samplingRate: Hertz = .samplingRate)
    {
        self.density = density

        //
        // Calculate frequencies
        //

        let P = Semitone(-36.0) // ~1Hz via MIDINote value
        let L = log(Double(samplingRate.nyquist) / Double(Hertz(P)))
        var N: Int
        var BF: ((Int) -> Hertz)

        switch density
        {
        case .oneOctave:
            N = Int(round(L / log(2)))
            BF = { Hertz(P.advanced(by: Cent($0 * 1200))) }
        case .thirdOctave:
            N = Int(round(L / log(pow(2.0, 1.0 / 3.0))))
            BF = { Hertz(P.advanced(by: Cent($0 * 400))) }
        case .sixthOctave:
            N = Int(round(L / log(pow(2.0, 1.0 / 6.0))))
            BF = { Hertz(P.advanced(by: Cent($0 * 200))) }
        case .twelfthOctave:
            N = Int(round(L / log(pow(2.0, 1.0 / 12.0))))
            BF = { Hertz(P.advanced(by: Cent($0 * 100))) }
        case .twentyFourthOctave:
            N = Int(round(L / log(pow(2.0, 1.0 / 24.0))))
            BF = { Hertz(P.advanced(by: Cent($0 * 50))) }
        case .fourtyEighthOctave:
            N = Int(round(L / log(pow(2.0, 1.0 / 48.0))))
            BF = { Hertz(P.advanced(by: Cent($0 * 25))) }
        case .ISO15:
            N = ISO15Frequencies.count
            BF = { ISO15Frequencies[$0] }
        case .ISO31:
            N = ISO31Frequencies.count
            BF = { ISO31Frequencies[$0] }
        case .critical:
            N = CriticalFrequencies.count
            BF = { CriticalFrequencies[$0] }
        case .view:
            N = count ?? 0
            BF =
            {
                guard let count, count > 0 else { return 0 }
                
                let n = Double($0) / Double(count)
                let r = n * (log10(24000) - log10(20)) + log10(20)
                return Hertz(pow(10, r))
            }
        }

        var fArray = [Hertz](repeating: 0, count: N)
        for bin in 0 ..< N
        {
            fArray[bin] = BF(bin)
        }
        frequencies = fArray
        
        calculating = [Float](repeating: 0, count: N)
    }

    public func calculate(_ buffer: PolarBuffer) -> [Float]
    {
        for i in frequencies.indices
        {
            calculating[i] = buffer.magnitude(at: frequencies[i])
        }

        return calculating
    }
}
