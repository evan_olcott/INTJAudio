// swift-tools-version: 6.0

import PackageDescription

let package = Package(
    name: "INTJAudio",
    platforms: [
        .macOS(.v14),
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "INTJAudio",
            targets: ["INTJAudio"])
    ],
    dependencies: [
        .package(
            url: "https://github.com/apple/swift-collections.git",
            .upToNextMajor(from: "1.1.0")
        )
    ],
    targets: [
        .target(
            name: "INTJAudio",
            dependencies: [
                .product(name: "Collections", package: "swift-collections")
            ]
        ),
        .testTarget(
            name: "INTJAudioTests",
            dependencies: ["INTJAudio"])
    ]
)
