//
//  UnsafeMutableAudioBufferListPointer+INTJAudioTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
import AVFoundation
@testable import INTJAudio
import XCTest

class UnsafeMutableAudioBufferListPointer_INTJAudioTests: XCTestCase
{
    func testUnsafeMutableAudioBufferListPointerConvenienceDimensions()
    {
        let list = AudioBufferList.allocate(maximumBuffers: 4)

        list[0] = AudioBuffer(mNumberChannels: 2,
                              mDataByteSize: 1024,
                              mData: UnsafeMutableRawPointer.allocate(byteCount: 1024, alignment: 0))
        list[1] = AudioBuffer(mNumberChannels: 4,
                              mDataByteSize: 2048,
                              mData: UnsafeMutableRawPointer.allocate(byteCount: 2048, alignment: 0))
        list[2] = AudioBuffer(mNumberChannels: 3,
                              mDataByteSize: 1536,
                              mData: UnsafeMutableRawPointer.allocate(byteCount: 1536, alignment: 0))
        list[3] = AudioBuffer(mNumberChannels: 1,
                              mDataByteSize: 512,
                              mData: UnsafeMutableRawPointer.allocate(byteCount: 512, alignment: 0))

        XCTAssertEqual(list.channelCount, 10)
        XCTAssertEqual(list.frameCount, 128)
    }

    func testUnsafeMutableAudioBufferListPointerConvenienceWrite()
    {
        let bufferList = AudioBufferList.allocate(maximumBuffers: 1)
        bufferList[0] = AudioBuffer(mNumberChannels: 2,
                                    mDataByteSize: 2048,
                                    mData: UnsafeMutableRawPointer.allocate(byteCount: 2048, alignment: 0))
        let channels = [
            AudioBuffer(count: 256),
            AudioBuffer(count: 256)
        ]

        var one: Float = 1
        vDSP_vfill(&one, channels[0].pointer, 1, vDSP_Length(256))
        var two: Float = 2
        vDSP_vfill(&two, channels[1].pointer, 1, vDSP_Length(256))

        bufferList.copy(from: channels)

        let interleaved = bufferList[0].mData?.assumingMemoryBound(to: Float.self)

        XCTAssertEqual(interleaved?[0], 1)
        XCTAssertEqual(interleaved?[1], 2)
        XCTAssertEqual(interleaved?[2], 1)
        XCTAssertEqual(interleaved?[3], 2)
        XCTAssertEqual(interleaved?[254], 1)
        XCTAssertEqual(interleaved?[255], 2)

        bufferList.clear()

        XCTAssertEqual(interleaved?[0], 0)
        XCTAssertEqual(interleaved?[1], 0)
        XCTAssertEqual(interleaved?[254], 0)
        XCTAssertEqual(interleaved?[255], 0)
    }
}
