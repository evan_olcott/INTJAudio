//
//  AudioInputtable.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import Foundation

public protocol AudioInputtable: StreamProcessor
{
    var audioInputBuses: [AudioInputBus] { get }
    
    /// The `AudioInputBus` that will be fed audio buffers directly from the `Device`.
    /// Return `nil` if the processor should not receive buffers from the `Device`, but from other `AudioConnection`s.
    
    var audioInputHeadBus: AudioInputBus? { get }
    
    func prepareForAudioInput(with: AudioDimensions, state: StreamState)

    func handleAudioInput(_: [AudioBuffer], from: AudioInputBus, state: StreamState)
}

public extension AudioInputtable
{
    var audioInputHeadBus: AudioInputBus? { nil }
    
    func prepareForAudioInput(with: AudioDimensions, state: StreamState) { }

    func handleAudioInput(_: [AudioBuffer], from: AudioInputBus, state: StreamState) { }
}
