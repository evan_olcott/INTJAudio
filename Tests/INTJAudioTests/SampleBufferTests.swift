//
//  AudioBufferTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
@testable import INTJAudio
import XCTest

class AudioBufferTests: XCTestCase
{
    var buffer: AudioBuffer?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        buffer = AudioBuffer(count: 2048)
        
        let buffer = try XCTUnwrap(buffer)
        
        vDSP.fill(&buffer.rawValue, with: 0.5)
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        buffer = nil
    }
    
    func testInit()
    {
        let ptr = UnsafeMutableBufferPointer<Float>.allocate(capacity: 2048)
        ptr.initialize(repeating: 0.5)
        let other = AudioBuffer(rawValue: ptr)
        
        XCTAssertEqual(other.sampleCount, 2048)
        XCTAssertEqual(other[0], 0.5)
        XCTAssertEqual(other.last, 0.5)
    }
    
    func testInitWithLength()
    {
        let other = AudioBuffer(length: 2.75)
        
        XCTAssertEqual(other.sampleCount, 132000)
    }
    
    func testInitWithData()
    {
        let other = AudioBuffer(Data(repeating: 0x3C, count: 16))
        
        XCTAssertEqual(other.sampleCount, 4)
        XCTAssertEqual(other[0], 0.01148, accuracy: 0.00001)
        XCTAssertEqual(other[3], 0.01148, accuracy: 0.00001)
    }
    
    func testContent() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        XCTAssertNotNil(buffer)
        XCTAssertEqual(buffer.sampleCount, 2048)
        
        XCTAssertEqual(buffer[0], 0.5)
        XCTAssertEqual(buffer[2047], 0.5)
    }
    
    func testCompare() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        let otherChannel = AudioBuffer(buffer)
        
        buffer.clear()
        XCTAssertEqual(buffer[2047], 0)
        XCTAssertNotEqual(otherChannel[2047], 0)
    }
    
    func testClearRange() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        buffer.clear(512 ..< 1536)
        
        XCTAssertNotEqual(buffer[511], 0)
        XCTAssertEqual(buffer[512], 0)
        XCTAssertEqual(buffer[1535], 0)
        XCTAssertNotEqual(buffer[1536], 0)
    }
    
    func testIsEmpty() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        XCTAssertFalse(buffer.isEmpty)
        buffer.clear()
        XCTAssertFalse(buffer.isEmpty)
        
        let empty = AudioBuffer(count: 0)
        XCTAssertTrue(empty.isEmpty)
    }
    
    func testCopy() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        let otherChannel = AudioBuffer(count: buffer.sampleCount)
        
        otherChannel.copy(from: buffer,
                          range: 100 ..< 200,
                          to: 50)
        
        XCTAssertEqual(otherChannel[49], 0)
        XCTAssertNotEqual(otherChannel[50], 0)
        XCTAssertNotEqual(otherChannel[149], 0)
        XCTAssertEqual(otherChannel[150], 0)
        
        otherChannel.copy(from: buffer,
                          range: -10 ..< 10,
                          to: -10)
        
        XCTAssertNotEqual(otherChannel[9], 0)
        XCTAssertEqual(otherChannel[10], 0)
        
        otherChannel.copy(from: buffer,
                          range: 100 ..< 200,
                          to: 200,
                          mix: false,
                          scaleWith: Ramp(scalar: 2))
        
        XCTAssertEqual(otherChannel[199], 0)
        XCTAssertEqual(otherChannel[200], 1)
    }
    
    func testMove() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        buffer.move(1024 ..< 2048, to: 0)
        
        XCTAssertEqual(buffer[0], 0.5)
        XCTAssertEqual(buffer[1023], 0.5)
        XCTAssertEqual(buffer[1024], 0)
        XCTAssertEqual(buffer[2047], 0)
    }
    
    func testScale() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        buffer.scale(by: Scalar(2), range: 0 ..< 1024)
        
        XCTAssertEqual(buffer[0], 1)
        XCTAssertEqual(buffer[1023], 1)
        XCTAssertEqual(buffer[1024], 0.5)
        XCTAssertEqual(buffer[2047], 0.5)
        
        buffer.scale(by: Decibel(6), range: 1024 ..< 2048)
        
        XCTAssertEqual(buffer[1024], 1, accuracy: 0.01)
        XCTAssertEqual(buffer[2047], 1, accuracy: 0.01)
    }
    
    func testRampLinearScale() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        buffer.scale(with: Ramp(start: 0, end: 2), range: 0 ..< 1024)
        buffer.scale(with: Ramp(start: 2, end: 1), range: 1024 ..< 2047)
        
        XCTAssertEqual(buffer[0], 0)
        XCTAssertEqual(buffer[512], 0.5)
        XCTAssertEqual(buffer[1024], 1)
        XCTAssertEqual(buffer[2047], 0.5)
    }
    
    func testRampPowerScale() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        buffer.scale(with: Ramp(start: 0, end: 1, shape: .power), range: 0 ..< 1024)
        buffer.scale(with: Ramp(start: 1, end: 0, shape: .power), range: 1024 ..< 2048)
        
        XCTAssertEqual(buffer[0], 0)
        XCTAssertEqual(buffer[512], 0.35355338)
        XCTAssertEqual(buffer[1024], 0.5)
        XCTAssertEqual(buffer[1536], 0.35355338)
        XCTAssertEqual(buffer[2047], 0.015625)
    }
    
    func testMeasurements() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        XCTAssertEqual(buffer.maximum, 0.5)
        XCTAssertEqual(buffer.minimum, 0.5)
        XCTAssertEqual(buffer.magnitude, 0.5)
        XCTAssertEqual(buffer.rms, 0.5)
        XCTAssertEqual(buffer.average, 0.5)
        XCTAssertEqual(buffer.energy, 512)
        
        XCTAssertEqual(buffer.maximum(of: 0 ..< 256), 0.5)
        XCTAssertEqual(buffer.minimum(of: 0 ..< 256), 0.5)
        XCTAssertEqual(buffer.magnitude(of: 0 ..< 256), 0.5)
        XCTAssertEqual(buffer.rms(of: 0 ..< 256), 0.5)
        XCTAssertEqual(buffer.average(of: 0 ..< 256), 0.5)
        XCTAssertEqual(buffer.energy(of: 0 ..< 256), 64)
    }
    
    func testReadFromFile() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        let tempDirectory = FileManager.default.temporaryDirectory
        let filename = UUID().uuidString
        let url = tempDirectory.appendingPathComponent(filename)
        
        FileManager.default.createFile(atPath: url.path,
                                       contents: nil,
                                       attributes: nil)
        
        let writer = try FileHandle(forWritingTo: url)
        
        let n = (Double.pi * 2) * 1000 / 48000
        var Ø = 0.0
        var v: Float = 0
        
        for _ in 0 ..< 2048
        {
            v = Float(0.707 * sin(Ø))
            writer.write(Data(bytes: &v, count: 4))
            
            Ø += n
            Ø.formTruncatingRemainder(dividingBy: .pi * 2)
        }
        
        writer.synchronizeFile()
        writer.closeFile()
        
        //
        //
        
        let reader = try FileHandle(forReadingFrom: url)
        
        buffer.read(from: reader,
                    range: -1024 ..< 1024,
                    to: -1024)
        buffer.read(from: reader,
                    range: 2000 ..< 3000,
                    to: 1024,
                    mix: true,
                    scaleWith: Ramp(scalar: 0.5))
        
        XCTAssertEqual(buffer[0], 0)
        XCTAssertNotEqual(buffer[1], 0.5)
        XCTAssertNotEqual(buffer[1023], 0.5)
        XCTAssertNotEqual(buffer[1024], 0.5)
        XCTAssertNotEqual(buffer[1071], 0.5)
        XCTAssertEqual(buffer[1072], 0.5)
        XCTAssertEqual(buffer[2047], 0.5)
        
        reader.closeFile()
        
        //
        //
        
        try FileManager.default.removeItem(at: url)
    }
    
    func testDataCasting() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        let data = Data(audioBuffer: buffer)
        
        XCTAssertEqual(data.count, 8192)
        XCTAssertEqual(data[0], 0x0)
        XCTAssertEqual(data[5], 0x0)
        XCTAssertEqual(data[12], 0x0)
        XCTAssertEqual(data[15], 0x3F)
        
        let slice = Data(audioBuffer: buffer, range: 3 ..< 13)
        
        XCTAssertEqual(slice.count, 40)
        XCTAssertEqual(slice[0], 0x0)
        XCTAssertEqual(slice[5], 0x0)
        XCTAssertEqual(slice[12], 0x0)
        XCTAssertEqual(slice[15], 0x3F)
    }
    
    func testSamplesArray() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        
        let other = AudioBuffer(count: 2048)
        vDSP.fill(&other.rawValue, with: 0.25)
        
        var stereo: [AudioBuffer] = [buffer, other]
        
        XCTAssertEqual(stereo.count, 2)
        XCTAssertEqual(stereo.frameCount, 2048)
        XCTAssertEqual(stereo.frameEnd, SamplePosition(2048))
        XCTAssertEqual(stereo.maximum, 0.5)
        XCTAssertEqual(stereo.minimum, 0.25)
        XCTAssertEqual(stereo.magnitude, 0.5)
        XCTAssertEqual(stereo.rms.rawValue, 0.395, accuracy: 0.001)
        XCTAssertEqual(stereo.average, 0.375)
        
        XCTAssertTrue(stereo[0] === buffer)
        XCTAssertFalse(stereo[1] === buffer)
        stereo[1] = buffer
        XCTAssertTrue(stereo[0] === buffer)
        XCTAssertTrue(stereo[1] === buffer)
    }
    
    func testInterleaveFromOneChannel() async throws
    {
        let buffer = try XCTUnwrap(buffer)
        let interleaved = Data(interleaving: [ buffer ])
        
        XCTAssertEqual(interleaved.count, 2048 * MemoryLayout<Float>.size)
        interleaved.withUnsafeBytes
        {
            let floatBuffer = $0.assumingMemoryBound(to: Float.self)
            XCTAssertEqual(floatBuffer[0], 0.5)
            XCTAssertEqual(floatBuffer[1], 0.5)
            XCTAssertEqual(floatBuffer[2047], 0.5)
        }
    }

    func testInterleaveFromTwoChannels() async throws
    {
        let one = try XCTUnwrap(buffer)
        let two = AudioBuffer(count: 2048)
        vDSP.fill(&two.rawValue, with: 0.25)
        let all = [one, two]
        
        let interleaved = Data(interleaving: all)
        
        XCTAssertEqual(interleaved.count, 4096 * MemoryLayout<Float>.size)
        
        interleaved.withUnsafeBytes
        {
            let floatBuffer = $0.assumingMemoryBound(to: Float.self)
            XCTAssertEqual(floatBuffer[0], 0.5)
            XCTAssertEqual(floatBuffer[1], 0.25)
            XCTAssertEqual(floatBuffer[2], 0.5)
            XCTAssertEqual(floatBuffer[3], 0.25)
            XCTAssertEqual(floatBuffer[4094], 0.5)
            XCTAssertEqual(floatBuffer[4095], 0.25)
        }
    }
    
    func testInterleaveFromThreeChannels() async throws
    {
        let one = try XCTUnwrap(buffer)
        let two = AudioBuffer(count: 2048)
        vDSP.fill(&two.rawValue, with: 0.25)
        let three = AudioBuffer(count: 2048)
        vDSP.fill(&three.rawValue, with: 0.1)
        let all = [one, two, three]
        
        let interleaved = Data(interleaving: all)
        
        XCTAssertEqual(interleaved.count, 6144 * MemoryLayout<Float>.size)
        
        interleaved.withUnsafeBytes
        {
            let floatBuffer = $0.assumingMemoryBound(to: Float.self)
            XCTAssertEqual(floatBuffer[0], 0.5)
            XCTAssertEqual(floatBuffer[1], 0.25)
            XCTAssertEqual(floatBuffer[2], 0.1)
            XCTAssertEqual(floatBuffer[3], 0.5)
            XCTAssertEqual(floatBuffer[4], 0.25)
            XCTAssertEqual(floatBuffer[5], 0.1)
            XCTAssertEqual(floatBuffer[6141], 0.5)
            XCTAssertEqual(floatBuffer[6142], 0.25)
            XCTAssertEqual(floatBuffer[6143], 0.1)
        }
    }
}
