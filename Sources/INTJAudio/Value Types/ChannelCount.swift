//
//  ChannelCount.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation

public struct ChannelCount: RawRepresentable, Codable, Sendable
{    
    public var rawValue: UInt
    
    public init(rawValue value: UInt)
    {
        rawValue = value
    }
    
    public init(_ value: Int)
    {
        rawValue = UInt(value)
    }
    
    public init(_ value: UInt32)
    {
        rawValue = UInt(value)
    }
    
    public static let none = ChannelCount(rawValue: 0)
    public static let mono = ChannelCount(rawValue: 1)
    public static let stereo = ChannelCount(rawValue: 2)
    
    public func map<T>(_ transform: (ChannelIndex) throws -> T) rethrows -> [T]
    {
        try (0 ..< rawValue).map { try transform(ChannelIndex(rawValue: $0)) }
    }
    
    public func forEach(_ body: (ChannelIndex) throws -> Void) rethrows
    {
        try (0 ..< rawValue).forEach { try body(ChannelIndex(rawValue: $0)) }
    }
}

extension ChannelCount: Equatable
{
    public static func == (lhs: ChannelCount, rhs: ChannelCount) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension ChannelCount: Comparable
{
    public static func < (lhs: ChannelCount, rhs: ChannelCount) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

extension ChannelCount: Strideable
{
    public func distance(to other: ChannelCount) -> Int
    {
        Int(other.rawValue - rawValue)
    }
    
    public func advanced(by n: Int) -> ChannelCount
    {
        ChannelCount(rawValue: UInt(max(Int(rawValue) + n, 0)))
    }
}

public func + (lhs: ChannelCount, rhs: ChannelCount) -> ChannelCount
{
    ChannelCount(rawValue: lhs.rawValue + rhs.rawValue)
}

extension ChannelCount: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = UInt

    public init(integerLiteral value: UInt)
    {
        rawValue = value
    }
}

public extension Int
{
    init(_ count: ChannelCount)
    {
        self.init(count.rawValue)
    }
}
