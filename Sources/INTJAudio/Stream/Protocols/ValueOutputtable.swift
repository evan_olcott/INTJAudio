//
//  ValueOutputtable.swift
//  INTJAudio
//
//  Created by Evan Olcott on 8/25/24.
//

import Foundation

public protocol ValueOutputtable: StreamProcessor
{
    var valueOutputBuses: [String: ValueOutputBus] { get }
    
    func prepareForValueOutput(state: StreamState)

    func value(for: ValueOutputBus, state: StreamState) -> Double?
}

extension ValueOutputtable
{
    public func prepareForValueOutput(state: StreamState) { }
    
    public func value(for: ValueOutputBus, state: StreamState) -> Double? { nil }
}
