//
//  AudioOutputBus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import Foundation

public final class AudioOutputBus: Bus, Equatable
{
    public let identifier: String
    public var connectedBus: Bus?
    public weak var processor: StreamProcessor?
    public var taps = [Bus]()

    public init(for processor: AudioOutputtable, identifier: String)
    {
        self.processor = processor
        self.identifier = identifier
    }
    
    public static func == (lhs: AudioOutputBus, rhs: AudioOutputBus) -> Bool
    {
        lhs.identifier == rhs.identifier
    }

    public func handleAudioInput(_ buffer: [AudioBuffer], state: StreamState)
    {
        if let taps = taps as? [AudioInputBus]
        {
            taps.forEach { $0.handleAudioInput(buffer, state: state) }
        }
        
        if let inputBus = connectedBus as? AudioInputBus
        {
            inputBus.handleAudioInput(buffer, state: state)
        }
    }
    
    public func renderAudio(into buffer: [AudioBuffer], state: StreamState)
    {
        if let processor = processor as? AudioOutputtable
        {
            processor.renderAudio(into: buffer, for: self, state: state)
        }

        if let taps = taps as? [AudioInputBus]
        {
            taps.forEach { $0.handleAudioInput(buffer, state: state) }
        }
    }
}
