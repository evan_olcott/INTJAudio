//
//  TimeLocation.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import AVFoundation
import Foundation

public struct TimeLocation: RawRepresentable, Hashable, Codable, Sendable
{
    public var rawValue: TimeInterval
    
    public init(rawValue: TimeInterval)
    {
        self.rawValue = rawValue
    }
    
    public init(_ value: TimeInterval)
    {
        rawValue = value
    }
    
    public init(position: SamplePosition, sampleRate: Hertz = .samplingRate)
    {
        rawValue = Double(position) / sampleRate.rawValue
    }

    public static var zero: TimeLocation { 0.0 }
    public static var distantFuture: TimeLocation { TimeLocation(rawValue: .greatestFiniteMagnitude) }
    public static var distantPast: TimeLocation { TimeLocation(rawValue: -.greatestFiniteMagnitude) }
    
    public var milliseconds: Double
    {
        return rawValue * 1000
    }
}

extension TimeLocation: Equatable
{
    public static func == (lhs: TimeLocation, rhs: TimeLocation) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension TimeLocation: Comparable
{
    public static func < (lhs: TimeLocation, rhs: TimeLocation) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

public extension TimeLocation
{
    mutating func advance(by length: TimeLength)
    {
        rawValue += length.rawValue
    }
    
    func advanced(by length: TimeLength) -> TimeLocation
    {
        var new = self
        new.advance(by: length)
        return new
    }
    
    mutating func recede(by length: TimeLength)
    {
        rawValue -= length.rawValue
    }
    
    func receded(by length: TimeLength) -> TimeLocation
    {
        var new = self
        new.recede(by: length)
        return new
    }
    
    func distance(to other: TimeLocation) -> TimeLength
    {
        TimeLength(other.rawValue - rawValue)
    }

    func absoluteDistance(to other: TimeLocation) -> TimeLength
    {
        TimeLength(abs(other.rawValue - rawValue))
    }

    mutating func scale(by scale: Double)
    {
        rawValue *= scale
    }
    
    func scaled(by scale: Double) -> TimeLocation
    {
        var new = self
        new.scale(by: scale)
        return new
    }
    
    mutating func clamp(to range: TimeRange)
    {
        rawValue = min(max(rawValue, range.rawValue.lowerBound), range.rawValue.upperBound)
    }
    
    func clamped(to range: TimeRange) -> TimeLocation
    {
        var new = self
        new.clamp(to: range)
        return new
    }
    
    mutating func round(_ rule: FloatingPointRoundingRule, toMultipleOf multiple: TimeLength)
    {
        rawValue = (rawValue / multiple.rawValue).rounded(rule) * multiple.rawValue
    }

    func rounded(_ rule: FloatingPointRoundingRule, toMultipleOf multiple: TimeLength) -> TimeLocation
    {
        var new = self
        new.round(rule, toMultipleOf: multiple)
        return new
    }
    
    mutating func convert(fromBase: TimeLocation, toBase: TimeLocation)
    {
        rawValue -= fromBase.rawValue
        rawValue += toBase.rawValue
    }
    
    /// The position of the receiver converted from one origin to another
    ///
    /// - Parameters:
    ///   - fromBase: The origin of the receiver's current context
    ///   - toBase: The origin of the destination context
    /// - Returns: The position in the context of the destination context
    
    func converted(fromBase: TimeLocation, toBase: TimeLocation) -> TimeLocation
    {
        var new = self
        new.convert(fromBase: fromBase, toBase: toBase)
        return new
    }

    func normalizedRelative(to range: TimeRange) -> Double
    {
        Double(rawValue - range.rawValue.lowerBound) / Double(range.length)
    }
}

extension TimeLocation: ExpressibleByFloatLiteral
{
    public typealias FloatLiteralType = TimeInterval

    public init(floatLiteral value: TimeInterval)
    {
        rawValue = value
    }
}

public extension TimeInterval
{
    init(_ value: TimeLocation)
    {
        self.init(value.rawValue)
    }
}

public extension CMTime
{
    init(_ location: TimeLocation)
    {
        self.init(seconds: location.rawValue, preferredTimescale: CMTimeScale(.samplingRate))
    }
}

public extension DateComponentsFormatter
{
    func string(from location: TimeLocation) -> String?
    {
        string(from: TimeInterval(location))
    }
}

@available(macOS 13.0, *)
public extension Duration
{
    init(_ value: TimeLocation)
    {
        self = Duration.seconds(value.rawValue)
    }
}
