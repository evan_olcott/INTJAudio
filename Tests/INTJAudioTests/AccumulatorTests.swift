//
//  AccumulatorTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class AccumulatorTests: XCTestCase
{
    func testAccumulatingEqualSized()
    {
        var count = 0
        let accumulator = Accumulator(count: 512) { _ in count += 1; return true }
        let input = AudioBuffer(count: 512)

        (0 ..< 12).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 12)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 0)
    }
    
    func testAccumulatingUndersized()
    {
        var count = 0
        let accumulator = Accumulator(count: 400) { _ in count += 1; return true }
        let input = AudioBuffer(count: 100)

        (0 ..< 12).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 3)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 0)
    }

    func testAccumulatingOddUndersized()
    {
        var count = 0
        let accumulator = Accumulator(count: 400) { _ in count += 1; return true }
        let input = AudioBuffer(count: 120)

        (0 ..< 12).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 3)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 240)
    }

    func testAccumulatingOversized()
    {
        var count = 0
        let accumulator = Accumulator(count: 300) { _ in count += 1; return true }
        let input = AudioBuffer(count: 600)

        (0 ..< 5).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 10)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 0)
    }

    func testAccumulatingOddOversized()
    {
        var count = 0
        let accumulator = Accumulator(count: 300) { _ in count += 1; return true }
        let input = AudioBuffer(count: 470)

        (0 ..< 12).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 18)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 240)
    }

    func testAccumulatingUndersizedWithInterval()
    {
        var count = 0
        let accumulator = Accumulator(count: 1000, interval: 200) { _ in count += 1; return true }
        let input = AudioBuffer(count: 70)

        (0 ..< 12).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 4)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 840)
    }

    func testAccumulatingOversizedWithInterval()
    {
        var count = 0
        let accumulator = Accumulator(count: 1000, interval: 200) { _ in count += 1; return true }
        let input = AudioBuffer(count: 470)

        (0 ..< 3).forEach { _ in accumulator.append(input) }

        XCTAssertEqual(count, 7)
        XCTAssertEqual(accumulator.accumulatedCount.rawValue, 810)
    }
    
    func testAccumulatingWithInterval()
    {
        let accumulator = Accumulator(count: 1000, interval: 200) { _ in return true }
        let input = AudioBuffer(count: 100)
        for i in 0 ..< Int(input.sampleCount) { input[SamplePosition(i)] = 0.5 }

        accumulator.append(input)
        
        XCTAssertEqual(accumulator.rawValue[0], 0)
        XCTAssertEqual(accumulator.rawValue[799], 0)
        XCTAssertEqual(accumulator.rawValue[800], 0.5)
        XCTAssertEqual(accumulator.rawValue[899], 0.5)
        XCTAssertEqual(accumulator.rawValue[900], 0)
        XCTAssertEqual(accumulator.rawValue[999], 0)

        accumulator.append(input)
        
        XCTAssertEqual(accumulator.rawValue[0], 0)
        XCTAssertEqual(accumulator.rawValue[599], 0)
        XCTAssertEqual(accumulator.rawValue[600], 0.5)
        XCTAssertEqual(accumulator.rawValue[799], 0.5)
        XCTAssertEqual(accumulator.rawValue[800], 0)
        XCTAssertEqual(accumulator.rawValue[999], 0)

        accumulator.append(input)
        
        XCTAssertEqual(accumulator.rawValue[0], 0)
        XCTAssertEqual(accumulator.rawValue[599], 0)
        XCTAssertEqual(accumulator.rawValue[600], 0.5)
        XCTAssertEqual(accumulator.rawValue[899], 0.5)
        XCTAssertEqual(accumulator.rawValue[900], 0)
        XCTAssertEqual(accumulator.rawValue[999], 0)

        accumulator.append(input)
        
        XCTAssertEqual(accumulator.rawValue[0], 0)
        XCTAssertEqual(accumulator.rawValue[399], 0)
        XCTAssertEqual(accumulator.rawValue[400], 0.5)
        XCTAssertEqual(accumulator.rawValue[799], 0.5)
        XCTAssertEqual(accumulator.rawValue[800], 0)
        XCTAssertEqual(accumulator.rawValue[999], 0)
    }
}
