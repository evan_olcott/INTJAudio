//
//  MockEffect.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

import Foundation
import INTJAudio

final class MockEffect: MockObject, Effect
{
    var displayName = "MockEffect"
    var isEnabled = true
    
    func prepare(for dimensions: AudioDimensions) throws
    {
        try addCallThrowing(to: #function, with: [ "for": dimensions ])
    }
    
    func process(_: [AudioBuffer], for position: SamplePosition)
    {
        addCall(to: #function, with: [ "for": position ])
    }
}
