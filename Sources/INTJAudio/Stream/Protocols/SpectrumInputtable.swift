//
//  SpectrumInputtable.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/31/24.
//

import Foundation

public protocol SpectrumInputtable: StreamProcessor
{
    var spectrumInputBuses: [SpectrumInputBus] { get }
    
    func prepareForSpectrumInput(with: SpectrumDimensions, state: StreamState)

    func handleSpectrumInput(_: [ComplexBuffer], from: SpectrumInputBus, state: StreamState)
}

public extension SpectrumInputtable
{
    func prepareForSpectrumInput(with: SpectrumDimensions, state: StreamState) { }
}
