//
//  Cent.swift
//  Atlas
//
//  Created by Evan Olcott on 1/14/21.
//

import Foundation

public struct Cent: RawRepresentable, Codable, Sendable
{
    public var rawValue: Int
    
    public init(rawValue value: Int)
    {
        rawValue = value
    }
    
    public init(_ value: Int)
    {
        rawValue = value
    }
    
    public init(_ value: Double)
    {
        rawValue = Int(value.rounded())
    }
    
    public var magnitude: Cent
    {
        Cent(Int(rawValue.magnitude))
    }
}

extension Cent: Equatable
{
    public static func == (lhs: Cent, rhs: Cent) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension Cent: Comparable
{
    public static func < (lhs: Cent, rhs: Cent) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

public extension Cent
{
    mutating func negate()
    {
        rawValue.negate()
    }
    
    func negated() -> Cent
    {
        var new = self
        new.negate()
        return new
    }
}

extension Cent: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int
    
    public init(integerLiteral value: Int)
    {
        rawValue = value
    }
}
