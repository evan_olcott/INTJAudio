//
//  ChannelCountTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class ChannelCountTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(ChannelCount(rawValue: 4).rawValue, 4)
    }
    
    func testInitAsInt()
    {
        XCTAssertEqual(ChannelCount(Int(4)).rawValue, 4)
    }

    func testInitAsUInt32()
    {
        XCTAssertEqual(ChannelCount(UInt32(4)).rawValue, 4)
    }

    func testConstants()
    {
        XCTAssertEqual(ChannelCount.none.rawValue, 0)
        XCTAssertEqual(ChannelCount.mono.rawValue, 1)
        XCTAssertEqual(ChannelCount.stereo.rawValue, 2)
    }
    
    func testMap()
    {
        let output = ChannelCount(4).map { $0.next }
        XCTAssertEqual(output, [ ChannelIndex(1), ChannelIndex(2), ChannelIndex(3), ChannelIndex(4) ])
    }
    
    func testForEach()
    {
        var total: UInt = 0
        
        ChannelCount(4).forEach { total += $0.rawValue }
        XCTAssertEqual(total, 6)
    }
    
    func testEquality()
    {
        let count1 = ChannelCount(2)
        let count2 = ChannelCount(5)
        
        XCTAssertTrue(count1 == count1)
        XCTAssertFalse(count1 == count2)
    }
    
    func testComparable()
    {
        let count1 = ChannelCount(2)
        let count2 = ChannelCount(5)

        XCTAssertTrue(count1 < count2)
        XCTAssertFalse(count1 > count2)
        XCTAssertTrue(count2 > count1)
    }
}
