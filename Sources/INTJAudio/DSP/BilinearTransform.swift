//
//  BilinearTransform.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

// https://www.earlevel.com/main/2012/11/26/biquad-c-source-code/

import Foundation
import OSLog

// swiftlint:disable identifier_name

private let π = Double.pi

public struct Coefficients
{
    let a0: Double
    let a1: Double
    let a2: Double
    let b1: Double
    let b2: Double

    public var ordered: [Double] { return [ a0, a1, a2, b1, b2 ] }

    // https://www.kvraudio.com/forum/viewtopic.php?t=248806

    public func magnitude(at frequency: Hertz) -> Decibel
    {
        Logger.stream.debug("Coefficients.magnitude(at:)")
        
        let 𝜔 = 2.0 * π * (frequency.rawValue / Hertz.samplingRate.rawValue)
        let cos𝜔 = cos(𝜔)
        let cos2𝜔 = cos(2 * 𝜔)
        let n = (a0 * a0) + (a1 * a1) + (a2 * a2) + (2 * cos𝜔 * (a0 * a1 + a1 * a2)) + (2 * cos2𝜔 * a0 * a2)
        let d = 1.0 + (b1 * b1) + (b2 * b2) + (2 * cos𝜔 * (b1 + b1 * b2)) + (2 * cos2𝜔 * b2)

        return Decibel(Scalar(sqrt(n / d)))
    }
}

public struct BilinearTransform
{
    public static func allPassCoefficients() -> Coefficients
    {
        return Coefficients(a0: 1, a1: 1, a2: 1, b1: 1, b2: 1)
    }

    public static func lowPassCoefficients(at frequency: Hertz, Q: Double) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let norm = 1 / (1 + K / Q + K * K)
        let a0 = K * K * norm
        let a1 = 2 * a0
        let a2 = a0
        let b1 = 2 * (K * K - 1) * norm
        let b2 = (1 - K / Q + K * K) * norm

        return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
    }

    public static func highPassCoefficients(at frequency: Hertz, Q: Double) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let norm = 1 / (1 + K / Q + K * K)
        let a0 = norm
        let a1 = -2 * a0
        let a2 = a0
        let b1 = 2 * (K * K - 1) * norm
        let b2 = (1 - K / Q + K * K) * norm

        return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
    }

    public static func bandPassCoefficients(at frequency: Hertz, Q: Double) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let norm = 1 / (1 + K / Q + K * K)
        let a0 = K / Q * norm
        let a1 = 0.0
        let a2 = -a0
        let b1 = 2 * (K * K - 1) * norm
        let b2 = (1 - K / Q + K * K) * norm

        return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
    }

    public static func notchCoefficients(at frequency: Hertz, Q: Double) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let norm = 1 / (1 + K / Q + K * K)
        let a0 = (1 + K * K) * norm
        let a1 = 2 * (K * K - 1) * norm
        let a2 = a0
        let b1 = a1
        let b2 = (1 - K / Q + K * K) * norm

        return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
    }

    public static func peakingCoefficients(at frequency: Hertz, gain: Decibel, Q: Double) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let V = pow(10, fabs(gain.rawValue) / 20)

        if gain.rawValue >= 0
        {
            let norm = 1 / (1 + 1 / Q * K + K * K)
            let a0 = (1 + V / Q * K + K * K) * norm
            let a1 = 2 * (K * K - 1) * norm
            let a2 = (1 - V / Q * K + K * K) * norm
            let b1 = a1
            let b2 = (1 - 1 / Q * K + K * K) * norm

            return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
        }
        else
        {
            let norm = 1 / (1 + V / Q * K + K * K)
            let a0 = (1 + 1 / Q * K + K * K) * norm
            let a1 = 2 * (K * K - 1) * norm
            let a2 = (1 - 1 / Q * K + K * K) * norm
            let b1 = a1
            let b2 = (1 - V / Q * K + K * K) * norm

            return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
        }
    }

    public static func lowShelfCoefficients(at frequency: Hertz, gain: Decibel) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let V = pow(10, fabs(gain.rawValue) / 20)

        if gain.rawValue >= 0
        {
            let norm = 1.0 / (1 + sqrt(2) * K + K * K)
            let a0 = (1 + sqrt(2 * V) * K + V * K * K) * norm
            let a1 = 2 * (V * K * K - 1) * norm
            let a2 = (1 - sqrt(2 * V) * K + V * K * K) * norm
            let b1 = 2 * (K * K - 1) * norm
            let b2 = (1 - sqrt(2) * K + K * K) * norm

            return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
        }
        else
        {
            let norm = 1.0 / (1 + sqrt(2 * V) * K + V * K * K)
            let a0 = (1 + sqrt(2) * K + K * K) * norm
            let a1 = 2 * (K * K - 1) * norm
            let a2 = (1 - sqrt(2) * K + K * K) * norm
            let b1 = 2 * (V * K * K - 1) * norm
            let b2 = (1 - sqrt(2 * V) * K + V * K * K) * norm

            return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
        }
    }

    public static func highShelfCoefficients(at frequency: Hertz, gain: Decibel) -> Coefficients
    {
        let K = tan(π * (frequency.rawValue / Hertz.samplingRate.rawValue))
        let V = pow(10, fabs(gain.rawValue) / 20)

        if gain.rawValue >= 0
        {
            let norm = 1.0 / (1 + sqrt(2) * K + K * K)
            let a0 = (V + sqrt(2 * V) * K + K * K) * norm
            let a1 = 2 * (K * K - V) * norm
            let a2 = (V - sqrt(2 * V) * K + K * K) * norm
            let b1 = 2 * (K * K - 1) * norm
            let b2 = (1 - sqrt(2) * K + K * K) * norm

            return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
        }
        else
        {
            let norm = 1.0 / (V + sqrt(2 * V) * K + K * K)
            let a0 = (1 + sqrt(2) * K + K * K) * norm
            let a1 = 2 * (K * K - 1) * norm
            let a2 = (1 - sqrt(2) * K + K * K) * norm
            let b1 = 2 * (K * K - V) * norm
            let b2 = (V - sqrt(2 * V) * K + K * K) * norm

            return Coefficients(a0: a0, a1: a1, a2: a2, b1: b1, b2: b2)
        }
    }
}
