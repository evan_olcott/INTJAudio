//
//  Stream.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import Foundation

public final class Stream: EngineDelegate
{
    enum Mode
    {
        case realtime(DeviceProtocol)
        case nonRealtime(AudioDimensions)
        
        var isRealtime: Bool
        {
            switch self {
            case .realtime:
                true
            case .nonRealtime:
                false
            }
        }
    }
    
    public private(set) var position: SamplePosition = .zero
    public private(set) var engine: Engine?

    private let mode: Mode
    private let spectrumResolution: Resolution
    private var processors = [StreamProcessor]()
    
    public enum Error: LocalizedError
    {
        case processorDoesNotSupportNonRealtime
        
        public var errorDescription: String?
        {
            switch self
            {
            case .processorDoesNotSupportNonRealtime:
                "A processor does not support non-realtime mode."
            }
        }
    }
    
    public init(device: DeviceProtocol, spectrumResolution: Resolution)
    {
        self.mode = .realtime(device)
        self.spectrumResolution = spectrumResolution
    }
    
    public init(dimensions: AudioDimensions, spectrumResolution: Resolution)
    {
        self.mode = .nonRealtime(dimensions)
        self.spectrumResolution = spectrumResolution
    }
    
    //
    // MARK: Connections
    //
    
    public func connect<T: Bus, U: Bus>(from outputBus: T, to inputBus: U)
    {
        // Add processors if necessary
        
        if let processor = inputBus.processor,
           processors.contains(where: { $0 === processor }) == false
        {
            processors.append(processor)
        }
        
        if let processor = outputBus.processor,
           processors.contains(where: { $0 === processor }) == false
        {
            processors.append(processor)
        }
        
        // Create connection
        
        outputBus.connectedBus = inputBus
        inputBus.connectedBus = outputBus
    }
    
    public func tap<T: Bus, U: Bus>(_ outputBus: T,
                                    to inputBus: U)
    {
        // Add processors if necessary
        
        if let processor = inputBus.processor,
           processors.contains(where: { $0 === processor }) == false
        {
            processors.append(processor)
        }
        
        if let processor = outputBus.processor,
           processors.contains(where: { $0 === processor }) == false
        {
            processors.append(processor)
        }
        
        // Add tap
        
        outputBus.taps.append(inputBus)
    }
    
    //
    // MARK: Running
    //
        
    private var nonRealtimeLoopTask: Task<Void, Never>?
    private var inputHeadBuses = [AudioInputBus]()
    private var outputHeadBuses = [AudioOutputBus]()
    private var method: StreamState.Method = .pull
    
    public func start() throws
    {
        inputHeadBuses = processors
            .compactMap { $0 as? AudioInputtable }
            .compactMap { $0.audioInputHeadBus }
        
        outputHeadBuses = processors
            .compactMap { $0 as? AudioOutputtable }
            .compactMap { $0.audioOutputHeadBus }
        
        method = outputHeadBuses.isEmpty ? .push : .pull
        
        switch mode
        {
        case .realtime(let device):
            engine = try Engine(device: device, delegate: self)
            try engine?.start()
            
        case .nonRealtime(let dimensions):
            guard processors.allSatisfy({ $0.supportsNonRealtime }) else {
                throw Error.processorDoesNotSupportNonRealtime
            }

            try self.willPrepare()
            try self.prepareForAudioInput(with: dimensions)
            try self.prepareForAudioRendering(with: dimensions)
            
            fatalError("Not implemented yet: not sure if this is right way, getting concurrency-related errors here")
//            nonRealtimeLoopTask = Task {
//                await nonRealtimeLoop(with: dimensions)
//            }
        }
    }
    
    public func stop() throws
    {
        switch mode
        {
        case .realtime:
            try engine?.stop()
            engine = nil

            inputHeadBuses = []
            outputHeadBuses = []

        case .nonRealtime:
            nonRealtimeLoopTask?.cancel()
        }
    }
    
// Not implemented yet: not sure if this is right way, getting concurrency-related errors here
    
//    private func nonRealtimeLoop(with dimensions: AudioDimensions) async
//    {
//        while Task.isCancelled == false
//        {
//            self.handleAudioInput([], frame: position)
//            self.renderAudio(into: [], frame: position)
//            position.advance(by: dimensions.frameCount)
//        }
//        
//        await loopStopped()
//    }
    
    @MainActor
    private func loopStopped()
    {
        do
        {
            try self.cleanup()

            inputHeadBuses = []
            outputHeadBuses = []
        }
        catch
        {
            print(error.localizedDescription)
        }
    }
    
    //
    // MARK: - Engine delegate
    //
    
    public func willPrepare() throws
    {
        let state = StreamState(frame: .zero, method: method, isRealtime: mode.isRealtime)

        processors
            .compactMap { $0 as? ValueOutputtable }
            .forEach { $0.prepareForValueOutput(state: state) }
        
        processors
            .compactMap { $0 as? ValueInputtable }
            .forEach { $0.prepareForValueInput(state: state) }
    }
    
    public func prepareForAudioInput(with dimensions: AudioDimensions) throws
    {
        let state = StreamState(frame: .zero, method: method, isRealtime: mode.isRealtime)
        let spectrumDimensions = SpectrumDimensions(resolution: spectrumResolution,
                                                    channelCount: dimensions.channelCount)
        
        processors
            .compactMap { $0 as? AudioInputtable }
            .forEach { $0.prepareForAudioInput(with: dimensions, state: state) }
        
        processors
            .compactMap { $0 as? SpectrumInputtable }
            .forEach { $0.prepareForSpectrumInput(with: spectrumDimensions, state: state)}
    }
    
    public func prepareForAudioRendering(with dimensions: AudioDimensions) throws
    {
        let state = StreamState(frame: .zero, method: method, isRealtime: mode.isRealtime)
        let spectrumDimensions = SpectrumDimensions(resolution: spectrumResolution,
                                                    channelCount: dimensions.channelCount)

        processors
            .compactMap { $0 as? AudioOutputtable }
            .forEach { $0.prepareForAudioRendering(with: dimensions, state: state) }
        
        processors
            .compactMap { $0 as? SpectrumOutputtable }
            .forEach { $0.prepareForSpectrumRendering(with: spectrumDimensions, state: state)}
    }
    
    public func handleAudioInput(_ buffer: [AudioBuffer], frame: SamplePosition)
    {
        position = frame
        
        let state = StreamState(frame: frame, method: method, isRealtime: mode.isRealtime)
        inputHeadBuses.forEach { $0.handleAudioInput(buffer, state: state) }
    }
    
    public func renderAudio(into buffer: [AudioBuffer], frame: SamplePosition)
    {
        let state = StreamState(frame: frame, method: .pull, isRealtime: mode.isRealtime)
        outputHeadBuses.forEach { $0.renderAudio(into: buffer, state: state) }
    }
    
    public func cleanup() throws
    {
        processors.forEach { $0.cleanup() }
    }
}
