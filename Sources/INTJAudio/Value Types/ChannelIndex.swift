//
//  ChannelIndex.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation

public struct ChannelIndex: RawRepresentable, Hashable, Codable, Sendable
{
    public var rawValue: UInt
    
    public init(rawValue value: UInt)
    {
        rawValue = value
    }
    
    public init(_ value: Int)
    {
        rawValue = UInt(value)
    }
    
    public func exists(in channels: [AudioBuffer]) -> Bool
    {
        rawValue < channels.count
    }
    
    public static let first = ChannelIndex(rawValue: 0)
    public static let firstPair = [ ChannelIndex(rawValue: 0), ChannelIndex(rawValue: 1) ]
    
    public var next: ChannelIndex
    {
        ChannelIndex(rawValue: rawValue + 1)
    }
}

extension ChannelIndex: Equatable
{
    public static func == (lhs: ChannelIndex, rhs: ChannelIndex) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension ChannelIndex: Comparable
{
    public static func < (lhs: ChannelIndex, rhs: ChannelIndex) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

public extension ChannelIndex
{
    mutating func advance(by count: ChannelCount)
    {
        rawValue += count.rawValue
    }
    
    func advanced(by count: ChannelCount) -> ChannelIndex
    {
        var new = self
        new.advance(by: count)
        return new
    }
    
    func distance(to other: ChannelIndex) -> ChannelCount
    {
        ChannelCount(rawValue: other.rawValue - rawValue)
    }
}

extension ChannelIndex: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int
    
    public init(integerLiteral value: Int)
    {
        self.rawValue = UInt(value)
    }
}

public extension Int
{
    init(_ count: ChannelIndex)
    {
        self.init(count.rawValue)
    }
}
