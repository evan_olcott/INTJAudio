//
//  AudioBufferArrayTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
import CoreAudio
@testable import INTJAudio
import XCTest

class AudioBufferArrayTests: XCTestCase
{
    var channelList = [INTJAudio.AudioBuffer]()

    override func setUp() async throws
    {
        try await super.setUp()

        channelList = (0 ..< 2).map { _ in AudioBuffer(count: 2048) }

        for c in 0 ..< channelList.count
        {
            let channel = channelList[c]

            for i in 0 ..< Int(channelList.frameCount) { channel[SamplePosition(i)] = 0.5 }
        }
    }

    override func tearDown() async throws
    {
        try await super.tearDown()

        channelList = []
    }

    func testDimensions()
    {
        XCTAssertNotNil(channelList)
        XCTAssertEqual(channelList.frameCount, 2048)

        XCTAssertEqual(channelList[0][0], 0.5)
        XCTAssertEqual(channelList[1][2047], 0.5)

        let emptyList = [INTJAudio.AudioBuffer]()
        XCTAssertTrue(emptyList.isEmpty)
        XCTAssertEqual(emptyList.frameCount, 0)
    }

    func testClear()
    {
        channelList.forEach { $0.clear() }

        XCTAssertEqual(channelList[0].maximum, 0)
        XCTAssertEqual(channelList[1].maximum, 0)
    }

    func testScale()
    {
        channelList.forEach { $0.scale(by: Scalar(2), range: 0 ..< 1024) }

        XCTAssertEqual(channelList[0][0], 1)
        XCTAssertEqual(channelList[0][1023], 1)
        XCTAssertEqual(channelList[0][1024], 0.5)
        XCTAssertEqual(channelList[0][2047], 0.5)

        XCTAssertEqual(channelList[1][0], 1)
        XCTAssertEqual(channelList[1][1023], 1)
        XCTAssertEqual(channelList[1][1024], 0.5)
        XCTAssertEqual(channelList[1][2047], 0.5)
    }

    func testRampScale()
    {
        channelList.forEach { $0.scale(with: Ramp(start: 0, end: 2), range: 0 ..< 1024) }
        channelList.forEach { $0.scale(with: Ramp(start: 2, end: 1), range: 1024 ..< 2047) }

        XCTAssertEqual(channelList[0][0], 0)
        XCTAssertEqual(channelList[0][512], 0.5)
        XCTAssertEqual(channelList[0][1024], 1)
        XCTAssertEqual(channelList[0][2047], 0.5)

        XCTAssertEqual(channelList[1][0], 0)
        XCTAssertEqual(channelList[1][512], 0.5)
        XCTAssertEqual(channelList[1][1024], 1)
        XCTAssertEqual(channelList[1][2047], 0.5)
    }

    func testMeasurements()
    {
        XCTAssertEqual(channelList.maximum, 0.5)
        XCTAssertEqual(channelList.minimum, 0.5)
        XCTAssertEqual(channelList.magnitude, 0.5)
        XCTAssertEqual(channelList.rms, 0.5)
        XCTAssertEqual(channelList.average, 0.5)

        channelList[1].clear()

        XCTAssertEqual(channelList.maximum, 0.5)
        XCTAssertEqual(channelList.minimum, 0)
        XCTAssertEqual(channelList.magnitude, 0.5)
        XCTAssertEqual(channelList.rms, 0.35355338)
        XCTAssertEqual(channelList.average, 0.25)
    }

    func testCopyFromUnsafeMutableAudioBufferListPointer()
    {
        let bufferList = AudioBufferList.allocate(maximumBuffers: 1)
        bufferList[0] = CoreAudio.AudioBuffer(mNumberChannels: 2,
                                    mDataByteSize: 2048,
                                    mData: UnsafeMutableRawPointer.allocate(byteCount: 2048, alignment: 0))
        
        let interleaved = bufferList[0].mData!.assumingMemoryBound(to: Float.self)

        var one: Float = 1
        vDSP_vfill(&one, &interleaved[0], 2, vDSP_Length(256))
        var two: Float = 2
        vDSP_vfill(&two, &interleaved[1], 2, vDSP_Length(256))
        
        let channelList = [INTJAudio.AudioBuffer](bufferList)

        var sum: Float = 0
        vDSP_sve(channelList[0].pointer, 1,
                 &sum,
                 vDSP_Length(256))
        XCTAssertEqual(sum, 256)

        sum = 0
        vDSP_sve(channelList[1].pointer, 1,
                 &sum,
                 vDSP_Length(256))
        XCTAssertEqual(sum, 512)
    }
}
