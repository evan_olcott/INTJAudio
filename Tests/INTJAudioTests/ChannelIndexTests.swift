//
//  ChannelIndexTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class ChannelIndexTests: XCTestCase
{
    func testInit()
    {
        let index = ChannelIndex(4)
        XCTAssertEqual(index.rawValue, 4)
    }

    func testInitRawValue()
    {
        let index = ChannelIndex(rawValue: 12)
        XCTAssertEqual(index.rawValue, 12)
    }
    
    func testInitFirst()
    {
        let index = ChannelIndex.first
        XCTAssertEqual(index.rawValue, 0)
    }
    
    func testInitFirstPair()
    {
        let pair = ChannelIndex.firstPair
        XCTAssertEqual(pair[0].rawValue, 0)
        XCTAssertEqual(pair[1].rawValue, 1)
    }
    
    func testNext()
    {
        let index = ChannelIndex(4)
        XCTAssertEqual(index.next.rawValue, 5)
    }
    
    func testExistsInBuffers()
    {
        let index = ChannelIndex(2)
        var buffers = [AudioBuffer]()
        
        XCTAssertFalse(index.exists(in: buffers))
        
        buffers.append(AudioBuffer(count: 8))
        buffers.append(AudioBuffer(count: 8))
        XCTAssertFalse(index.exists(in: buffers))

        buffers.append(AudioBuffer(count: 8))
        XCTAssertTrue(index.exists(in: buffers))
    }
    
    func testEquatable()
    {
        let index1 = ChannelIndex(4)
        let index2 = ChannelIndex(5)
        
        XCTAssertTrue(index1 == index1)
        XCTAssertFalse(index1 == index2)
    }

    func testComparable()
    {
        let index1 = ChannelIndex(4)
        let index2 = ChannelIndex(5)
        
        XCTAssertFalse(index1 < index1)
        XCTAssertTrue(index1 < index2)
        XCTAssertFalse(index2 < index1)
    }
    
    func testStrideable()
    {
        let index = ChannelIndex(4)
        
        XCTAssertEqual(index.advanced(by: ChannelCount(2)).rawValue, 6)
        XCTAssertEqual(index.distance(to: ChannelIndex(6)).rawValue, 2)
    }
}
