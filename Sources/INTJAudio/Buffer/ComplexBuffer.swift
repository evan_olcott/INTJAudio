//
//  ComplexBuffer.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/28/24.
//

import Accelerate
import Foundation

/// A collection of real and imaginary value pairs representing the spectrum of an audio signal

open class ComplexBuffer
{
    public let resolution: Resolution
    public let samplingRate: Hertz

    public var real: UnsafeMutableBufferPointer<Float>
    public var imag: UnsafeMutableBufferPointer<Float>
    private var owns = false
    
    public init(resolution: Resolution,
                samplingRate: Hertz = .samplingRate)
    {
        Logger.stream.debug("ComplexBuffer.init(resolution:)")

        self.resolution = resolution
        self.samplingRate = samplingRate

        let count = Int(resolution.sampleCount.half)
        real = UnsafeMutableBufferPointer<Float>.allocate(capacity: count)
        imag = UnsafeMutableBufferPointer<Float>.allocate(capacity: count)
        
        real.initialize(repeating: 0)
        imag.initialize(repeating: 0)
        
        owns = true
    }
    
    public init(buffer: DSPSplitComplex,
                resolution: Resolution,
                samplingRate: Hertz = .samplingRate)
    {
        Logger.stream.debug("ComplexBuffer.init(buffer:resolution:)")

        self.resolution = resolution
        self.samplingRate = samplingRate
        
        let count = Int(resolution.sampleCount.half)
        real = UnsafeMutableBufferPointer<Float>(start: buffer.realp, count: count)
        imag = UnsafeMutableBufferPointer<Float>(start: buffer.imagp, count: count)
    }
    
    convenience public init(_ polar: PolarBuffer)
    {
        self.init(resolution: polar.resolution,
                  samplingRate: polar.samplingRate)
        
        let count = Int(resolution.sampleCount.half)
        var sines = [Float](repeating: 0, count: count)
        var cosines = [Float](repeating: 0, count: count)
        
        vForce.sincos(polar.phases, sinResult: &sines, cosResult: &cosines)
                
        vDSP.multiply(polar.magnitudes, cosines, result: &real)
        vDSP.multiply(polar.magnitudes, sines, result: &imag)
    }
    
    deinit
    {
        Logger.stream.debug("ComplexBuffer.deinit")

        if owns
        {
            real.deallocate()
            imag.deallocate()
        }
    }
    
    /// Sets the spectrum values to 0 (silence).
    
    public func clear()
    {
        Logger.stream.debug("ComplexBuffer.clear()")

        let count = Int(resolution.sampleCount.half)
        let bytes = MemoryLayout<Float>.size * count
        memset(real.baseAddress!, 0, bytes)
        memset(imag.baseAddress!, 0, bytes)
    }
    
    // TODO: Add cool processes
}

extension DSPSplitComplex
{
    init(_ buffer: ComplexBuffer)
    {
        self.init(realp: buffer.real.baseAddress!,
                  imagp: buffer.imag.baseAddress!)
    }
}
