//
//  RingBuffer.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation
import OSLog

public class RingBuffer: AudioBuffer
{
    public var clearOnRetrieve = true
    
    var storePosition: SamplePosition = .zero
    var retrievePosition: SamplePosition = .zero
    
    enum Error: Swift.Error
    {
        case tooLarge
    }
    
    /// Copies the contents of an `AudioBuffer` into the receiver at the next available frame.
    ///
    /// - Parameters:
    ///   - input: The `AudioBuffer` to store.

    public func store(_ input: AudioBuffer) throws
    {
        try store(input, at: &storePosition)
    }
    
    /// Copies the contents of an `AudioBuffer` into the receiver at the given frame and increments the store position.
    ///
    /// - Parameters:
    ///   - input: The AudioChannel to store.
    ///   - position: The frame that will store the start of the contents of `input`. This value will be incremented by the number of frames added.

    public func store(_ input: AudioBuffer, at position: inout SamplePosition) throws
    {
        try store(input, at: position)
        position.advance(by: input.sampleCount)
    }

    /// Copies the contents of an `AudioBuffer` into the receiver at the given frame.
    ///
    /// - Parameters:
    ///   - input: The `AudioBuffer` to store.
    ///   - position: The frame that will store the start of the contents of `input`.

    public func store(_ input: AudioBuffer, at position: SamplePosition) throws
    {
        let range = position ..< position.advanced(by: input.sampleCount)
        Logger.stream.debug("RingBuffer.store(at: \(range.start.rawValue) ..< \(range.end.rawValue))")
        
        guard input.sampleCount <= sampleCount else { throw Error.tooLarge }

        var offset = position
        while offset < .zero
        {
            offset.advance(by: sampleCount)
        }

        var readPosition = SamplePosition.zero
        let readRange = ..<input.end
        var writePosition = SamplePosition(Int(offset) % Int(sampleCount))
        let writeRange = ..<end

        repeat
        {
            let readCount = min(readPosition.distance(to: input.end),
                                writePosition.distance(to: end))

            copy(from: input,
                 range: readPosition ..< readPosition.advanced(by: readCount),
                 to: writePosition)

            writePosition.advance(by: readCount)
            if writeRange.contains(writePosition) == false
            {
                writePosition = .zero
            }
            readPosition.advance(by: readCount)
        }
        while readRange.contains(readPosition)
    }
    
    /// Moves the contents of the `RingBuffer` from the next available frame into an `AudioBuffer`
    ///
    /// - Parameters:
    ///   - output: The `AudioBuffer` to hold the retrieved results.
    
    public func retrieve(into output: AudioBuffer) throws
    {
        try retrieve(into: output, from: &retrievePosition)
    }

    /// Moves the contents of the `RingBuffer` from the next available frame into an `AudioBuffer` and incremenets the retrieve position.
    ///
    /// - Parameters:
    ///   - output: The `AudioBuffer` to hold the retrieved results.
    ///   - position: The start frame of the contents of the ring buffer to be moved to `output`. This value will be incremented by the number of frames moved.

    public func retrieve(into output: AudioBuffer, from position: inout SamplePosition) throws
    {
        try retrieve(into: output, from: position)
        position.advance(by: output.sampleCount)
    }

    /// Moves the contents of the `RingBuffer` from the given frame into an `AudioBuffer`
    ///
    /// - Parameters:
    ///   - output: The `AudioBuffer` to hold the retrieved results.
    ///   - position: The start frame of the contents of the ring buffer to be moved to `output`.
    
    public func retrieve(into output: AudioBuffer, from position: SamplePosition) throws
    {
        let range = position ..< position.advanced(by: output.sampleCount)
        Logger.stream.debug("RingBuffer.retrieve(from: \(range.start.rawValue) ..< \(range.end.rawValue))")
        
        guard output.sampleCount <= sampleCount else { throw Error.tooLarge }

        var offset = position
        while offset < .zero
        {
            offset.advance(by: sampleCount)
        }

        var readPosition = SamplePosition(Int(offset) % Int(sampleCount))
        let readRange = ..<end
        var writePosition = SamplePosition.zero
        let writeRange = ..<output.end

        repeat
        {
            let readCount = min(readPosition.distance(to: end),
                                writePosition.distance(to: output.end))
            let safeReadRange = readPosition ..< readPosition.advanced(by: readCount)

            output.copy(from: self,
                        range: safeReadRange,
                        to: writePosition)

            if clearOnRetrieve {
                clear(safeReadRange)
            }

            readPosition.advance(by: readCount)
            if readRange.contains(readPosition) == false
            {
                readPosition = .zero
            }
            writePosition.advance(by: readCount)
        }
        while writeRange.contains(writePosition)
    }
    
    public var readableCount: SampleCount
    {
        retrievePosition.distance(to: storePosition)
    }
}
