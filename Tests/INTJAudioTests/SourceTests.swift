//
//  SourceTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

@testable import INTJAudio
import XCTest

class SourceTests: XCTestCase
{
    func testInMemory() async throws
    {
        let sourceBuffer = AudioBuffer(count: 2048)
        for i in 0 ..< 2048 { sourceBuffer[i] = Float(i) }
        let source = Source(with: [sourceBuffer])
        
        XCTAssertEqual(source.frameCount, 2048)
        XCTAssertEqual(source.end, 2048)
        
        let renderBuffers = [ AudioBuffer(count: 512), AudioBuffer(count: 512) ]
        source.read(into: renderBuffers, from: 0 ..< 512, to: 0)
        XCTAssertEqual(renderBuffers[0].magnitude, 0)
        XCTAssertEqual(renderBuffers[1].magnitude, 0)

        source.prepare(for: AudioDimensions(frameCount: 512, channelCount: 2))
        source.read(into: renderBuffers, from: 1024 ..< 1536, to: 0)
        XCTAssertEqual(renderBuffers[0][0], 1024)
        XCTAssertEqual(renderBuffers[0][511], 1535)
        XCTAssertEqual(renderBuffers[1][0], 1024)
        XCTAssertEqual(renderBuffers[1][511], 1535)
    }
    
    func testFiles() async throws
    {
        let tempDirectory = FileManager.default.temporaryDirectory
        
        let leftURL = tempDirectory.appendingPathComponent("LEFT")
        let rightURL = tempDirectory.appendingPathComponent("RIGHT")
        
        let sourceBuffer = AudioBuffer(count: 2048)
        for i in 0 ..< 2048 { sourceBuffer[i] = Float(i) }
        try Data(audioBuffer: sourceBuffer).write(to: leftURL)
        try Data(audioBuffer: sourceBuffer).write(to: rightURL)
        
        let source = try Source(with: [leftURL, rightURL])
        
        XCTAssertEqual(source.frameCount, 2048)
        XCTAssertEqual(source.end, 2048)
        
        let renderBuffers = [ AudioBuffer(count: 512), AudioBuffer(count: 512) ]
        source.read(into: renderBuffers, from: 0 ..< 512, to: 0)
        XCTAssertEqual(renderBuffers[0].magnitude, 0)
        XCTAssertEqual(renderBuffers[1].magnitude, 0)

        source.prepare(for: AudioDimensions(frameCount: 512, channelCount: 2))
        source.read(into: renderBuffers, from: 1024 ..< 1536, to: 0)
        XCTAssertEqual(renderBuffers[0][0], 1024)
        XCTAssertEqual(renderBuffers[0][511], 1535)
        XCTAssertEqual(renderBuffers[1][0], 1024)
        XCTAssertEqual(renderBuffers[1][511], 1535)

        try FileManager.default.removeItem(at: leftURL)
        try FileManager.default.removeItem(at: rightURL)
    }
}
