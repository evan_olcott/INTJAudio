//
//  SpectrumDimensions.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/31/24.
//

import Foundation

/// Describes a collection of spectrum buffers in resolution and channels

public struct SpectrumDimensions
{
    public let resolution: Resolution
    public let channelCount: ChannelCount
    
    public init(resolution: Resolution, channelCount: ChannelCount)
    {
        self.resolution = resolution
        self.channelCount = channelCount
    }
}
