//
//  ValueOutputBus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 8/25/24.
//

import Foundation

public final class ValueOutputBus: Bus, Equatable
{
    public let identifier: String
    public var connectedBus: Bus?
    public weak var processor: StreamProcessor?
    public var taps = [Bus]()
    
    public init(for processor: ValueOutputtable, identifier: String)
    {
        self.processor = processor
        self.identifier = identifier
    }
    
    public static func == (lhs: ValueOutputBus, rhs: ValueOutputBus) -> Bool
    {
        lhs.identifier == rhs.identifier
    }

    public func value(state: StreamState) -> Double?
    {
        guard
            let processor = processor as? ValueOutputtable
        else { return nil }
        
        let v = processor.value(for: self, state: state)
        
        if let taps = taps as? [ValueInputBus]
        {
            taps.forEach { $0.setValue(v, state: state) }
        }
        
        return v
    }

    public func setValue(_ value: Double?, state: StreamState)
    {
        if let taps = taps as? [ValueInputBus]
        {
            taps.forEach { $0.setValue(value, state: state) }
        }

        if let inputBus = connectedBus as? ValueInputBus
        {
            inputBus.setValue(value, state: state)
        }
    }
}
