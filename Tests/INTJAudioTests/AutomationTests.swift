//
//  AutomationTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/3/22.
//

@testable import INTJAudio
import XCTest

class AutomationTests: XCTestCase
{
    var automation: Automation?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        automation = Automation(for: "testing")
    }

    override func tearDown() async throws
    {
        try await super.tearDown()
        
        automation = nil
    }
    
    func testEmpty() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        XCTAssertNil(automation.value(at: 0.0))
        XCTAssertNil(automation.value(at: 10.0))
    }
    
    func testSimple() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        automation.setValue(0.7, at: 0.0)
        
        XCTAssertEqual(automation.value(at: 0.0), 0.7)
        XCTAssertEqual(automation.value(at: 1.0), 0.7)
    }
    
    func testModifiedSimple() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        automation.setValue(0.7, at: 0.5)
        
        XCTAssertEqual(automation.value(at: 0.0), 0.7)
        XCTAssertEqual(automation.value(at: 1.0), 0.7)
    }
    
    func testNegative() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        automation.setValue(0.5, at: -10.0)
        automation.setValue(0.0, at: 0.0)
        
        XCTAssertEqual(automation.value(at: -5.0), 0.25)
    }
    
    func testReplaceExistingPoint() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        automation.setValue(1.0, at: 0.0)
        automation.setValue(0.0, at: 5.0)

        XCTAssertEqual(automation.value(at: 0.0005), 0.9999)
        XCTAssertEqual(automation.value(at: 2.5), 0.5)
        
        automation.setValue(0.5, at: 0.0)
        
        XCTAssertEqual(automation.value(at: 0.0005), 0.49995)
        XCTAssertEqual(automation.value(at: 2.5), 0.25)
    }
    
    func testRampDown() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        automation.setValue(1, at: 1.0)
        automation.setValue(0, at: 3.0)
        
        XCTAssertEqual(automation.value(at: 0.0), 1)
        XCTAssertEqual(automation.value(at: 1.0), 1)
        XCTAssertEqual(automation.value(at: 2.0), 0.5)
        XCTAssertEqual(automation.value(at: 3.0), 0)
        XCTAssertEqual(automation.value(at: 4.0), 0)
    }
    
    func testRampUp() async throws
    {
        let automation = try XCTUnwrap(automation)
        
        automation.setValue(0, at: 1.0)
        automation.setValue(1, at: 3.0)
        
        XCTAssertEqual(automation.value(at: 0.0), 0)
        XCTAssertEqual(automation.value(at: 1.0), 0)
        XCTAssertEqual(automation.value(at: 2.0), 0.5)
        XCTAssertEqual(automation.value(at: 3.0), 1)
        XCTAssertEqual(automation.value(at: 4.0), 1)
    }
}
