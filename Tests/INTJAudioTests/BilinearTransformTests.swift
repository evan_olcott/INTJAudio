//
//  BilinearTransformTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class BilinearTransformTests: XCTestCase
{
    let accuracy = 1.0e-17

    func testLowPass()
    {
        do
        {
            let c = BilinearTransform.lowPassCoefficients(at: 1000, Q: 0.5)
            XCTAssertEqual(c.a0, 0.0037836976644431294, accuracy: accuracy)
            XCTAssertEqual(c.a1, 0.007567395328886259, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.0037836976644431294, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.7690877166432863, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, -6.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, -42.8, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.lowPassCoefficients(at: 99.7, Q: 0.707)
            XCTAssertEqual(c.a0, 0.00004219019596284541, accuracy: accuracy)
            XCTAssertEqual(c.a1, 0.00008438039192569082, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.00004219019596284541, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.981541304023147, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.9817100648069986, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -3.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, -40.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, -82.8, accuracy: 0.1)
        }
    }

    func testHighPass()
    {
        do
        {
            let c = BilinearTransform.highPassCoefficients(at: 1000, Q: 0.5)
            XCTAssertEqual(c.a0, 0.8807601606572, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.7615203213144, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.8807601606572, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.7690877166432863, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -40.1, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, -6.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.0, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.highPassCoefficients(at: 99.7, Q: 0.707)
            XCTAssertEqual(c.a0, 0.9908128422075364, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.9816256844150728, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.9908128422075364, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.981541304023147, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.9817100648069986, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -3.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.0, accuracy: 0.1)
        }
    }

    func testBandPass()
    {
        do
        {
            let c = BilinearTransform.bandPassCoefficients(at: 1000, Q: 0.5)
            XCTAssertEqual(c.a0, 0.11545614167835686, accuracy: accuracy)
            XCTAssertEqual(c.a1, 0, accuracy: accuracy)
            XCTAssertEqual(c.a2, -0.11545614167835686, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.7690877166432863, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -14.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, -15.4, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.bandPassCoefficients(at: 99.7, Q: 0.707)
            XCTAssertEqual(c.a0, 0.009144967596500634, accuracy: accuracy)
            XCTAssertEqual(c.a1, 0, accuracy: accuracy)
            XCTAssertEqual(c.a2, -0.009144967596500634, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.981541304023147, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.9817100648069986, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, -17.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, -38.4, accuracy: 0.1)
        }
    }

    func testNotch()
    {
        do
        {
            let c = BilinearTransform.notchCoefficients(at: 1000, Q: 0.5)
            XCTAssertEqual(c.a0, 0.8845438583216431, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.8845438583216431, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.7690877166432863, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -0.2, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, -121.3, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, -0.1, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.notchCoefficients(at: 99.7, Q: 0.707)
            XCTAssertEqual(c.a0, 0.9908550324034993, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.981541304023147, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.9908550324034993, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.981541304023147, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.9817100648069986, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -47.4, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.0, accuracy: 0.1)
        }
    }

    func testPeak()
    {
        do
        {
            let c = BilinearTransform.peakingCoefficients(at: 1000, gain: Decibel(3), Q: 0.5)
            XCTAssertEqual(c.a0, 1.047629993199606, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.7214577234436801, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.7539529259855136, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.7690877166432863, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, 0.2, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 3.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.1, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.peakingCoefficients(at: 99.7, gain: Decibel(-3), Q: 0.707)
            XCTAssertEqual(c.a0, 0.9962415368597797, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.9740937540711518, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.9780203437140382, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.9740937540711518, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.9742618805738177, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -3.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.0, accuracy: 0.1)
        }
    }

    func testLowShelf()
    {
        do
        {
            let c = BilinearTransform.lowShelfCoefficients(at: 1000, gain: Decibel(3))
            XCTAssertEqual(c.a0, 1.0175434606932452, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.8121099841506203, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.8166932272074604, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.8153410827045682, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.8310055893467576, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, 3.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 1.7, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.0, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.lowShelfCoefficients(at: 99.7, gain: Decibel(-3))
            XCTAssertEqual(c.a0, 0.9982620319064086, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.9781001802058535, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.9800066160150915, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.978065430576973, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.9783033975503806, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -1.7, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 0.0, accuracy: 0.1)
        }
    }

    func testHighShelf()
    {
        do
        {
            let c = BilinearTransform.highShelfCoefficients(at: 1000, gain: Decibel(3))
            XCTAssertEqual(c.a0, 1.3919916371491228, accuracy: accuracy)
            XCTAssertEqual(c.a1, -2.5674685341702714, accuracy: accuracy)
            XCTAssertEqual(c.a2, 1.1911414036633379, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.8153410827045682, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.8310055893467576, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, 0.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, 1.7, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, 3.0, accuracy: 0.1)
        }

        do
        {
            let c = BilinearTransform.highShelfCoefficients(at: 99.7, gain: Decibel(-3))
            XCTAssertEqual(c.a0, 0.7089827007247206, accuracy: accuracy)
            XCTAssertEqual(c.a1, -1.4048804454559154, accuracy: accuracy)
            XCTAssertEqual(c.a2, 0.6960173933727403, accuracy: accuracy)
            XCTAssertEqual(c.b1, -1.9844710546912205, accuracy: accuracy)
            XCTAssertEqual(c.b2, 0.984590703332766, accuracy: accuracy)

            XCTAssertEqual(c.magnitude(at: 100).rawValue, -1.7, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 1000).rawValue, -3.0, accuracy: 0.1)
            XCTAssertEqual(c.magnitude(at: 10000).rawValue, -3.0, accuracy: 0.1)
        }
    }
}
