//
//  DeviceManagerTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/9/22.
//

import CoreAudio
@testable import INTJAudio
import XCTest

class DeviceManagerTests: XCTestCase
{
    var mockInterface: MockAudioObjectInterface?
    
    let mockInput = MockDevice()
    let mockOutput = MockDevice()
    let mockYeti = MockDevice()
    let mockTurret = MockDevice()
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        mockInterface = MockAudioObjectInterface()
        
        mockInput.id = 12
        mockInput.identifier = "input:audio:device"
        mockInput.title = "Built-in Microphone"
        mockInput.inputChannelTitles = [ "1", "2" ]
        mockInput.isDefaultInput = true
        
        mockOutput.id = 13
        mockOutput.identifier = "output:audio:device"
        mockOutput.title = "Built-in Output"
        mockOutput.outputChannelTitles = [ "1", "2" ]
        mockOutput.frameCount = 256
        mockOutput.isDefaultOutput = true
        mockOutput.isSystemOutput = true

        mockYeti.id = 14
        mockYeti.identifier = "inout:Yeti"
        mockYeti.title = "Yeti Stereo Microphone"
        mockYeti.inputChannelTitles = [ "Front Left", "Front Right" ]
        mockYeti.outputChannelTitles = [ "Front Left", "Front Right" ]

        mockTurret.id = 15
        mockTurret.identifier = "inout:Turret"
        mockTurret.title = "Marantz TURRET Audio"
        mockTurret.inputChannelTitles = [ "Front Left", "Front Right" ]
        mockTurret.outputChannelTitles = [ "Front Left", "Front Right" ]
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        mockInterface = nil
    }

    func testStartupDeviceManager() async throws
    {
        let interface = try XCTUnwrap(mockInterface)
        let mockDevices = [ mockInput, mockOutput, mockYeti, mockTurret ]
        
        interface.mockDevices = mockDevices

        let manager = DeviceManager(with: interface)

        XCTAssertEqual(manager.devices.count, mockDevices.count)
        XCTAssertEqual(interface.calls.filter { $0.function == "addPropertyListener(to:address:handler:)" }.count, mockDevices.count + 1)
    }
    
    func testDevicesChanged() async throws
    {
        let interface = try XCTUnwrap(mockInterface)
        var handler: AudioObjectPropertyListenerBlock?
        
        interface.didCallHandler =
        { call in
            
            if call.function == "addPropertyListener(to:address:handler:)"
            {
                handler = call.arguments["handler"] as? AudioObjectPropertyListenerBlock
            }
            
            return nil
        }

        let manager = DeviceManager(with: interface)
        XCTAssertEqual(manager.devices.count, 0)
        
        // update devices
        
        interface.mockDevices = [ mockInput, mockOutput ]
                
        var address = AudioObjectPropertyAddress(mSelector: kAudioHardwarePropertyDevices,
                                                 mScope: kAudioObjectPropertyScopeGlobal,
                                                 mElement: kAudioObjectPropertyElementMain)
        handler?(1, &address)
        
        // confirm devices created
        
        XCTAssertEqual(manager.devices.count, 2)
    }
}
