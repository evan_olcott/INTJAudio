//
//  TimeLocationTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class TimeLocationTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(TimeLocation(rawValue: 4).rawValue, 4.0)
    }
    
    func testInitAsDouble()
    {
        XCTAssertEqual(TimeLocation(4.0).rawValue, 4.0)
    }
    
    func testWithSampleCount()
    {
        let location = TimeLocation(position: 200, sampleRate: Hertz(100))
        XCTAssertEqual(location.rawValue, 2.0)
    }
    
    func testZero()
    {
        XCTAssertEqual(TimeLocation.zero.rawValue, 0)
    }
    
    func testMilliseconds()
    {
        XCTAssertEqual(TimeLocation(1.0206).milliseconds, 1020.6, accuracy: 0.1)
    }
    
    func testAdvancedBy()
    {
        XCTAssertEqual(TimeLocation(1.5).advanced(by: TimeLength(2.0)).rawValue, 3.5)
    }
    
    func testRecededBy()
    {
        XCTAssertEqual(TimeLocation(7).receded(by: TimeLength(2.5)).rawValue, 4.5)
    }
    
    func testDistanceTo()
    {
        XCTAssertEqual(TimeLocation(3.5).distance(to: TimeLocation(8)).rawValue, 4.5)
        XCTAssertEqual(TimeLocation(8).distance(to: TimeLocation(3.5)).rawValue, -4.5)
    }
    
    func testAbsoluteDistanceTo()
    {
        XCTAssertEqual(TimeLocation(3.5).absoluteDistance(to: TimeLocation(8)).rawValue, 4.5)
        XCTAssertEqual(TimeLocation(8).absoluteDistance(to: TimeLocation(3.5)).rawValue, 4.5)
    }
    
    func testScaledBy()
    {
        XCTAssertEqual(TimeLocation(1.5).scaled(by: 2).rawValue, 3)
        XCTAssertEqual(TimeLocation(1.5).scaled(by: 0.5).rawValue, 0.75)
    }
    
    func testClamped()
    {
        XCTAssertEqual(TimeLocation(1.5).clamped(to: 1.0 ... 2.0), TimeLocation(1.5))
        XCTAssertEqual(TimeLocation(1.5).clamped(to: 2.0 ... 3.0), TimeLocation(2))
        XCTAssertEqual(TimeLocation(1.5).clamped(to: 0.0 ... 1.0), TimeLocation(1))
    }
    
    func testRoundedToMultiple()
    {
        XCTAssertEqual(TimeLocation(422.0).rounded(.up, toMultipleOf: 5.0), 425.0)
        XCTAssertEqual(TimeLocation(422.0).rounded(.down, toMultipleOf: 5.0), 420.0)
        XCTAssertEqual(TimeLocation(1044.72994722).rounded(.up, toMultipleOf: 6.4).rawValue, 1049.6, accuracy: 0.0001)
    }
    
    func testNormalizedRelativeTo()
    {
        XCTAssertEqual(TimeLocation(12.3).normalizedRelative(to: 10.4773 ... 15.8), 0.3424, accuracy: 0.0001)
        XCTAssertEqual(TimeLocation(10).normalizedRelative(to: 10.4773 ... 15.8), -0.0896, accuracy: 0.0001)
        XCTAssertEqual(TimeLocation(13).normalizedRelative(to: 11.0 ... 12.0), 2, accuracy: 0.0001)
    }
    
    func testEquality()
    {
        let length1 = TimeLocation(2)
        let length2 = TimeLocation(5)
        
        XCTAssertTrue(length1 == length1)
        XCTAssertFalse(length1 == length2)
    }
    
    func testComparable()
    {
        let length1 = TimeLocation(2)
        let length2 = TimeLocation(5)
        
        XCTAssertTrue(length1 < length2)
        XCTAssertFalse(length1 > length2)
        XCTAssertTrue(length2 > length1)
    }
    
    func testStrideable()
    {
        XCTAssertEqual(TimeLocation(5).advanced(by: TimeLength(7)), TimeLocation(12))
        XCTAssertEqual(TimeLocation(5).distance(to: TimeLocation(7)), TimeLength(2))
    }
    
    func testCasting()
    {
        let location = TimeLocation(25.12)
        
        XCTAssertEqual(TimeInterval(location), 25.12)
    }
    
    func testDateComponentsFormatter()
    {
        let timeFormatter = DateComponentsFormatter()
        timeFormatter.unitsStyle = .positional
        timeFormatter.allowedUnits = [ .hour, .minute, .second]
        timeFormatter.zeroFormattingBehavior = .pad

        XCTAssertEqual(timeFormatter.string(from: TimeLocation(644.12)), "00:10:44")
    }
}
