//
//  Landmark.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

/// Identifies a location in the arrangement and describes an action to be taken

public class Landmark
{
    /// The action to be taken when playback reaches this `Landmark`
    
    public indirect enum Action
    {
        case stop
        case jump(Landmark)
    }
    
    public let position: SamplePosition
    public let action: Action?
    public var isEnabled = true
    
    public init(position: SamplePosition,
                action: Action?)
    {
        self.position = position
        self.action = action
    }
    
    public init(location: TimeLocation,
                action: Action?)
    {
        self.position = SamplePosition(location: location)
        self.action = action
    }
    
    public var location: TimeLocation
    {
        TimeLocation(position: position)
    }
}
