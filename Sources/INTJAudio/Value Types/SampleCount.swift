//
//  SampleCount.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
import Foundation

public struct SampleCount: RawRepresentable, Codable, Hashable, Sendable
{
    public var rawValue: Int
    
    public init(rawValue value: Int)
    {
        rawValue = value
    }

    public init(_ value: UInt32)
    {
        rawValue = Int(value)
    }

    public init(_ value: Int)
    {
        rawValue = value
    }
    
    public init(_ value: Int32)
    {
        rawValue = Int(value)
    }
    
    public init(_ value: Int64)
    {
        rawValue = Int(value)
    }
    
    public init(_ value: Double)
    {
        rawValue = Int(value)
    }
    
    public init(_ value: CGFloat)
    {
        rawValue = Int(value)
    }
    
    public init(length: TimeLength, sampleRate: Hertz = .samplingRate)
    {
        rawValue = Int((length.rawValue * sampleRate.rawValue).rounded())
    }
    
    public init(byteCount: Int)
    {
        rawValue = Int(byteCount / MemoryLayout<Sample>.size)
    }
    
    public init(byteCount: Int64)
    {
        rawValue = Int(byteCount / Int64(MemoryLayout<Sample>.size))
    }
    
    public init(byteCount: UInt32)
    {
        rawValue = Int(byteCount / UInt32(MemoryLayout<Sample>.size))
    }
    
    public init(byteCount: UInt64)
    {
        rawValue = Int(byteCount / UInt64(MemoryLayout<Sample>.size))
    }
    
    public var byteCount: Int
    {
        rawValue * MemoryLayout<Sample>.size
    }
    
    public var half: SampleCount
    {
        divided(by: 2)
    }

    public static var zero: SampleCount { 0 }
    public static var realtime: SampleCount { 128 }
    public static var playback: SampleCount { 512 }
    public static var offline: SampleCount { 512 }
    public static var encode: SampleCount { 960 }
}

public extension SampleCount
{
    mutating func increase(by other: SampleCount)
    {
        rawValue += other.rawValue
    }
    
    func increased(by other: SampleCount) -> SampleCount
    {
        var new = self
        new.increase(by: other)
        return new
    }
    
    mutating func decrease(by other: SampleCount)
    {
        rawValue -= other.rawValue
    }
    
    func decreased(by other: SampleCount) -> SampleCount
    {
        var new = self
        new.decrease(by: other)
        return new
    }
    
    mutating func multiply(by multiplier: Int)
    {
        rawValue *= multiplier
    }
    
    func multiplied(by multiplier: Int) -> SampleCount
    {
        var new = self
        new.multiply(by: multiplier)
        return new
    }
    
    mutating func divide(by divisor: Int)
    {
        rawValue /= divisor
    }
    
    func divided(by divisor: Int) -> SampleCount
    {
        var new = self
        new.divide(by: divisor)
        return new
    }
}

extension SampleCount: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int

    public init(integerLiteral value: Int)
    {
        rawValue = value
    }
}

extension SampleCount: Equatable
{
    public static func == (lhs: SampleCount, rhs: SampleCount) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension SampleCount: Comparable
{
    public static func < (lhs: SampleCount, rhs: SampleCount) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

extension SampleCount: LosslessStringConvertible
{
    public init?(_ string: String)
    {
        guard let intValue = Int(string) else { return nil }
        rawValue = intValue
    }

    public var description: String
    {
        return String(rawValue)
    }
}

public extension Int
{
    init(_ count: SampleCount)
    {
        self.init(count.rawValue)
    }
}

public extension Int32
{
    init(_ count: SampleCount)
    {
        self.init(Int32(count.rawValue))
    }
}

public extension Int64
{
    init(_ count: SampleCount)
    {
        self.init(Int64(count.rawValue))
    }
}

public extension UInt32
{
    init(_ count: SampleCount)
    {
        self.init(UInt32(count.rawValue))
    }
}

public extension Float
{
    init(_ count: SampleCount)
    {
        self.init(Float(count.rawValue))
    }
}

public extension Double
{
    init(_ count: SampleCount)
    {
        self.init(Double(count.rawValue))
    }
}

public extension CGFloat
{
    init(_ count: SampleCount)
    {
        self.init(CGFloat(count.rawValue))
    }
}

public extension vDSP_Length
{
    init(_ count: SampleCount)
    {
        self.init(vDSP_Length(count.rawValue))
    }
}

@available(macOS 13.0, *)
public extension Duration
{
    init(_ value: SampleCount, sampleRate: Hertz = .samplingRate)
    {
        self = Duration(TimeLength(count: value, sampleRate: sampleRate))
    }
}
