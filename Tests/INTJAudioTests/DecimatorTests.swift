//
//  DecimatorTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/24/24.
//

import Foundation
@testable import INTJAudio
import Testing

struct DecimatorTests
{
/*    func fft(tone: Hertz, resolution: Resolution) -> ComplexBuffer
    {
        let sine = AudioBuffer(tone: tone, count: resolution.sampleCount)
        
        let fft = FourierTransform(resolution: resolution)
        let buffer = ComplexBuffer(resolution: resolution)
        fft.forward(sine, result: buffer)
        
        return buffer
    }
    
    //
    // MARK: - Tests
    //
    
    // total 432 tests
    
    @Test(arguments: TestingResolutions, TestingTones)
    func tone_bin(resolution: Resolution, tone: Hertz) throws
    {
        for (density, count) in TestingDensities
        {
            try tone_bin(resolution: resolution,
                         tone: tone,
                         density: density,
                         outputCount: count)
        }
    }
    
    func tone_bin(resolution: Resolution,
                  tone: Hertz,
                  density: Decimator.Density,
                  outputCount: Int) throws
    {
        let complexBuffer = fft(tone: tone, resolution: resolution)
        let polarBuffer = PolarBuffer(complexBuffer)
        let decimator = Decimator(density: density)

        #expect(decimator.frequencies.count == outputCount)

        let decimated = decimator.calculate(polarBuffer)
        let expectedUpperBin = decimator.frequencies.firstIndex { $0 > tone } ?? 0
        
        let max = decimated.max()!
        let foundMaxBin = decimated.firstIndex(of: max)!
        
        #expect(foundMaxBin <= expectedUpperBin && foundMaxBin >= expectedUpperBin - 1,
                "resolution = \(resolution), tone = \(tone), density = \(density), expected = \(expectedUpperBin - 1) ... \(expectedUpperBin), found = \(foundMaxBin)")
    }
     */
}
