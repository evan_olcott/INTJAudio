//
//  ScalarTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class ScalarTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(Scalar(rawValue: 0.75).rawValue, 0.75)
    }
    
    func testInitWithFloat()
    {
        XCTAssertEqual(Scalar(Float(0.75)).rawValue, 0.75)
    }
    
    func testInitWithDouble()
    {
        XCTAssertEqual(Scalar(Double(0.75)).rawValue, 0.75)
    }
    
    func testInitWithDecibel()
    {
        XCTAssertEqual(Scalar(Decibel(-6)).rawValue, 0.5, accuracy: 0.01)
        XCTAssertEqual(Scalar(Decibel(-120)).rawValue, 0.000001)
        XCTAssertEqual(Scalar(Decibel.negativeInfinity).rawValue, 0)
    }
    
    func testConstants()
    {
        XCTAssertEqual(Scalar.silence.rawValue, 0)
        XCTAssertEqual(Scalar.unity.rawValue, 1)
    }
    
    func testStringValue()
    {
        XCTAssertEqual(String(describing: Scalar(rawValue: 0.254883)), "0.254883")
    }
    
    func testScale()
    {
        let value = Scalar(0.5)
        let scaled = value.scaled(by: 2)
        
        XCTAssertEqual(value, 0.5)
        XCTAssertEqual(scaled, 1)
    }
    
    func testReference()
    {
        XCTAssertEqual(Scalar(0.5).toReference(2), Scalar(4))
        XCTAssertEqual(Scalar(2).toReference(0.5), Scalar(0.25))
    }
    
    func testEquality()
    {
        let scalar1 = Scalar(0.2)
        let scalar2 = Scalar(0.5)
        
        XCTAssertTrue(scalar1 == scalar1)
        XCTAssertFalse(scalar1 == scalar2)
    }
    
    func testComparable()
    {
        let scalar1 = Scalar(0.2)
        let scalar2 = Scalar(0.5)

        XCTAssertTrue(scalar1 < scalar2)
        XCTAssertFalse(scalar1 > scalar2)
        XCTAssertTrue(scalar2 > scalar1)
    }
    
    func testCasting()
    {
        let scalar = Scalar(0.2)

        XCTAssertEqual(Float(scalar), 0.2, accuracy: 0.01)
        XCTAssertEqual(CGFloat(scalar), 0.2, accuracy: 0.01)
    }
}
