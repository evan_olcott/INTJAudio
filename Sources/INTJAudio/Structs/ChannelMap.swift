//
//  ChannelMap.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//

import Foundation

public struct ChannelMap
{
    public struct Pair
    {
        let inChannel: ChannelIndex
        let outChannel: ChannelIndex
    }
    
    let pairs: [Pair]
    
    public init(with pairs: [Pair])
    {
        self.pairs = pairs
    }
    
    public init?(forInChannelCount inCount: ChannelCount,
                 outChannelCount outCount: ChannelCount)
    {
        switch (inCount, outCount)
        {
        case (1, 1):
            pairs = [ Pair(inChannel: 0, outChannel: 0) ]
        case (1, 2):
            pairs = [
                Pair(inChannel: 0, outChannel: 0),
                Pair(inChannel: 0, outChannel: 1)
            ]
        case (2, _):
            pairs = [
                Pair(inChannel: 0, outChannel: 0),
                Pair(inChannel: 1, outChannel: 1)
            ]
        default:
            return nil
        }
    }
    
    public func forEach(_ body: (ChannelIndex, ChannelIndex) throws -> Void) rethrows
    {
        try pairs.forEach { try body($0.inChannel, $0.outChannel) }
    }
}
