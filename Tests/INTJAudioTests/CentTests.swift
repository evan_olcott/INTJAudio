//
//  CentTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

@testable import INTJAudio
import XCTest

class CentTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(Cent(rawValue: 4).rawValue, 4)
    }
    
    func testInitAsInt()
    {
        XCTAssertEqual(Cent(Int(4)).rawValue, 4)
    }

    func testInitAsDouble()
    {
        XCTAssertEqual(Cent(4.2).rawValue, 4)
    }

    func testMagnitude()
    {
        XCTAssertEqual(Cent(12).magnitude.rawValue, 12)
        XCTAssertEqual(Cent(-12).magnitude.rawValue, 12)
    }
    
    func testEquality()
    {
        let count1 = Cent(2)
        let count2 = Cent(5)
        
        XCTAssertTrue(count1 == count1)
        XCTAssertFalse(count1 == count2)
    }
    
    func testComparable()
    {
        let count1 = Cent(2)
        let count2 = Cent(5)

        XCTAssertTrue(count1 < count2)
        XCTAssertFalse(count1 > count2)
        XCTAssertTrue(count2 > count1)
    }
    
    func testNegate()
    {
        XCTAssertEqual(Cent(4).negated().rawValue, -4)
        XCTAssertEqual(Cent(-4).negated().rawValue, 4)
    }
}
