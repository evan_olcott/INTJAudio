//
//  AudioDevice.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/1/22.
//

import CoreAudio
import Foundation
import Collections
import OSLog

public protocol DeviceProtocol: AnyObject
{
    var id: AudioObjectID { get }
    var title: String { get }
    var identifier: String { get }

    var isDefaultInput: Bool { get }
    var isDefaultOutput: Bool { get }
    var isSystemOutput: Bool { get }
    
    var inputChannelTitles: [String] { get }
    var outputChannelTitles: [String] { get }

    var frameCount: SampleCount { get }
    func setFrameCount(_ count: SampleCount) throws

    var isAggregate: Bool { get }
    var subdeviceIdentifiers: OrderedSet<String>? { get set }
}

extension DeviceProtocol
{
    public var hasInputs: Bool
    {
        inputChannelTitles.isEmpty == false
    }
    
    public var hasOutputs: Bool
    {
        outputChannelTitles.isEmpty == false
    }
    
    public var inputDimensions: AudioDimensions
    {
        .init(frameCount: frameCount,
              channelCount: ChannelCount(inputChannelTitles.count))
    }
    
    public var outputDimensions: AudioDimensions
    {
        .init(frameCount: frameCount,
              channelCount: ChannelCount(outputChannelTitles.count))
    }
}

@Observable public final class Device: DeviceProtocol
{
    public let id: AudioObjectID
    public let title: String
    public let identifier: String
    
    public var inputChannelTitles = [String]()
    public var outputChannelTitles = [String]()
    
    public var frameCount: SampleCount = .zero
    
    private let interface: AudioObjectInterfaceProtocol
    private var handlerID: AudioDeviceIOProcID?
    
    internal init(id: AudioObjectID,
                  interface: AudioObjectInterfaceProtocol = AudioObjectInterface()) throws
    {
        guard id != 0 else { throw AudioObjectInterface.Error.badObject }
        
        self.id = id
        self.interface = interface
        
        self.title = try interface.firstValue(of: kAudioObjectPropertyName, on: id, as: CFString.self) as String
        self.identifier = try interface.firstValue(of: kAudioDevicePropertyDeviceUID, on: id, as: CFString.self) as String
        
        self.inputChannelTitles = try interface.channelTitles(id: id, scope: kAudioDevicePropertyScopeInput)
        self.outputChannelTitles = try interface.channelTitles(id: id, scope: kAudioDevicePropertyScopeOutput)
        self.frameCount = SampleCount(try interface.firstValue(of: kAudioDevicePropertyBufferFrameSize,
                                                               in: kAudioDevicePropertyScopeOutput,
                                                               on: id,
                                                               as: Int32.self))
        
        try addObserver(for: kAudioDevicePropertyStreamConfiguration) { [weak self] in
            guard let self else { return }

            do
            {
                let inputs = try interface.channelTitles(id: id, scope: kAudioDevicePropertyScopeInput)
                let outputs = try interface.channelTitles(id: id, scope: kAudioDevicePropertyScopeOutput)
                
                if inputs.isEmpty == false, inputs != inputChannelTitles {
                    inputChannelTitles = inputs
                }
                
                if outputs.isEmpty == false, outputs != outputChannelTitles {
                    outputChannelTitles = outputs
                }
            }
            catch
            {
                Logger.hardware.error("Getting channel titles failed: \(error.localizedDescription)")
            }
        }
    }
    
    public init(inputs: ChannelCount, outputs: ChannelCount)
    {
        id = 0
        title = "Preview Device"
        identifier = "preview-device"
        
        inputChannelTitles = inputs.map { "C\($0.rawValue)" }
        outputChannelTitles = outputs.map { "C\($0.rawValue)" }
        frameCount = 512
        
        interface = PreviewAudioObjectInterface()
    }
    
    deinit
    {
        try? removeObserver(for: kAudioDevicePropertyStreamConfiguration)
    }
    
    public var isDefaultInput: Bool
    {
        id == deviceID(of: kAudioHardwarePropertyDefaultInputDevice)
    }
    
    public var isDefaultOutput: Bool
    {
        id == deviceID(of: kAudioHardwarePropertyDefaultOutputDevice)
    }
    
    public var isSystemOutput: Bool
    {
        id == deviceID(of: kAudioHardwarePropertyDefaultSystemOutputDevice)
    }

    //
    // MARK: - Dimensions
    //
    
    public func setFrameCount(_ count: SampleCount) throws
    {
        func set(scope: AudioObjectPropertyScope) throws
        {
            try interface.setValue(of: kAudioDevicePropertyBufferFrameSize,
                                   in: scope,
                                   to: Int(count),
                                   on: id)
        }
        
        try set(scope: kAudioObjectPropertyScopeInput)
        try set(scope: kAudioObjectPropertyScopeOutput)
        frameCount = count
    }

    //
    // MARK: - Aggregate
    //
    
    public var isAggregate: Bool
    {
        interface.isAggregateDevice(id: id)
    }

    public var subdeviceIdentifiers: OrderedSet<String>?
    {
        get
        {
            self.access(keyPath: \.subdeviceIdentifiers)
            
            guard interface.isAggregateDevice(id: id) else { return nil }
            
            do
            {
                var address = AudioObjectPropertyAddress(mSelector: kAudioAggregateDevicePropertyComposition,
                                                         mScope: kAudioObjectPropertyScopeGlobal,
                                                         mElement: kAudioObjectPropertyElementMain)
                
                var composition: CFDictionary?
                var size = UInt32(MemoryLayout<CFDictionary>.size)
                
                try withUnsafeMutablePointer(to: &composition) { ptr in
                    try interface.propertyData(of: id,
                                               address: &address,
                                               dataSize: &size,
                                               outData: ptr)
                }
                
                guard
                    let structure = composition as? [String: Any],
                    let subdevices = structure["subdevices"] as? [[String: Any]]
                else { return nil }
                
                return OrderedSet<String>(subdevices.compactMap { $0["uid"] as? String })
            }
            catch
            {
                Logger.hardware.error("subdeviceIdentifiers.get error: \(error.localizedDescription)")
                return nil
            }
        }
        
        set
        {
            self.withMutation(keyPath: \.subdeviceIdentifiers)
            {
                guard
                    interface.isAggregateDevice(id: id),
                    let identifiers = newValue
                else { return }
                
                do
                {
                    var address = AudioObjectPropertyAddress(mSelector: kAudioAggregateDevicePropertyComposition,
                                                             mScope: kAudioObjectPropertyScopeGlobal,
                                                             mElement: kAudioObjectPropertyElementMain)
                    
                    var desc = try interface.aggregateDeviceDescription(title: title,
                                                                        identifier: identifier,
                                                                        subdeviceIdentifiers: Array(identifiers)) as CFDictionary
                    let size = UInt32(MemoryLayout<CFDictionary>.size)
                    
                    try withUnsafePointer(to: &desc) { p in
                        try interface.setPropertyData(of: id,
                                                      address: &address,
                                                      dataSize: size,
                                                      data: p)
                    }
                }
                catch
                {
                    Logger.hardware.error("subdeviceIdentifiers.set error: \(error.localizedDescription)")
                }
            }
        }
    }
    
    //
    // MARK: - Private
    //

    private func deviceID(of selector: AudioObjectPropertySelector) -> AudioObjectID
    {
        var address = AudioObjectPropertyAddress(mSelector: selector,
                                                 mScope: kAudioObjectPropertyScopeGlobal,
                                                 mElement: kAudioObjectPropertyElementMain)
        var size = UInt32(MemoryLayout<AudioObjectID>.size)
        var deviceID = AudioObjectID(0)
        
        do
        {
            try interface.propertyData(of: AudioObjectID(kAudioObjectSystemObject),
                                       address: &address,
                                       dataSize: &size,
                                       outData: &deviceID)
            
            return deviceID
        }
        catch
        {
            return 0
        }
    }
    
    private var observers = [AudioObjectPropertySelector: (() -> Void)]()
    
    private func addObserver(for selector: AudioObjectPropertySelector,
                             _ handler: @escaping (() -> Void)) throws
    {
        var address = AudioObjectPropertyAddress(mSelector: selector,
                                                 mScope: kAudioObjectPropertyScopeWildcard,
                                                 mElement: kAudioObjectPropertyElementWildcard)
        
        try interface.addPropertyListener(to: id, address: &address, handler: observer)
        observers[selector] = handler
    }
    
    private func removeObserver(for selector: AudioObjectPropertySelector) throws
    {
        var address = AudioObjectPropertyAddress(mSelector: selector,
                                                 mScope: kAudioObjectPropertyScopeWildcard,
                                                 mElement: kAudioObjectPropertyElementWildcard)
        
        try interface.removePropertyListener(to: id, address: &address, handler: observer)
        observers[selector] = nil
    }
    
    private func observer(_ inNumberAddresses: UInt32, _ inAddresses: UnsafePointer<AudioObjectPropertyAddress>)
    {
        let buffer = UnsafeBufferPointer(start: inAddresses, count: Int(inNumberAddresses))
        let addresses = [AudioObjectPropertyAddress](buffer)
        
        addresses.forEach {
            if let handler = observers[$0.mSelector] { handler() }
        }
    }
}

