//
//  MockFunctionCall.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 11/4/22.
//

import Foundation

struct MockFunctionCall
{
    let date = Date.timeIntervalSinceReferenceDate
    let function: String
    let arguments: [String: Any?]
}
