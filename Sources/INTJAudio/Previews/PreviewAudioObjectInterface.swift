//
//  PreviewAudioObjectInterface.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import CoreAudio
import Foundation

struct PreviewAudioObjectInterface: AudioObjectInterfaceProtocol
{
    func hasProperty(with ID: AudioObjectID, address: UnsafePointer<AudioObjectPropertyAddress>) -> Bool { false }
    func propertyDataSize(of ID: AudioObjectID, address: UnsafePointer<AudioObjectPropertyAddress>, outDataSize: UnsafeMutablePointer<UInt32>) throws { }
    func propertyData(of ID: AudioObjectID, address: UnsafePointer<AudioObjectPropertyAddress>, dataSize: UnsafeMutablePointer<UInt32>, outData: UnsafeMutableRawPointer) throws { }
    func setPropertyData(of ID: AudioObjectID, address: UnsafePointer<AudioObjectPropertyAddress>, dataSize: UInt32, data: UnsafeRawPointer) throws { }
    
    func addPropertyListener(to ID: AudioObjectID, address: UnsafePointer<AudioObjectPropertyAddress>, handler: @escaping AudioObjectPropertyListenerBlock) throws { }
    func removePropertyListener(to ID: AudioObjectID, address: UnsafePointer<AudioObjectPropertyAddress>, handler: @escaping AudioObjectPropertyListenerBlock) throws { }
    
    func createAggregateDevice(description: CFDictionary, outDeviceID: UnsafeMutablePointer<AudioObjectID>) throws { }
    func destroyAggregateDevice(id: AudioObjectID) throws { }
    
    func createIOProcID(id: AudioObjectID, handler: AudioDeviceIOProc, clientData: UnsafeMutableRawPointer?, handlerID: UnsafeMutablePointer<AudioDeviceIOProcID?>) throws { }
    func destroyIOProcID(id: AudioObjectID, handlerID: AudioDeviceIOProcID) throws { }
    
    func start(id: AudioObjectID, handlerID: AudioDeviceIOProcID?) throws { }
    func stop(id: AudioObjectID, handlerID: AudioDeviceIOProcID?) throws { }
    func reset() throws { }
}
