//
//  Scalar.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import CoreGraphics
import Foundation

public struct Scalar: RawRepresentable, Codable, Sendable
{
    public var rawValue: Float
    
    public init(rawValue v: Float)
    {
        rawValue = v
    }
    
    public init(_ v: Float)
    {
        rawValue = v
    }
    
    public init(_ v: Double)
    {
        rawValue = Float(v)
    }
    
    public init(_ decibel: Decibel)
    {
        if decibel == .negativeInfinity
        {
            rawValue = 0
        }
        else
        {
            rawValue = Float(pow(10.0, decibel.rawValue / 20))
        }
    }
    
    public static var silence: Scalar { Scalar(rawValue: 0.0) }
    public static var unity: Scalar { Scalar(rawValue: 1.0) }
    
    public mutating func scale(by amount: Scalar)
    {
        rawValue *= amount.rawValue
    }
    
    public func scaled(by amount: Scalar) -> Scalar
    {
        var new = self
        new.scale(by: amount)
        return new
    }

    /// the scalar necessary to apply to the receiver to match the reference

    public func toReference(_ other: Scalar) -> Scalar
    {
        Scalar(other.rawValue / rawValue)
    }
    
    
    public func interpolate<T: BinaryFloatingPoint>(to other: Scalar, amount: T) -> Scalar
    {
        Scalar(other.rawValue + (Float(amount) * (rawValue - other.rawValue)))
    }
}

extension Scalar: CustomStringConvertible
{
    public var description: String
    {
        String(rawValue)
    }
}

extension Scalar: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int
    
    public init(integerLiteral value: Int)
    {
        self.rawValue = Float(value)
    }
}

extension Scalar: ExpressibleByFloatLiteral
{
    public typealias FloatLiteralType = Float

    public init(floatLiteral value: Float)
    {
        self.rawValue = value
    }
}

extension Scalar: Equatable
{
    public static func == (lhs: Scalar, rhs: Scalar) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension Scalar: Comparable
{
    public static func < (lhs: Scalar, rhs: Scalar) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

public extension Float
{
    init(_ v: Scalar)
    {
        self.init(v.rawValue)
    }
}

public extension Double
{
    init(_ v: Scalar)
    {
        self.init(v.rawValue)
    }
}

public extension CGFloat
{
    init(_ v: Scalar)
    {
        self.init(v.rawValue)
    }
}
