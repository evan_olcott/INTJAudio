//
//  AudioUnitInterface.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/8/22.
//

import AudioToolbox
import AVFoundation
import Foundation
import OSLog

// swiftlint:disable function_parameter_count

protocol AudioUnitInterfaceProtocol
{
    func instantiate(with description: AudioComponentDescription,
                     options: AudioComponentInstantiationOptions) async throws -> AVAudioUnit
    
    func initialize(with unit: AudioUnit) throws
    func uninitialize(with unit: AudioUnit) throws
    
    func renderResourcesAllocated(for unit: AUAudioUnit) -> Bool
    func allocateRenderResources(for unit: AUAudioUnit) throws
    func deallocateRenderResources(for unit: AUAudioUnit)
    
    func setFormat(of unit: AUAudioUnit, onInputBus bus: Int, to format: AVAudioFormat) throws
    func setFormat(of unit: AUAudioUnit, onOutputBus bus: Int, to format: AVAudioFormat) throws
    func setMaximumFramesToRender(of unit: AUAudioUnit, to count: AUAudioFrameCount) throws

    func propertyDataInfo(of unit: AudioUnit,
                          property: AudioUnitPropertyID,
                          scope: AudioUnitScope,
                          element: AudioUnitElement,
                          outDataSize: UnsafeMutablePointer<UInt32>?,
                          outWritable: UnsafeMutablePointer<DarwinBoolean>?) throws
    
    func propertyData(of unit: AudioUnit,
                      property: AudioUnitPropertyID,
                      scope: AudioUnitScope,
                      element: AudioUnitElement,
                      outData: UnsafeMutableRawPointer,
                      ioDataSize: UnsafeMutablePointer<UInt32>) throws
    
    func setPropertyData(of unit: AudioUnit,
                         property: AudioUnitPropertyID,
                         scope: AudioUnitScope,
                         element: AudioUnitElement,
                         inData: UnsafeRawPointer?,
                         inDataSize: UInt32) throws
    
    func render(_ unit: AudioUnit,
                flags: inout AudioUnitRenderActionFlags,
                timestamp: inout AudioTimeStamp,
                bus: UInt32,
                frameCount: UInt32,
                buffer: UnsafeMutablePointer<AudioBufferList>) -> OSStatus
}

struct AudioUnitInterface: AudioUnitInterfaceProtocol
{
    enum Error: Swift.Error
    {
        case unsupportedFormat
        case isInitialized
        case isUninitialized
        case badProperty
        case badPropertyValue
        case propertyNotWritable
        case propertyNotInUse
        case badScope
        case badElement
        case badParameterValue
        case unknown(OSStatus)
    }
    
    func error(for code: OSStatus) -> Error
    {
        switch code
        {
        case kAudioUnitErr_FormatNotSupported:
            return .unsupportedFormat
        case kAudioUnitErr_Initialized:
            return .isInitialized
        case kAudioUnitErr_Uninitialized:
            return .isUninitialized
        case kAudioUnitErr_InvalidProperty:
            return .badProperty
        case kAudioUnitErr_InvalidPropertyValue:
            return .badPropertyValue
        case kAudioUnitErr_PropertyNotWritable:
            return .propertyNotWritable
        case kAudioUnitErr_PropertyNotInUse:
            return .propertyNotInUse
        case kAudioUnitErr_InvalidScope:
            return .badScope
        case kAudioUnitErr_InvalidElement:
            return .badElement
        case kAudioUnitErr_InvalidParameterValue:
            return .badParameterValue
        default:
            return .unknown(code)
        }
    }

    func instantiate(with description: AudioComponentDescription,
                     options: AudioComponentInstantiationOptions) async throws -> AVAudioUnit
    {
        try await AVAudioUnit.instantiate(with: description, options: options)
    }

    func initialize(with unit: AudioUnit) throws
    {
        let err = AudioUnitInitialize(unit)
        if err != noErr
        {
            Logger.hardware.info("AudioUnitInitialize failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }
    
    func uninitialize(with unit: AudioUnit) throws
    {
        let err = AudioUnitUninitialize(unit)
        if err != noErr
        {
            Logger.hardware.info("AudioUnitUninitialize failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }
    
    func renderResourcesAllocated(for unit: AUAudioUnit) -> Bool
    {
        unit.renderResourcesAllocated
    }
    
    func allocateRenderResources(for unit: AUAudioUnit) throws
    {
        try unit.allocateRenderResources()
    }
    
    func deallocateRenderResources(for unit: AUAudioUnit)
    {
        unit.deallocateRenderResources()
    }
    
    func setFormat(of unit: AUAudioUnit, onInputBus bus: Int, to format: AVAudioFormat) throws
    {
        try unit.inputBusses[bus].setFormat(format)
    }
    
    func setFormat(of unit: AUAudioUnit, onOutputBus bus: Int, to format: AVAudioFormat) throws
    {
        try unit.outputBusses[bus].setFormat(format)
    }
    
    func setMaximumFramesToRender(of unit: AUAudioUnit, to count: AUAudioFrameCount)
    {
        unit.maximumFramesToRender = count
    }

    func propertyDataInfo(of unit: AudioUnit,
                          property: AudioUnitPropertyID,
                          scope: AudioUnitScope,
                          element: AudioUnitElement,
                          outDataSize: UnsafeMutablePointer<UInt32>?,
                          outWritable: UnsafeMutablePointer<DarwinBoolean>?) throws
    {
        let err = AudioUnitGetPropertyInfo(unit,
                                           property,
                                           scope,
                                           element,
                                           outDataSize,
                                           outWritable)
        if err != noErr
        {
            Logger.hardware.info("AudioUnitGetPropertyInfo failed: \(err)")
            throw error(for: err)
        }
    }
    
    func propertyData(of unit: AudioUnit,
                      property: AudioUnitPropertyID,
                      scope: AudioUnitScope,
                      element: AudioUnitElement,
                      outData: UnsafeMutableRawPointer,
                      ioDataSize: UnsafeMutablePointer<UInt32>) throws
    {
        let err = AudioUnitGetProperty(unit,
                                       property,
                                       scope,
                                       element,
                                       outData,
                                       ioDataSize)
        if err != noErr
        {
            Logger.hardware.info("AudioUnitGetProperty failed: \(err)")
            throw error(for: err)
        }
    }
    
    func setPropertyData(of unit: AudioUnit,
                         property: AudioUnitPropertyID,
                         scope: AudioUnitScope,
                         element: AudioUnitElement,
                         inData: UnsafeRawPointer?,
                         inDataSize: UInt32) throws
    {
        let err = AudioUnitSetProperty(unit,
                                       property,
                                       scope,
                                       element,
                                       inData,
                                       inDataSize)
        if err != noErr
        {
            Logger.hardware.info("AudioUnitSetProperty failed: \(err)")
            throw error(for: err)
        }
    }
    
    func render(_ unit: AudioUnit,
                flags: inout AudioUnitRenderActionFlags,
                timestamp: inout AudioTimeStamp,
                bus: UInt32,
                frameCount: UInt32,
                buffer: UnsafeMutablePointer<AudioBufferList>) -> OSStatus
    {
        return AudioUnitRender(unit,
                               &flags,
                               &timestamp,
                               bus,
                               frameCount,
                               buffer)
    }
}
