//
//  MockDevice.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/9/22.
//

import Collections
import CoreAudio
import Foundation
import INTJAudio

class MockDevice: DeviceProtocol
{
    var id: AudioObjectID = 0
    var title = ""
    var identifier = ""

    var isDefaultInput = false
    var isDefaultOutput = false
    var isSystemOutput = false
    
    var inputChannelTitles = [String]()
    var outputChannelTitles = [String]()

    var frameCount: SampleCount = 512
    func setFrameCount(_ count: SampleCount) throws { }
    
    var isAggregate = false
    var subdeviceIdentifiers: OrderedSet<String>?
}
