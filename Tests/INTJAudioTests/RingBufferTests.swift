//
//  RingBufferTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
@testable import INTJAudio
import XCTest

class RingBufferTests: XCTestCase
{
    var source: AudioBuffer?
    var output: AudioBuffer?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        source = AudioBuffer(count: SampleCount(250))
        output = AudioBuffer(count: SampleCount(250))

        let source = try XCTUnwrap(source)
        vDSP.formRamp(in: 0 ... 249, result: &source.rawValue)
    }
    
    override func tearDown() async throws
    {
        output = nil
        source = nil
        
        try await super.tearDown()
    }
    
    func testNormal() async throws
    {
        let source = try XCTUnwrap(source)
        let output = try XCTUnwrap(output)
        
        let ring = RingBuffer(count: SampleCount(400))
        
        try ring.store(source, at: SamplePosition(0))
        try ring.store(source, at: SamplePosition(250))
        try ring.retrieve(into: output, from: SamplePosition(250))
        
        XCTAssertEqual(output.rawValue[0], 0)
        XCTAssertEqual(output.rawValue[120], 120)
        XCTAssertEqual(output.rawValue[249], 249)
        
        try ring.retrieve(into: output, from: SamplePosition(250))

        XCTAssertEqual(output.rawValue[0], 0)
        XCTAssertEqual(output.rawValue[120], 0)
        XCTAssertEqual(output.rawValue[249], 0)
    }
    
    func testStoreAndReadNegative() async throws
    {
        let source = try XCTUnwrap(source)
        let output = try XCTUnwrap(output)
        
        let ring = RingBuffer(count: SampleCount(400))
        
        try ring.store(source, at: SamplePosition(-150))
        try ring.retrieve(into: output, from: SamplePosition(-150))
        
        XCTAssertEqual(output.rawValue[0], 0)
        XCTAssertEqual(output.rawValue[120], 120)
        XCTAssertEqual(output.rawValue[249], 249)
    }
    
    func testStoreAndReadLargeNumber() async throws
    {
        let source = try XCTUnwrap(source)
        let output = try XCTUnwrap(output)
        
        let ring = RingBuffer(count: SampleCount(400))
        
        try ring.store(source, at: SamplePosition(2763222))
        try ring.retrieve(into: output, from: SamplePosition(2763222))
        
        XCTAssertEqual(output.rawValue[0], 0)
        XCTAssertEqual(output.rawValue[120], 120)
        XCTAssertEqual(output.rawValue[249], 249)
    }
    
    func testTooLarge() async throws
    {
        let source = try XCTUnwrap(source)
        let output = try XCTUnwrap(output)
        
        let ring = RingBuffer(count: SampleCount(150))
        
        XCTAssertThrowsError(try ring.store(source, at: .zero))
        XCTAssertThrowsError(try ring.retrieve(into: output, from: .zero))
    }
}
