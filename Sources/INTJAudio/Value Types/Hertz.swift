//
//  Hertz.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import AVFoundation
import Foundation

public struct Hertz: RawRepresentable, Codable, LosslessStringConvertible, Sendable
{
    public let rawValue: Double
    
    public init(rawValue value: Double)
    {
        rawValue = value
    }

    public init(_ value: Double)
    {
        rawValue = value
    }
    
    public init(_ value: Float)
    {
        rawValue = Double(value)
    }

    public init(_ value: Semitone)
    {
        rawValue = 440 * pow(2, ((value.rawValue - 69) / 12))
    }
    
    public init?(_ value: String)
    {
        guard let doubleValue = Double(value) else { return nil }
        rawValue = doubleValue
    }
    
    public var description: String
    {
        String(rawValue)
    }

    public static var samplingRate: Hertz
    {
        48000
    }
    
    public var nyquist: Hertz
    {
        Hertz(rawValue / 2)
    }
}

extension Hertz: Equatable
{
    public static func == (lhs: Hertz, rhs: Hertz) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension Hertz: Comparable
{
    public static func < (lhs: Hertz, rhs: Hertz) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

extension Hertz: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int
    
    public init(integerLiteral value: Int)
    {
        rawValue = Double(value)
    }
}

extension Hertz: ExpressibleByFloatLiteral
{
    public typealias FloatLiteralType = Double
    
    public init(floatLiteral value: Double)
    {
        rawValue = value
    }
}

public extension Double
{
    init(_ value: Hertz)
    {
        self.init(value.rawValue)
    }
}

public extension Int
{
    init(_ value: Hertz)
    {
        self.init(value.rawValue)
    }
}

public extension CMTimeScale
{
    init(_ value: Hertz)
    {
        self.init(value.rawValue)
    }
}
