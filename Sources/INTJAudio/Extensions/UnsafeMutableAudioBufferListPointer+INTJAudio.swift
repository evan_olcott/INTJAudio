//
//  UnsafeMutableAudioBufferListPointer+INTJAudio.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
import AVFoundation
import Foundation
import OSLog

public extension UnsafeMutableAudioBufferListPointer
{
    /// The number of buffers.
        
    var channelCount: ChannelCount
    {
        let total = (0 ..< count).reduce(0) { $0 + Int(self[$1].mNumberChannels) }
        return ChannelCount(total)
    }
    
    /// The number of frames.
    
    var frameCount: SampleCount
    {
        var frameCount: SampleCount?

        forEach
        {
            let audioBufferFrames = $0.mDataByteSize / (UInt32(MemoryLayout<Float>.size) * $0.mNumberChannels)
            if let channelFrames = frameCount
            {
                frameCount = Swift.min(channelFrames, SampleCount(audioBufferFrames))
            }
            else
            {
                frameCount = SampleCount(audioBufferFrames)
            }
        }

        return frameCount ?? 0
    }

    //
    // MARK: - Process
    //
    
    /// Sets the values of all the samples to 0 (silence).

    func clear()
    {
        Logger.stream.debug("UnsafeMutableAudioBufferListPointer.clear()")
        
        for index in 0 ..< count
        {
            memset(self[index].mData, 0, Int(self[index].mDataByteSize))
        }
    }
    
    /// Copies the contents of an array of `AudioBuffer`.
    ///
    /// - Parameter samples: The samples to copy
    
    func copy(from buffers: [AudioBuffer])
    {
        Logger.stream.debug("UnsafeMutableAudioBufferListPointer.copy(from:)")

        assert(Int(channelCount) == buffers.count, "Channel count mismatch")
        assert(frameCount == buffers.frameCount, "Frame count mismatch")

        var channelIndex = 0
        for bufferIndex in 0 ..< count
        {
            let numberOfChannels = self[bufferIndex].mNumberChannels
            for bufferChannelIndex in 0 ..< numberOfChannels
            {
                guard let bufferData = self[bufferIndex].mData else { return }

                let bufferSamples = bufferData.advanced(by: Int(bufferChannelIndex) * MemoryLayout<Float>.size).assumingMemoryBound(to: Float.self)

                if channelIndex < buffers.count
                {
                    vDSP_vclr(bufferSamples, vDSP_Stride(numberOfChannels),
                              vDSP_Length(buffers.frameCount))

                    vDSP_vadd(buffers[channelIndex].pointer, 1,
                              bufferSamples, vDSP_Stride(numberOfChannels),
                              bufferSamples, vDSP_Stride(numberOfChannels),
                              vDSP_Length(buffers.frameCount))

                    channelIndex += 1
                }
            }
        }
    }
}
