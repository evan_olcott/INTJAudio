//
//  TimeLengthTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class TimeLengthTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(TimeLength(rawValue: 4).rawValue, 4.0)
    }
    
    func testInitAsInt()
    {
        XCTAssertEqual(TimeLength(Int(4)).rawValue, 4.0)
    }

    func testInitAsDouble()
    {
        XCTAssertEqual(TimeLength(4.0).rawValue, 4.0)
    }
    
    func testWithSampleCount()
    {
        let length = TimeLength(count: 200, sampleRate: Hertz(100))
        XCTAssertEqual(length.rawValue, 2.0)
    }
    
    func testMilliseconds()
    {
        XCTAssertEqual(TimeLength(1.0206).milliseconds, 1020.6, accuracy: 0.1)
    }
    
    func testIncreasedBy()
    {
        XCTAssertEqual(TimeLength(1.5).increased(by: TimeLength(2.5)).rawValue, 4)
    }
    
    func testDecreasedBy()
    {
        XCTAssertEqual(TimeLength(2.5).decreased(by: TimeLength(1.5)).rawValue, 1)
    }
    
    func testDividedBy()
    {
        XCTAssertEqual(TimeLength(32).divided(by: 4).rawValue, 8)
    }
    
    func testScaledBy()
    {
        XCTAssertEqual(TimeLength(1.5).scaled(by: 2).rawValue, 3)
        XCTAssertEqual(TimeLength(1.5).scaled(by: 0.5).rawValue, 0.75)
    }
    
    func testEquality()
    {
        let length1 = TimeLength(2)
        let length2 = TimeLength(5)
        
        XCTAssertTrue(length1 == length1)
        XCTAssertFalse(length1 == length2)
    }
    
    func testComparable()
    {
        let length1 = TimeLength(2)
        let length2 = TimeLength(5)
        
        XCTAssertTrue(length1 < length2)
        XCTAssertFalse(length1 > length2)
        XCTAssertTrue(length2 > length1)
    }
    
    func testCasting()
    {
        let length = TimeLength(25.12)
        
        XCTAssertEqual(Int(length), 25)
        XCTAssertEqual(Float(length), Float(25.12))
        XCTAssertEqual(TimeInterval(length), 25.12)
    }
    
    func testDateComponentsFormatter()
    {
        let timeFormatter = DateComponentsFormatter()
        timeFormatter.unitsStyle = .positional
        timeFormatter.allowedUnits = [ .hour, .minute, .second]
        timeFormatter.zeroFormattingBehavior = .pad

        XCTAssertEqual(timeFormatter.string(from: TimeLength(644.12)), "00:10:44")
    }
}
