//
//  DeviceManager.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/5/22.
//

import CoreAudio
import Foundation
import OSLog

protocol DeviceManagerProtocol: AnyObject
{
    var devices: [DeviceProtocol] { get }
}

@Observable public final class DeviceManager: DeviceManagerProtocol
{
    nonisolated(unsafe) public static let shared = DeviceManager()
    
    // TODO: is it possible that `devices` is getting called from multiple threads? Do I need to lock this down?
    public var devices = [DeviceProtocol]()
    
    internal let interface: AudioObjectInterfaceProtocol
    
    init(with interface: AudioObjectInterfaceProtocol = AudioObjectInterface())
    {
        self.interface = interface
        
        do
        {
            try updateDevices()
            try addDevicesObserver()
        }
        catch
        {
            Logger.hardware.error("DeviceManager init failed: \(error.localizedDescription)")
        }
    }
    
    //
    // MARK: - Devices
    //
    
    private func addDevicesObserver() throws
    {
        var address = AudioObjectPropertyAddress(mSelector: kAudioHardwarePropertyDevices,
                                                 mScope: kAudioObjectPropertyScopeGlobal,
                                                 mElement: kAudioObjectPropertyElementMain)
        
        try interface.addPropertyListener(to: AudioObjectID(kAudioObjectSystemObject),
                                          address: &address)
        { [weak self] inNumberAddresses, inAddresses in
            
            let addresses = (0 ..< Int(inNumberAddresses)).map { inAddresses[$0] }
            self?.devicesChanged(addresses)
        }
    }
    
    private func devicesChanged(_ addresses: [AudioObjectPropertyAddress])
    {
        guard addresses.contains(where: { $0.mSelector == kAudioHardwarePropertyDevices }) else { return }
        
        do
        {
            try updateDevices()
        }
        catch
        {
            Logger.hardware.error("DeviceManager updateDevices failed: \(error.localizedDescription)")
        }
    }
    
    private func updateDevices() throws
    {
        var address = AudioObjectPropertyAddress(mSelector: kAudioHardwarePropertyDevices,
                                                 mScope: kAudioObjectPropertyScopeGlobal,
                                                 mElement: kAudioObjectPropertyElementMain)
        
        var size = UInt32(0)
        
        try interface.propertyDataSize(of: AudioObjectID(kAudioObjectSystemObject),
                                       address: &address,
                                       outDataSize: &size)
        
        var data = Data(count: Int(size))
        
        try data.withUnsafeMutableBytes
        { bytes in
            
            let deviceID = bytes.bindMemory(to: AudioDeviceID.self).baseAddress!
            try interface.propertyData(of: AudioObjectID(kAudioObjectSystemObject),
                                       address: &address,
                                       dataSize: &size,
                                       outData: deviceID)
        }
        
        let numberOfDevices = Int(size) / MemoryLayout<AudioDeviceID>.size
        var deviceIDs = [AudioDeviceID](repeating: AudioDeviceID(0), count: numberOfDevices)
        
        deviceIDs.withUnsafeMutableBufferPointer
        { deviceIDBytes in
            
            _ = data.copyBytes(to: deviceIDBytes)
        }
        
        let knownIDs = Set(devices.map { $0.id })
        let existingIDs = Set(deviceIDs)
        
        let addedIDs = existingIDs.subtracting(knownIDs)
        let removedIDs = knownIDs.subtracting(existingIDs)
        
        devices.removeAll(where: { removedIDs.contains($0.id) })
        try devices.append(contentsOf: addedIDs.map { try Device(id: $0,
                                                                 interface: interface) })
    }
    
    //
    // MARK: - Aggregates
    //
    
    public func createAggregateDevice(title: String,
                                      subdeviceIdentifiers: [String]? = nil) throws -> Device
    {
        var deviceID: AudioObjectID = 0
        let identifiers = subdeviceIdentifiers ?? devices.filter { $0.isDefaultInput || $0.isDefaultOutput }.map { $0.identifier }

        let description = try interface.aggregateDeviceDescription(title: title,
                                                                   identifier: title.replacingOccurrences(of: " ", with: "-"),
                                                                   subdeviceIdentifiers: identifiers)
        
        try interface.createAggregateDevice(description: description as CFDictionary,
                                            outDeviceID: &deviceID)
        
        let aggregateDevice = try Device(id: deviceID, interface: interface)
        devices.append(aggregateDevice)
        
        return aggregateDevice
    }
    
    public func destroyAggregateDevice(_ device: DeviceProtocol) throws
    {
        try interface.destroyAggregateDevice(id: device.id)
    }
}
