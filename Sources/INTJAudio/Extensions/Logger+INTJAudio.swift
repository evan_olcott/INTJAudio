//
//  Logger+INTJAudio.swift
//  INTJAudio
//
//  Created by Evan Olcott on 10/24/22.
//

import Foundation
import OSLog

extension Logger
{
    private static let subsystem = "com.intj-software.intj-audio"

    public static let stream: Logger =
    {
        if ProcessInfo.processInfo.environment["LOG_AUDIO_STREAM"] != nil
        {
            return Logger(subsystem: subsystem, category: "Stream")
        }
        else
        {
            return Logger(.disabled)
        }
    }()

    public static let hardware = Logger(subsystem: subsystem, category: "Hardware")
    public static let lifecycle = Logger(subsystem: subsystem, category: "Lifecycle")
}
