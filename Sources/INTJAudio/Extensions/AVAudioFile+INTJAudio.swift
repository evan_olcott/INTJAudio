//
//  AVAudioFile+INTJAudio.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/2/22.
//

import AVFoundation
import OSLog

extension AVAudioFormat
{
    convenience init(standardFormatWithChannelCount count: ChannelCount)
    {
        self.init(standardFormatWithSampleRate: Hertz.samplingRate.rawValue,
                  channels: AVAudioChannelCount(count.rawValue))!
    }
}

public extension AVAudioFile
{
    enum Error: Swift.Error
    {
        case bufferCreateFailed
        case floatDataNotAvailable
    }
    
    var frameCount: SampleCount
    {
        SampleCount(length)
    }
    
    var position: SamplePosition
    {
        get { SamplePosition(framePosition) }
        set { framePosition = AVAudioFramePosition(newValue.rawValue) }
    }
    
    var channelCount: ChannelCount
    {
        ChannelCount(fileFormat.channelCount)
    }

    @discardableResult
    func readFrames(into buffers: [AudioBuffer],
                    at position: SamplePosition = .zero,
                    count: SampleCount? = nil) throws -> SampleCount
    {
        Logger.stream.debug("AVAudioFile.readFrames(into:at:count:)")
        
        buffers.forEach { $0.clear() }

        let framesToRead = count ?? position.distance(to: buffers.frameEnd)
        
        guard
            let avBuffer = AVAudioPCMBuffer(pcmFormat: .init(standardFormatWithChannelCount: channelCount),
                                            frameCapacity: AVAudioFrameCount(framesToRead))
        else { throw Error.bufferCreateFailed }
        avBuffer.frameLength = avBuffer.frameCapacity
        
        try read(into: avBuffer)
        let framesRead = SampleCount(avBuffer.frameLength)
        
        guard let sourceChannels = avBuffer.floatChannelData else { throw Error.floatDataNotAvailable }
        
        channelCount.forEach
        { index in
            
            let source = sourceChannels.advanced(by: Int(index)).pointee
            let sourceBuffer = UnsafeBufferPointer<Float>(start: source, count: Int(framesRead))
            
            buffers[index].copy(from: sourceBuffer,
                                range: SampleRange(count: framesRead),
                                to: position)
        }
        
        return framesRead
    }
    
    func writeFrames(from buffer: [AudioBuffer]) throws
    {
        Logger.stream.debug("AVAudioFile.writeFrames(from:)")

        guard
            let avBuffer = AVAudioPCMBuffer(pcmFormat: .init(standardFormatWithChannelCount: channelCount),
                                            frameCapacity: AVAudioFrameCount(buffer.frameCount)),
            let destinationChannels = avBuffer.floatChannelData
        else { throw Error.bufferCreateFailed }
        avBuffer.frameLength = avBuffer.frameCapacity
        
        channelCount.forEach
        { index in
            
            let destination = destinationChannels.advanced(by: Int(index)).pointee
            
            memcpy(destination,
                   buffer[index].pointer,
                   Int(buffer.frameCount) * MemoryLayout<Float>.size)
        }

        try write(from: avBuffer)
    }
}
