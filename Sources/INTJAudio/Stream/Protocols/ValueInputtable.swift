//
//  ValueInputtable.swift
//  INTJAudio
//
//  Created by Evan Olcott on 8/25/24.
//

import Foundation

public protocol ValueInputtable: StreamProcessor
{
    var valueInputBuses: [String: ValueInputBus] { get }
    
    func prepareForValueInput(state: StreamState)

    func setValue(_: Double?, from: ValueInputBus, state: StreamState)
}

extension ValueInputtable
{
    public func prepareForValueInput(state: StreamState) { }
    
    public func setValue(_: Double?, from: ValueInputBus, state: StreamState) { }
}
