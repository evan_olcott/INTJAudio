//
//  TimeRangeTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class TimeRangeTests: XCTestCase
{
    func testInit()
    {
        let range = TimeRange()
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 0)
    }
    
    func testInitRawValue()
    {
        let range = TimeRange(rawValue: 100 ... 200)
        XCTAssertEqual(range.rawValue.lowerBound, 100)
        XCTAssertEqual(range.rawValue.upperBound, 200)
    }
    
    func testInitEmpty()
    {
        let range = TimeRange.empty
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 0)
    }
    
    func testInitTimeLocationRange()
    {
        let range = TimeLocation(5) ... TimeLocation(7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitTimeLocation()
    {
        let range = ...TimeLocation(7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitTimeLength()
    {
        let range = ...TimeLength(7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }

    func testEncode() throws
    {
        let range = TimeRange()
        let data = try JSONEncoder().encode(range)
        let string = String(data: data, encoding: .utf8)
        XCTAssertEqual(string, "[0,0]")
    }
    
    func testDecode() throws
    {
        let string = "[100,200]"
        let data = string.data(using: .utf8)!
        let range = try JSONDecoder().decode(TimeRange.self, from: data)
        XCTAssertEqual(range.rawValue.lowerBound, 100)
        XCTAssertEqual(range.rawValue.upperBound, 200)
    }
    
    func testDecodeFailure()
    {
        let string = "[200]"
        let data = string.data(using: .utf8)!
        XCTAssertThrowsError(try JSONDecoder().decode(TimeRange.self, from: data))
    }
    
    func testInitWithSampleRange()
    {
        let range = TimeRange(range: SampleRange(rawValue: 500 ..< 700), sampleRate: Hertz(100))
        
        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitWithStartEnd()
    {
        let range = TimeRange(start: 5.0, end: 7.0)
        
        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitWithEnd()
    {
        let range = TimeRange(end: 7.0)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testWithStartLength()
    {
        let range = TimeRange(start: 5.0, length: 2.0)

        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testWithLength()
    {
        let range = TimeRange(length: 2.0)

        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 2)
    }
    
    func testStart()
    {
        let range = TimeRange(range: 500 ..< 700, sampleRate: Hertz(100))
        XCTAssertEqual(range.start, TimeLocation(5))
    }
    
    func testEnd()
    {
        let range = TimeRange(range: 500 ..< 700, sampleRate: Hertz(100))
        XCTAssertEqual(range.end, TimeLocation(7))
    }
    
    func testIsEmpty()
    {
        let range: TimeRange = 500.0 ... 500.0
        XCTAssertTrue(range.isEmpty)
    }

    func testLength()
    {
        let range = TimeRange(range: 500 ..< 700, sampleRate: Hertz(100))
        XCTAssertEqual(range.length, TimeLength(2))
    }

    func testMid()
    {
        let range = TimeRange(range: 500 ..< 700, sampleRate: Hertz(100))
        XCTAssertEqual(range.mid, TimeLocation(6))
    }
    
    func testContains()
    {
        let range = TimeRange(range: 500 ..< 700, sampleRate: Hertz(100))
        
        XCTAssertFalse(range.contains(TimeLocation(4.99)))
        XCTAssertTrue(range.contains(TimeLocation(5.00)))
        XCTAssertTrue(range.contains(TimeLocation(5.01)))

        XCTAssertTrue(range.contains(TimeLocation(6.99)))
        XCTAssertTrue(range.contains(TimeLocation(7.00)))
        XCTAssertFalse(range.contains(TimeLocation(7.01)))
    }
    
    func testNormalized()
    {
        let range = TimeRange(range: 500 ..< 700, sampleRate: Hertz(100))

        XCTAssertEqual(range.normal(for: TimeLocation(6.0)), 0.5)
        XCTAssertEqual(range.location(from: 0.5), 6.0)
        XCTAssertEqual(TimeRange().normal(for: 6.0), 0)
    }
    
    func testAdvancedBy()
    {
        XCTAssertEqual(TimeRange(start: 3.5, end: 7.5).advanced(by: TimeLength(2)).rawValue, 5.5 ... 9.5)
    }
    
    func testRecededBy()
    {
        XCTAssertEqual(TimeRange(start: 3.5, end: 7.5).receded(by: TimeLength(2)).rawValue, 1.5 ... 5.5)
    }
    
    func testExpand()
    {
        var range = TimeRange(start: 5.0, end: 9.0)
        
        let expanded = range.expanded(by: 2.0, anchor: 7.0)
        XCTAssertEqual(expanded, TimeRange(start: 3.0, end: 11.0))
        XCTAssertEqual(range, TimeRange(start: 5.0, end: 9.0))
        
        range.expand(by: 2.0, anchor: 7.0)
        XCTAssertEqual(range, TimeRange(start: 3.0, end: 11.0))
    }
    
    func testClamp()
    {
        let range1 = TimeRange(start: 5.0, end: 9.0)
        let range2 = TimeRange(start: 4.0, end: 8.0)
        
        let clamped = range1.clamped(to: range2)
        XCTAssertEqual(clamped, TimeRange(start: 5.0, end: 8.0))
    }

    func testEquality()
    {
        let range1 = TimeRange(start: 5.0, end: 7.0)
        let range2 = TimeRange(start: 5.01, end: 6.99)
        
        XCTAssertTrue(range1 == range1)
        XCTAssertFalse(range1 == range2)
    }
}
