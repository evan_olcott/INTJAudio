//
//  SamplePosition.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import CoreGraphics
import Foundation

public struct SamplePosition: RawRepresentable, Codable, Hashable, Sendable
{
    public var rawValue: Int
    
    public init(rawValue value: Int)
    {
        rawValue = value
    }
    
    public init(_ value: Int)
    {
        rawValue = value
    }
    
    public init(_ value: Int64)
    {
        rawValue = Int(value)
    }
    
    public init(_ value: Double)
    {
        rawValue = Int(value)
    }
    
    public init(_ value: CGFloat)
    {
        rawValue = Int(value)
    }

    public init(location: TimeLocation, sampleRate: Hertz = .samplingRate)
    {
        rawValue = Int((location.rawValue * sampleRate.rawValue).rounded())
    }
    
    public static var zero: SamplePosition { 0 }
}

public extension SamplePosition
{
    mutating func advance(by count: SampleCount)
    {
        rawValue += count.rawValue
    }
    
    func advanced(by count: SampleCount) -> SamplePosition
    {
        var new = self
        new.advance(by: count)
        return new
    }
    
    mutating func recede(by count: SampleCount)
    {
        rawValue -= count.rawValue
    }
    
    func receded(by count: SampleCount) -> SamplePosition
    {
        var new = self
        new.recede(by: count)
        return new
    }

    func distance(to other: SamplePosition) -> SampleCount
    {
        SampleCount(other.rawValue - rawValue)
    }

    func absoluteDistance(to other: SamplePosition) -> SampleCount
    {
        SampleCount(abs(other.rawValue - rawValue))
    }

    mutating func quantize(to count: SampleCount)
    {
        rawValue = (rawValue / count.rawValue) * count.rawValue
    }
    
    func quantized(to count: SampleCount) -> SamplePosition
    {
        var new = self
        new.quantize(to: count)
        return new
    }
    
    mutating func clamp(to range: SampleRange)
    {
        rawValue = min(max(rawValue, range.rawValue.lowerBound), range.rawValue.upperBound)
    }
    
    func clamped(to range: SampleRange) -> SamplePosition
    {
        var new = self
        new.clamp(to: range)
        return new
    }

    /// The position of the receiver if the given position is considered the origin
    ///
    /// - Parameter position: The "origin" position
    /// - Returns: The relative position of the receiver
    
    func relative(to position: SamplePosition) -> SamplePosition
    {
        SamplePosition(rawValue - position.rawValue)
    }
    
    mutating func convert(fromBase: SamplePosition, toBase: SamplePosition)
    {
        rawValue -= fromBase.rawValue
        rawValue += toBase.rawValue
    }
    
    /// The position of the receiver converted from one origin to another
    ///
    /// - Parameters:
    ///   - fromBase: The origin of the receiver's current context
    ///   - toBase: The origin of the destination context
    /// - Returns: The position in the context of the destination context
    
    func converted(fromBase: SamplePosition, toBase: SamplePosition) -> SamplePosition
    {
        var new = self
        new.convert(fromBase: fromBase, toBase: toBase)
        return new
    }
    
    func normalizedRelative(to range: SampleRange) -> Double
    {
        Double(rawValue - range.rawValue.lowerBound) / Double(range.count)
    }
}

extension SamplePosition: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int

    public init(integerLiteral value: Int)
    {
        rawValue = value
    }
}

extension SamplePosition: Equatable
{
    public static func == (lhs: SamplePosition, rhs: SamplePosition) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension SamplePosition: Comparable
{
    public static func < (lhs: SamplePosition, rhs: SamplePosition) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

extension SamplePosition: LosslessStringConvertible
{
    public init?(_ string: String)
    {
        guard let intValue = Int(string) else { return nil }
        rawValue = intValue
    }
    
    public var description: String
    {
        return String(rawValue)
    }
}

public extension Int
{
    init(_ position: SamplePosition)
    {
        self.init(position.rawValue)
    }
}

public extension Float
{
    init(_ position: SamplePosition)
    {
        self.init(Float(position.rawValue))
    }
}

public extension Double
{
    init(_ position: SamplePosition)
    {
        self.init(Double(position.rawValue))
    }
}

public extension CGFloat
{
    init(_ position: SamplePosition)
    {
        self.init(CGFloat(position.rawValue))
    }
}

public extension Int64
{
    init(_ position: SamplePosition)
    {
        self.init(position.rawValue)
    }
}

public extension UInt64
{
    init(_ position: SamplePosition)
    {
        self.init(UInt64(position.rawValue))
    }
}

@available(macOS 13.0, *)
public extension Duration
{
    init(_ value: SamplePosition, sampleRate: Hertz = .samplingRate)
    {
        self = Duration(TimeLocation(position: value, sampleRate: sampleRate))
    }
}
