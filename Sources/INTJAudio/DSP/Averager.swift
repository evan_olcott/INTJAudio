//
//  Averager.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/27/24.
//

import Accelerate
import Foundation

public class Averager
{
    private let count: Int
    private let depth: Int

    private var buffers = [[Float]]()
    private var depthCursor: Int = 0
    private var output = [Float]()
    
    /// Create an object that takes the average of a collection of values
    ///
    /// When a new set of values is added, the oldest set of values is discarded
    ///
    /// - Parameters:
    ///   - count: The expected number of distinct `Float` values that will be continuously added via the `adding` functions..
    ///   - depth: The number of sets of values that will be used to find the average value. If the `depth` is `0`, the average will be taken over all of the previously added values.
    
    public init(count: Int, depth: Int)
    {
        self.count = count
        self.depth = depth

        for _ in 0 ..< max(depth, 1)
        {
            buffers.append([Float](repeating: 0, count: count))
        }
        
        output = [Float](repeating: 0, count: count)
    }
    
    /// Resets the averaging

    public func clear()
    {
        for i in 0 ..< max(depth, 1)
        {
            vDSP.clear(&buffers[i])
        }
        
        depthCursor = 0
    }
    
    /// Adds a single value and returns the new averaged value.
    ///
    /// Use this only if the `Averager` was initialized with a count of `1`.
    ///
    /// - Parameter input: The value to add

    public func adding(_ input: Float) -> Float
    {
        adding([input])[0]
    }
    
    /// Adds a collection of values and returns the new averaged values.
    ///
    /// - Parameter input: The values to add
    
    public func adding(_ input: [Float]) -> [Float]
    {
        guard input.count == count else { fatalError() }
        
        if depth == 0
        {
            vDSP.add(input, buffers[0], result: &buffers[0])
            depthCursor += 1
            
            vDSP.divide(buffers[0], Float(depthCursor), result: &output)
        }
        else
        {
            let index = depthCursor % buffers.count
            buffers[index] = input
            depthCursor += 1

            memset(&output, 0, MemoryLayout<Float>.size * output.count)

            let scalar = 1.0 / Float(min(depthCursor, buffers.count))
            
            for buffer in buffers
            {
                vDSP.add(multiplication: (buffer, scalar), output, result: &output)
            }
        }
        
        return output
    }
}
