//
//  ArrangementTests.swift
//
//
//  Created by Evan Olcott on 11/3/22.
//

@testable import INTJAudio
import XCTest

class ArrangementTests: XCTestCase
{
    var mockTracks = [MockTrack]()
    var arrangement: Arrangement?

    override func setUp() async throws
    {
        try await super.setUp()
        
        mockTracks = [ MockTrack(), MockTrack(), MockTrack() ]
        
        arrangement = Arrangement()
        arrangement?.tracks = try XCTUnwrap(mockTracks)
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        arrangement = nil
        mockTracks = []
    }
    
    func testPrepare() async throws
    {
        let arrangement = try XCTUnwrap(arrangement)
        let mockTracks = try XCTUnwrap(mockTracks)
        
        try arrangement.prepare(for: AudioDimensions(frameCount: 768, channelCount: 4))
        
        mockTracks.forEach
        {
            XCTAssertEqual($0.calls.count, 1)
            XCTAssertEqual($0.calls[0].function, "prepare(for:)")
            XCTAssertEqual($0.calls[0].arguments.count, 1)
            XCTAssertEqual(($0.calls[0].arguments["for"] as? AudioDimensions)?.frameCount, 768)
            XCTAssertEqual(($0.calls[0].arguments["for"] as? AudioDimensions)?.channelCount, 4)
        }
    }
    
    func testRender() async throws
    {
        let arrangement = try XCTUnwrap(arrangement)
        let mockTracks = try XCTUnwrap(mockTracks)
        
        arrangement.render(into: [], for: 0)
        arrangement.render(into: [], for: 768)
        arrangement.render(into: [], for: 1536)
        
        mockTracks.forEach
        {
            XCTAssertEqual($0.calls.count, 3)

            for i in 0 ... 2
            {
                XCTAssertEqual($0.calls[i].function, "render(into:for:)")
                XCTAssertEqual($0.calls[i].arguments.count, 2)
                
                let position = SamplePosition(i * 768)
                XCTAssertEqual($0.calls[i].arguments["for"] as? SamplePosition, position)
            }
        }
    }
}
