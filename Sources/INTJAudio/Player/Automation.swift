//
//  Automation.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

/// Describes a unique property to be automated

public struct AutomationKey: RawRepresentable, ExpressibleByStringLiteral, Sendable
{
    public let rawValue: String
    
    public init?(rawValue: String)
    {
        self.rawValue = rawValue
    }
    
    public init(stringLiteral: String)
    {
        rawValue = stringLiteral
    }
}

public protocol AutomationProtocol
{
    func value(at location: TimeLocation) -> Double?
    func values(for range: TimeRange) -> [(TimeLocation, Double)]
    func setValue(_ value: Double, at location: TimeLocation)
}

/// Given a set of values at specific points in time, this calculates the value at any given time.
///
/// When a value is requested that is not between two points in time, it uses the value of the nearest point.

final class Automation: AutomationProtocol
{
    private struct Point
    {
        let location: TimeLocation
        let value: Double
    }
    
    let key: AutomationKey
    private var points = [Point]()
    
    init(for key: AutomationKey)
    {
        self.key = key
    }
    
    /// Returns the value at a given time
    ///
    /// - Parameter location: The `TimeLocation` being requested
    /// - Returns: The value at the `location` parameter. Returns `nil` if the value cannot be determined.

    func value(at location: TimeLocation) -> Double?
    {
        guard points.isEmpty == false else { return nil }
        
        var previous = points.last { $0.location <= location }
        var next = points.first { $0.location > location }
        
        if let next, previous == nil { previous = Point(location: .distantPast, value: next.value) }
        if let previous, next == nil { next = Point(location: .distantFuture, value: previous.value) }
        
        guard let next, let previous else { return nil }
        
        let interval = TimeRange(start: previous.location, end: next.location)
        let normal = interval.normal(for: location)
                
        return previous.value + ((next.value - previous.value) * normal)
    }
    
    /// Returns the important values from a range of time relative to the requested range
    ///
    /// - Parameter range: The `TimeRange` being requested
    /// - Returns: An array of `TimeLocation`, `Double` tuples that identify the value at a time relative to the start of the requested range.
    
    func values(for range: TimeRange) -> [(TimeLocation, Double)]
    {
        guard points.isEmpty == false else { return [] }

        var values = [(TimeLocation, Double)]()
        
        values.append((.zero, value(at: range.start)!))
        values.append(contentsOf: points
            .filter { range.contains($0.location) }
            .map { ($0.location.converted(fromBase: range.start, toBase: .zero), $0.value) }
            .sorted { $0.0 < $1.0 })
        values.append((.zero.advanced(by: range.length), value(at: range.end)!))
        
        return values
    }
    
    /// Sets a value for a specific time
    ///
    /// - Parameters:
    ///   - value: The value represented at the given time
    ///   - location: The `TimeLocation` that representes this value
    ///
    /// - Note:
    ///  If an existing value point exists within 1ms of the given location, the existing value is replaced.
    
    func setValue(_ value: Double, at location: TimeLocation)
    {
        let sampleLength = TimeLength(count: 48)
        
        if let existingIndex = points.firstIndex(where: { location.absoluteDistance(to: $0.location) <= sampleLength })
        {
            let location = points[existingIndex].location
            points.remove(at: existingIndex)
            points.insert(Point(location: location, value: value), at: existingIndex)
        }
        else
        {
            points.append(Point(location: location, value: value))
            points.sort { $0.location < $1.location }
        }
    }
}

extension AutomationProtocol
{
    func value(at position: SamplePosition) -> Double?
    {
        value(at: TimeLocation(position: position))
    }
    
    func values(for range: SampleRange) -> [(SamplePosition, Double)]
    {
        values(for: TimeRange(range: range)).map { (SamplePosition(location: $0.0), $0.1) }
    }
    
    func setValue(_ value: Double, at position: SamplePosition)
    {
        setValue(value, at: TimeLocation(position: position))
    }
}
