//
//  StreamState.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/27/24.
//

import Foundation

public struct StreamState
{
    public enum Method
    {
        case pull
        case push
    }
    
    public var frame: SamplePosition

    public var method: Method
    public var isRealtime: Bool
    
    public init(frame: SamplePosition,
                method: Method,
                isRealtime: Bool)
    {
        self.frame = frame
        self.method = method
        self.isRealtime = isRealtime
    }
}
