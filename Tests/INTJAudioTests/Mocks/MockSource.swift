//
//  MockSource.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

import Foundation
import INTJAudio

final class MockSource: MockObject, SourceProtocol
{
    var end: SamplePosition = .zero
    
    func prepare(for dimensions: AudioDimensions)
    {
        addCall(to: #function, with: [ "for": dimensions ])
    }
    
    func read(into output: [AudioBuffer], from range: SampleRange, to cursor: SamplePosition)
    {
        addCall(to: #function, with: [ "into": output, "from": range, "to": cursor ])
    }
}
