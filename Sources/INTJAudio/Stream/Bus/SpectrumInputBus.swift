//
//  SpectrumInputBus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/31/24.
//

import Foundation

public final class SpectrumInputBus: Bus, Equatable
{
    public let identifier: String
    public var connectedBus: Bus?
    public weak var processor: StreamProcessor?
    public var taps = [Bus]()
    
    public init(for processor: SpectrumInputtable, identifier: String)
    {
        self.processor = processor
        self.identifier = identifier
    }
    
    public static func == (lhs: SpectrumInputBus, rhs: SpectrumInputBus) -> Bool
    {
        lhs.identifier == rhs.identifier
    }

    public func handleSpectrumInput(_ buffer: [ComplexBuffer], state: StreamState)
    {
        if let taps = taps as? [SpectrumInputBus]
        {
            taps.forEach { $0.handleSpectrumInput(buffer, state: state) }
        }
        
        if let processor = processor as? SpectrumInputtable
        {
            processor.handleSpectrumInput(buffer, from: self, state: state)
        }
    }
    
    public func renderSpectrum(into buffer: [ComplexBuffer], state: StreamState) -> Bool
    {
        var didCalculate = false
        
        if let outputBus = connectedBus as? SpectrumOutputBus
        {
            didCalculate = outputBus.renderSpectrum(into: buffer, state: state)
        }
        else
        {
            buffer.forEach { $0.clear() }
        }
        
        if didCalculate, let taps = taps as? [SpectrumInputBus]
        {
            taps.forEach { $0.handleSpectrumInput(buffer, state: state) }
        }
        
        return didCalculate
    }
}
