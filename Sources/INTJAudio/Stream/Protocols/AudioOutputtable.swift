//
//  AudioOutputtable.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import Foundation

public protocol AudioOutputtable: StreamProcessor
{
    var audioOutputBuses: [AudioOutputBus] { get }

    /// The `AudioOutputBus` that will render audio buffers directly to the `Device`.
    /// Return `nil` if the processor should not render buffers to the `Device`, but from other `AudioConnection`s.

    var audioOutputHeadBus: AudioOutputBus? { get }
    
    func prepareForAudioRendering(with: AudioDimensions, state: StreamState)

    func renderAudio(into: [AudioBuffer], for: AudioOutputBus, state: StreamState)
}

public extension AudioOutputtable
{
    var audioOutputHeadBus: AudioOutputBus? { nil }
    
    func prepareForAudioRendering(with: AudioDimensions, state: StreamState) { }
}
