//
//  FileHandle+INTJAudioTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class FileHandle_INTJAudioTests: XCTestCase
{
    var url: URL?
    
    override func setUp() async throws
    {
        try await super.setUp()

        let tempDirectory = FileManager.default.temporaryDirectory
        let filename = UUID().uuidString
        url = tempDirectory.appendingPathComponent(filename)

        let url = try XCTUnwrap(url)
        
        FileManager.default.createFile(atPath: url.path,
                                       contents: nil,
                                       attributes: nil)

        let writer = try FileHandle(forWritingTo: url)

        let n = (Double.pi * 2) * 1000 / 48000
        var Ø = 0.0
        var v: Float = 0

        (0 ..< 2048).forEach
        { _ in

            v = Float(0.707 * sin(Ø))
            writer.write(Data(bytes: &v, count: 4))

            Ø += n
            Ø.formTruncatingRemainder(dividingBy: .pi * 2)
        }

        writer.synchronizeFile()
    }

    override func tearDown() async throws
    {
        try await super.tearDown()

        try FileManager.default.removeItem(at: try XCTUnwrap(url))
    }

    func testFileHandleAccess() async throws
    {
        let url = try XCTUnwrap(url)

        let reader = try FileHandle(forReadingFrom: url)
        let audioBuffer = AudioBuffer(count: 1024)

        reader.position = 96
        reader.readFrames(into: audioBuffer)

        XCTAssertEqual(reader.sampleCount, 2048)
        XCTAssertEqual(reader.position, 1120)
        XCTAssertEqual(audioBuffer[0], 0, accuracy: 0.000001)
    }
}
