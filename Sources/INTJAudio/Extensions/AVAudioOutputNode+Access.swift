//
//  AVAudioOutputNode+Access.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/28/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import AVFoundation
import Foundation
import OSLog

extension AVAudioOutputNode
{
    var outputDeviceID: AudioObjectID
    {
        get
        {
            guard let audioUnit else { return 0 }

            var currentDeviceID: AudioObjectID = 0
            var currentDeviceIDSize = UInt32(MemoryLayout<UInt32>.size)
            
            AudioUnitGetProperty(audioUnit,
                                 kAudioOutputUnitProperty_CurrentDevice,
                                 kAudioUnitScope_Global,
                                 0,
                                 &currentDeviceID,
                                 &currentDeviceIDSize)
            
            return currentDeviceID
        }
        
        set
        {
            guard let audioUnit else { return }
            
            var newDeviceID = newValue
            
            let err = AudioUnitSetProperty(audioUnit,
                                           kAudioOutputUnitProperty_CurrentDevice,
                                           kAudioUnitScope_Global,
                                           0,
                                           &newDeviceID,
                                           UInt32(MemoryLayout<AudioDeviceID>.size))
            
            if err != noErr
            {
                Logger.hardware.error("set outputDeviceID failed: \(err)")
            }
        }
    }
    
    var sampleRate: Hertz
    {
        get
        {
            guard let audioUnit else { return 0 }

            var currentRate: Float64 = 0
            var currentRateSize = UInt32(MemoryLayout<Float64>.size)
            
            AudioUnitGetProperty(audioUnit,
                                 kAudioDevicePropertyNominalSampleRate,
                                 kAudioUnitScope_Global,
                                 0,
                                 &currentRate,
                                 &currentRateSize)
            
            return Hertz(currentRate)
        }
        
        set
        {
            guard let audioUnit else { return }
            
            var newRate = Float64(newValue)
            
            let err = AudioUnitSetProperty(audioUnit,
                                           kAudioDevicePropertyNominalSampleRate,
                                           kAudioUnitScope_Global,
                                           0,
                                           &newRate,
                                           UInt32(MemoryLayout<Float64>.size))
            
            if err != noErr
            {
                Logger.hardware.error("set sampleRate failed: \(err)")
            }
        }
    }
    
    var bufferFrameSize: SampleCount
    {
        get
        {
            guard let audioUnit else { return 0 }

            var currentSize: UInt32 = 0
            var currentSizeSize = UInt32(MemoryLayout<UInt32>.size)
            
            AudioUnitGetProperty(audioUnit,
                                 kAudioDevicePropertyBufferFrameSize,
                                 kAudioUnitScope_Global,
                                 0,
                                 &currentSize,
                                 &currentSizeSize)
            
            return SampleCount(currentSize)
        }
        
        set
        {
            guard let audioUnit else { return }
            
            var newSize = UInt32(newValue)
            
            let err = AudioUnitSetProperty(audioUnit,
                                           kAudioDevicePropertyBufferFrameSize,
                                           kAudioUnitScope_Global,
                                           0,
                                           &newSize,
                                           UInt32(MemoryLayout<UInt32>.size))
            
            if err != noErr
            {
                Logger.hardware.error("set bufferFrameSize failed: \(err)")
            }
        }
    }
}
