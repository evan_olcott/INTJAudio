//
//  MockObject.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 11/4/22.
//

import Foundation

enum TestError: Error, Equatable
{
    case simpleCondition
}

class MockObject
{
    var calls = [MockFunctionCall]()
    var didCallHandler: ((MockFunctionCall) -> Any?)?
    
    enum Error: LocalizedError
    {
        case indexOutOfRange
        
        var errorDescription: String?
        {
            "Call index out of range"
        }
    }
    
    @discardableResult
    func addCall(to function: String, with arguments: [String: Any?] = [:]) -> Any?
    {
        let call = MockFunctionCall(function: function, arguments: arguments)
        calls.append(call)
        return didCallHandler?(call)
    }

    @discardableResult
    func addCallThrowing(to function: String, with arguments: [String: Any?] = [:]) throws -> Any?
    {
        let call = MockFunctionCall(function: function, arguments: arguments)
        calls.append(call)
        let response = didCallHandler?(call)
        if let error = response as? Error
        {
            throw error
        }
        else
        {
            return response
        }
    }
    
    func call(at index: Int) throws -> MockFunctionCall
    {
        guard index < calls.count else { throw Error.indexOutOfRange }
        return calls[index]
    }
}
