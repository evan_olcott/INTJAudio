//
//  SampleCountTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
@testable import INTJAudio
import XCTest

class SampleCountTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(SampleCount(rawValue: 4000).rawValue, 4000)
    }
    
    func testInitAsUInt32()
    {
        XCTAssertEqual(SampleCount(UInt32(4000)).rawValue, 4000)
    }
    
    func testInitAsInt()
    {
        XCTAssertEqual(SampleCount(Int(4000)).rawValue, 4000)
    }

    func testInitAsInt32()
    {
        XCTAssertEqual(SampleCount(Int32(4000)).rawValue, 4000)
    }
    
    func testInitAsInt64()
    {
        XCTAssertEqual(SampleCount(Int64(4000)).rawValue, 4000)
    }
    
    func testInitAsDouble()
    {
        XCTAssertEqual(SampleCount(4000.0).rawValue, 4000)
    }
    
    func testInitAsCGFloat()
    {
        XCTAssertEqual(SampleCount(CGFloat(4000)).rawValue, 4000)
    }
    
    func testInitWithTimeLength()
    {
        let count = SampleCount(length: 2.0, sampleRate: Hertz(100))
        XCTAssertEqual(count.rawValue, 200)
    }
    
    func testInitWithByteCountInt()
    {
        XCTAssertEqual(SampleCount(byteCount: 16).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: 17).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: 18).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: 19).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: 20).rawValue, 5)
    }
    
    func testInitWithByteCountInt64()
    {
        XCTAssertEqual(SampleCount(byteCount: Int64(16)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: Int64(17)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: Int64(18)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: Int64(19)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: Int64(20)).rawValue, 5)
    }

    func testInitWithByteCountUInt32()
    {
        XCTAssertEqual(SampleCount(byteCount: UInt32(16)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt32(17)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt32(18)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt32(19)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt32(20)).rawValue, 5)
    }

    func testInitWithByteCountUInt64()
    {
        XCTAssertEqual(SampleCount(byteCount: UInt64(16)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt64(17)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt64(18)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt64(19)).rawValue, 4)
        XCTAssertEqual(SampleCount(byteCount: UInt64(20)).rawValue, 5)
    }
    
    func testConstants()
    {
        XCTAssertEqual(SampleCount.realtime.rawValue, 128)
        XCTAssertEqual(SampleCount.offline.rawValue, 512)
        XCTAssertEqual(SampleCount.encode.rawValue, 960)
        
        #if os(macOS)
        XCTAssertEqual(SampleCount.playback.rawValue, 512)
        #elseif targetEnvironment(simulator)
        XCTAssertEqual(SampleCount.playback.rawValue, 512)
        #elseif os(iOS)
        XCTAssertEqual(SampleCount.playback.rawValue, 128)
        #endif
    }
    
    func testsByteCount()
    {
        XCTAssertEqual(SampleCount(32).byteCount, 128)
    }
    
    func testHalf()
    {
        XCTAssertEqual(SampleCount(32).half.rawValue, 16)
    }
    
    func testIncreasedBy()
    {
        XCTAssertEqual(SampleCount(32).increased(by: 12).rawValue, 44)
    }
    
    func testDecreasedBy()
    {
        XCTAssertEqual(SampleCount(32).decreased(by: 12).rawValue, 20)
    }
    
    func testMultipliedBy()
    {
        XCTAssertEqual(SampleCount(32).multiplied(by: 3).rawValue, 96)
    }
    
    func testDividedBy()
    {
        XCTAssertEqual(SampleCount(32).divided(by: 4).rawValue, 8)
    }
    
    func testEquality()
    {
        let length1 = SampleCount(2)
        let length2 = SampleCount(5)
        
        XCTAssertTrue(length1 == length1)
        XCTAssertFalse(length1 == length2)
    }
    
    func testComparable()
    {
        let length1 = SampleCount(2)
        let length2 = SampleCount(5)
        
        XCTAssertTrue(length1 < length2)
        XCTAssertFalse(length1 > length2)
        XCTAssertTrue(length2 > length1)
    }

    func testCasting()
    {
        let count = SampleCount(25)
        
        XCTAssertEqual(Int(count), 25)
        XCTAssertEqual(Int32(count), 25)
        XCTAssertEqual(Int64(count), 25)
        XCTAssertEqual(UInt32(count), 25)
        XCTAssertEqual(Float(count), Float(25))
        XCTAssertEqual(Double(count), 25.0)
        XCTAssertEqual(CGFloat(count), 25.0)
        XCTAssertEqual(vDSP_Length(count), 25)
    }
    
    func testStringValue()
    {
        XCTAssertNil(SampleCount("ABC"))
        XCTAssertEqual(SampleCount("130")?.rawValue, 130)
        XCTAssertEqual(String(SampleCount(400)), "400")
    }
}
