//
//  ChannelMapTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/28/22.
//

@testable import INTJAudio
import XCTest

class ChannelMapTests: XCTestCase
{
    func testToMono()
    {
        var map = ChannelMap(forInChannelCount: 1, outChannelCount: 1)
        XCTAssertEqual(map?.pairs.count, 1)

        map = ChannelMap(forInChannelCount: 2, outChannelCount: 1)
        XCTAssertEqual(map?.pairs.count, 2)

        map = ChannelMap(forInChannelCount: 3, outChannelCount: 1)
        XCTAssertNil(map)

        map = ChannelMap(forInChannelCount: 4, outChannelCount: 1)
        XCTAssertNil(map)

        map = ChannelMap(forInChannelCount: 32, outChannelCount: 1)
        XCTAssertNil(map)
    }
    
    func testToStereo()
    {
        var map = ChannelMap(forInChannelCount: 1, outChannelCount: 2)
        XCTAssertEqual(map?.pairs.count, 2)

        map = ChannelMap(forInChannelCount: 2, outChannelCount: 2)
        XCTAssertEqual(map?.pairs.count, 2)

        map = ChannelMap(forInChannelCount: 3, outChannelCount: 2)
        XCTAssertNil(map)

        map = ChannelMap(forInChannelCount: 4, outChannelCount: 2)
        XCTAssertNil(map)

        map = ChannelMap(forInChannelCount: 32, outChannelCount: 2)
        XCTAssertNil(map)
    }
    
    func testForEach()
    {
        let map = ChannelMap(forInChannelCount: 1, outChannelCount: 2)
        
        var flag = false
        map?.forEach
        {
            if flag
            {
                XCTAssertEqual($0, 0)
                XCTAssertEqual($1, 1)
            }
            else
            {
                XCTAssertEqual($0, 0)
                XCTAssertEqual($1, 0)
                flag = true
            }
        }
    }
}
