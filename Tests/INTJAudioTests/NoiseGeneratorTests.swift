//
//  NoiseGeneratorTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 1/9/25.
//

import Accelerate
import Foundation
@testable import INTJAudio
import Testing

struct NoiseGeneratorTests {

    @Test func measure_arc4random() async throws
    {
        let buffer = AudioBuffer(count: 1024)
        
        let start = Date.timeIntervalSinceReferenceDate
        
        for _ in 0 ..< 1000
        {
            for i in 0 ..< buffer.end
            {
                buffer[i] = Float(arc4random_uniform(UInt32.max)) / Float(UInt32.max) * 2.0 - 1.0
            }
        }
        
        let elapsed = Date.timeIntervalSinceReferenceDate - start
        
        print(elapsed, "ms")
    }

    @Test func measure_BNNS() async throws
    {
        let buffer = AudioBuffer(count: 1024)
        
        let generator = BNNSCreateRandomGenerator(BNNSRandomGeneratorMethodAES_CTR, nil)

        var descriptor = BNNSNDArrayDescriptor(flags: BNNSNDArrayFlags(0),
                                               layout: BNNSDataLayoutVector,
                                               size: (Int(buffer.sampleCount), 0, 0, 0, 0, 0, 0, 0),
                                               stride: (0, 0, 0, 0, 0, 0, 0, 0),
                                               data: buffer.pointer,
                                               data_type: BNNSDataType.float,
                                               table_data: nil,
                                               table_data_type: BNNSDataType.float,
                                               data_scale: 1, data_bias: 0)

        let start = Date.timeIntervalSinceReferenceDate
        for _ in 0 ..< 1000
        {
            BNNSRandomFillUniformFloat(generator, &descriptor, -1, 1)
        }
        let elapsed = Date.timeIntervalSinceReferenceDate - start

        print(elapsed, "ms")
    }
}
