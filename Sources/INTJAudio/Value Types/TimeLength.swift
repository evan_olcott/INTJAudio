//
//  TimeLength.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation

public struct TimeLength: RawRepresentable, Codable, Hashable, Sendable
{
    public var rawValue: TimeInterval
    
    public init(rawValue: TimeInterval)
    {
        self.rawValue = rawValue
    }
    
    public init(_ value: Int)
    {
        rawValue = TimeInterval(value)
    }
    
    public init(_ value: Double)
    {
        rawValue = value
    }
    
    public init(count: SampleCount, sampleRate: Hertz = .samplingRate)
    {
        rawValue = Double(count) / sampleRate.rawValue
    }
    
    public var seconds: Double
    {
        return rawValue
    }
    
    public var milliseconds: Double
    {
        return rawValue * 1000
    }
    
    public static var zero: TimeLength { 0.0 }
}

extension TimeLength: Equatable
{
    public static func == (lhs: TimeLength, rhs: TimeLength) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension TimeLength: Comparable
{
    public static func < (lhs: TimeLength, rhs: TimeLength) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

public extension TimeLength
{
    mutating func increase(by other: TimeLength)
    {
        rawValue += other.rawValue
    }
    
    func increased(by other: TimeLength) -> TimeLength
    {
        var new = self
        new.increase(by: other)
        return new
    }
    
    mutating func decrease(by other: TimeLength)
    {
        rawValue -= other.rawValue
    }
    
    func decreased(by other: TimeLength) -> TimeLength
    {
        var new = self
        new.decrease(by: other)
        return new
    }
    
    mutating func divide(by divisor: Double)
    {
        rawValue /= divisor
    }
    
    func divided(by divisor: Double) -> TimeLength
    {
        var new = self
        new.divide(by: divisor)
        return new
    }
    
    mutating func scale(by scale: Double)
    {
        rawValue *= scale
    }
    
    func scaled(by scale: Double) -> TimeLength
    {
        var new = self
        new.scale(by: scale)
        return new
    }
}

extension TimeLength: ExpressibleByFloatLiteral
{
    public typealias FloatLiteralType = TimeInterval

    public init(floatLiteral value: TimeInterval)
    {
        rawValue = value
    }
}

public extension Int
{
    init(_ value: TimeLength)
    {
        self.init(Int(value.rawValue))
    }
}

public extension Float
{
    init(_ value: TimeLength)
    {
        self.init(Float(value.rawValue))
    }
}

public extension TimeInterval
{
    init(_ value: TimeLength)
    {
        self.init(value.rawValue)
    }
}

public extension DateComponentsFormatter
{
    func string(from location: TimeLength) -> String?
    {
        string(from: TimeInterval(location))
    }
}

@available(macOS 13.0, *)
public extension Duration
{
    init(_ value: TimeLength)
    {
        self = Duration.seconds(value.rawValue)
    }
}
