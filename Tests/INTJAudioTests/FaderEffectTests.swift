//
//  FaderEffectTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/4/22.
//

@testable import INTJAudio
import XCTest

class FaderEffectTests: XCTestCase
{
    var fader: FaderEffect?
    var buffer: AudioBuffer?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        buffer = AudioBuffer(count: 2048)
        buffer?.rawValue.initialize(repeating: 0.5)
                
        fader = FaderEffect()
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        fader = nil
    }
    
    func testSetVolume() async throws
    {
        let mockAutomation = MockAutomation()
        let fader = try XCTUnwrap(fader)
        
        fader.scalarAutomation = mockAutomation
        fader.setVolume(-6.0, at: 2.5)
        
        XCTAssertEqual(mockAutomation.calls[0].function, "setValue(_:at:)")
        XCTAssertEqual(try XCTUnwrap(mockAutomation.calls[0].arguments["value"] as? Double), 0.5, accuracy: 0.01)
        XCTAssertEqual(mockAutomation.calls[0].arguments["at"] as? TimeLocation, 2.5)
    }
    
    func testAddFadeIn() async throws
    {
        let mockAutomation = MockAutomation()
        let fader = try XCTUnwrap(fader)
        
        fader.scalarAutomation = mockAutomation
        fader.addFadeIn(with: 1.0 ... 3.0)
        
        XCTAssertEqual(mockAutomation.calls[0].function, "setValue(_:at:)")
        XCTAssertEqual(mockAutomation.calls[0].arguments["value"] as? Double, 0.0)
        XCTAssertEqual(mockAutomation.calls[0].arguments["at"] as? TimeLocation, 1.0)
        
        XCTAssertEqual(mockAutomation.calls[1].function, "setValue(_:at:)")
        XCTAssertEqual(mockAutomation.calls[1].arguments["value"] as? Double, 1.0)
        XCTAssertEqual(mockAutomation.calls[1].arguments["at"] as? TimeLocation, 3.0)
    }
    
    func testAddFadeOut() async throws
    {
        let mockAutomation = MockAutomation()
        let fader = try XCTUnwrap(fader)
        
        fader.scalarAutomation = mockAutomation
        fader.addFadeOut(with: 1.0 ... 3.0)
        
        XCTAssertEqual(mockAutomation.calls[0].function, "setValue(_:at:)")
        XCTAssertEqual(mockAutomation.calls[0].arguments["value"] as? Double, 1.0)
        XCTAssertEqual(mockAutomation.calls[0].arguments["at"] as? TimeLocation, 1.0)
        
        XCTAssertEqual(mockAutomation.calls[1].function, "setValue(_:at:)")
        XCTAssertEqual(mockAutomation.calls[1].arguments["value"] as? Double, 0.0)
        XCTAssertEqual(mockAutomation.calls[1].arguments["at"] as? TimeLocation, 3.0)
    }
    
    func testNoFade() async throws
    {
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        fader.process([buffer], for: 0)
        
        XCTAssertEqual(buffer.minimum, 0.5)
        XCTAssertEqual(buffer.maximum, 0.5)
    }
    
    func testFadeBefore() async throws
    {
        let automation = Automation(for: "process")
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        automation.setValue(1, at: 512)
        automation.setValue(0, at: 1768)
        
        fader.scalarAutomation = automation
        fader.process([buffer], for: 2048)
        
        XCTAssertEqual(buffer.maximum, 0)
    }
    
    func testFadeCrossingStart() async throws
    {
        let automation = Automation(for: "process")
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        automation.setValue(1, at: 1024)
        automation.setValue(0, at: 3072)
        
        fader.scalarAutomation = automation
        fader.process([buffer], for: 2048)
        
        XCTAssertEqual(buffer[0], 0.25)
        XCTAssertGreaterThan(buffer[1023], 0)
        XCTAssertEqual(buffer[1024], 0)
        XCTAssertEqual(buffer[2047], 0)
    }
    
    func testFadeWithin() async throws
    {
        let automation = Automation(for: "process")
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        automation.setValue(1, at: 2560)
        automation.setValue(0, at: 3584)
        
        fader.scalarAutomation = automation
        fader.process([buffer], for: 2048)
        
        XCTAssertEqual(buffer[0], 0.5)
        XCTAssertEqual(buffer[512], 0.5)
        XCTAssertLessThan(buffer[513], 0.5)
        XCTAssertEqual(buffer[1024], 0.25)
        XCTAssertGreaterThan(buffer[1535], 0)
        XCTAssertEqual(buffer[1536], 0)
        XCTAssertEqual(buffer[2047], 0)
    }
    
    func testFadeCrossingEnd() async throws
    {
        let automation = Automation(for: "process")
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        automation.setValue(1, at: 3072)
        automation.setValue(0, at: 5120)
        
        fader.scalarAutomation = automation
        fader.process([buffer], for: 2048)
        
        XCTAssertEqual(buffer[0], 0.5)
        XCTAssertEqual(buffer[1024], 0.5)
        XCTAssertLessThan(buffer[1025], 0.5)
        XCTAssertEqual(buffer[2047], 0.25, accuracy: 0.001)
    }
    
    func testFadeAfter() async throws
    {
        let automation = Automation(for: "process")
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        automation.setValue(1, at: 8192)
        automation.setValue(0, at: 10864)
        
        fader.scalarAutomation = automation
        fader.process([buffer], for: 2048)
        
        XCTAssertEqual(buffer.maximum, 0.5)
        XCTAssertEqual(buffer.minimum, 0.5)
    }

    func testFadeEncompassing() async throws
    {
        let automation = Automation(for: "process")
        let fader = try XCTUnwrap(fader)
        let buffer = try XCTUnwrap(buffer)
        
        automation.setValue(1, at: 0)
        automation.setValue(0, at: 6144)
        
        fader.scalarAutomation = automation
        fader.process([buffer], for: 2048)
        
        XCTAssertEqual(buffer[0], 0.3333, accuracy: 0.0001)
        XCTAssertEqual(buffer[1024], 0.25, accuracy: 0.0001)
        XCTAssertEqual(buffer[2047], 0.1667, accuracy: 0.0001)
    }
}
