//
//  AudioInputBus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import Foundation

public final class AudioInputBus: Bus, Equatable
{
    public let identifier: String
    public var connectedBus: Bus?
    public weak var processor: StreamProcessor?
    public var taps = [Bus]()
    
    public init(for processor: AudioInputtable, identifier: String)
    {
        self.processor = processor
        self.identifier = identifier
    }
    
    public static func == (lhs: AudioInputBus, rhs: AudioInputBus) -> Bool
    {
        lhs.identifier == rhs.identifier
    }

    public func handleAudioInput(_ buffer: [AudioBuffer], state: StreamState)
    {
        if let taps = taps as? [AudioInputBus]
        {
            taps.forEach { $0.handleAudioInput(buffer, state: state) }
        }
        
        if let processor = processor as? AudioInputtable
        {
            processor.handleAudioInput(buffer, from: self, state: state)
        }
    }
    
    public func renderAudio(into buffer: [AudioBuffer], state: StreamState)
    {
        if let outputBus = connectedBus as? AudioOutputBus
        {
            outputBus.renderAudio(into: buffer, state: state)
        }
        else
        {
            buffer.forEach { $0.clear() }
        }
        
        if let taps = taps as? [AudioInputBus]
        {
            taps.forEach { $0.handleAudioInput(buffer, state: state) }
        }
    }
}
