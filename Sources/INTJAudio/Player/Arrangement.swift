//
//  Arrangement.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

public protocol ArrangementProtocol
{
    /// The tracks that the arrangemnent contains

    var tracks: [TrackProtocol] { get set }
    
    /// The landmarks that the arrangement contains
    
    var landmarks: [Landmark] { get set }
        
    /// Prepares the resources necessary for playback
    ///
    /// - Parameters:
    ///   - dimensions: The expected number of frames and channels that will be requested to render.

    func prepare(for dimensions: AudioDimensions) throws
    
    /// Asks the receiver to render the audio contents into the provided buffer
    ///
    /// - Parameters:
    ///   - buffer: The buffer to render the audio contents into
    ///   - position: The start of the buffer represents the audio at this sample frame
    
    func render(into buffer: [AudioBuffer], for position: SamplePosition)
}

/// An object that combines and arranges audio from multiple source into a single object that you can play or process.

public final class Arrangement: ArrangementProtocol
{
    public var tracks = [TrackProtocol]()
    public var landmarks = [Landmark]()
    
    public init() { }
    
    public func prepare(for dimensions: AudioDimensions) throws
    {
        try tracks.forEach { try $0.prepare(for: dimensions) }
    }
    
    public func render(into buffer: [AudioBuffer], for position: SamplePosition)
    {
        tracks.forEach { $0.render(into: buffer, for: position) }
    }
}
