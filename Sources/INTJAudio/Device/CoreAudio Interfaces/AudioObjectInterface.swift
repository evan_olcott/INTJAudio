//
//  AudioObject.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/1/22.
//

import CoreAudio
import Foundation
import OSLog

public protocol AudioObjectInterfaceProtocol
{
    func hasProperty(with ID: AudioObjectID,
                     address: UnsafePointer<AudioObjectPropertyAddress>) -> Bool

    func propertyDataSize(of ID: AudioObjectID,
                          address: UnsafePointer<AudioObjectPropertyAddress>,
                          outDataSize: UnsafeMutablePointer<UInt32>) throws

    func propertyData(of ID: AudioObjectID,
                      address: UnsafePointer<AudioObjectPropertyAddress>,
                      dataSize: UnsafeMutablePointer<UInt32>,
                      outData: UnsafeMutableRawPointer) throws

    func setPropertyData(of ID: AudioObjectID,
                         address: UnsafePointer<AudioObjectPropertyAddress>,
                         dataSize: UInt32,
                         data: UnsafeRawPointer) throws

    func addPropertyListener(to ID: AudioObjectID,
                             address: UnsafePointer<AudioObjectPropertyAddress>,
                             handler: @escaping AudioObjectPropertyListenerBlock) throws

    func removePropertyListener(to ID: AudioObjectID,
                                address: UnsafePointer<AudioObjectPropertyAddress>,
                                handler: @escaping AudioObjectPropertyListenerBlock) throws

    func createAggregateDevice(description: CFDictionary,
                               outDeviceID: UnsafeMutablePointer<AudioObjectID>) throws

    func destroyAggregateDevice(id: AudioObjectID) throws

    func createIOProcID(id: AudioObjectID,
                        handler: @escaping AudioDeviceIOProc,
                        clientData: UnsafeMutableRawPointer?,
                        handlerID: UnsafeMutablePointer<AudioDeviceIOProcID?>) throws

    func destroyIOProcID(id: AudioObjectID,
                         handlerID: @escaping AudioDeviceIOProcID) throws

    func start(id: AudioObjectID,
               handlerID: AudioDeviceIOProcID?) throws

    func stop(id: AudioObjectID,
              handlerID: AudioDeviceIOProcID?) throws
    
    func reset() throws
}

//
// MARK: -
//

struct AudioObjectInterface: AudioObjectInterfaceProtocol
{
    enum Error: Swift.Error
    {
        case notRunning
        case unspecified
        case unknownProperty
        case unsupportedProperty
        case badPropertySize
        case illegalOperation
        case badObject
        case badDevice
        case badStream
        case unsupportedOperation
        case notReady
        case unsupportedFormat
        case badPermissions
        case cannotDoInPreview
        case unknown(OSStatus)
    }
    
    func error(for code: OSStatus) -> Error
    {
        switch code
        {
        case kAudioHardwareNotRunningError:
            return .notRunning
        case kAudioHardwareUnspecifiedError:
            return .unspecified
        case kAudioHardwareUnknownPropertyError:
            return .unknownProperty
        case kAudioHardwareBadPropertySizeError:
            return .badPropertySize
        case kAudioHardwareIllegalOperationError:
            return .illegalOperation
        case kAudioHardwareBadObjectError:
            return .badObject
        case kAudioHardwareBadDeviceError:
            return .badDevice
        case kAudioHardwareBadStreamError:
            return .badStream
        case kAudioHardwareUnsupportedOperationError:
            return .unsupportedOperation
        case kAudioDeviceUnsupportedFormatError:
            return .unsupportedFormat
        case kAudioDevicePermissionsError:
            return .badPermissions
        default:
            return .unknown(code)
        }
    }

    func hasProperty(with id: AudioObjectID,
                     address: UnsafePointer<AudioObjectPropertyAddress>) -> Bool
    {
        return AudioObjectHasProperty(id, address)
    }
    
    func propertyDataSize(of id: AudioObjectID,
                          address: UnsafePointer<AudioObjectPropertyAddress>,
                          outDataSize: UnsafeMutablePointer<UInt32>) throws
    {
        let err = AudioObjectGetPropertyDataSize(id, address, 0, nil, outDataSize)
        if err != noErr
        {
            Logger.hardware.info("AudioObjectGetPropertyDataSize failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func propertyData(of id: AudioObjectID,
                      address: UnsafePointer<AudioObjectPropertyAddress>,
                      dataSize: UnsafeMutablePointer<UInt32>,
                      outData: UnsafeMutableRawPointer) throws
    {
        let err = AudioObjectGetPropertyData(id, address, 0, nil, dataSize, outData)
        if err != noErr
        {
            Logger.hardware.info("AudioObjectGetPropertyData failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func setPropertyData(of id: AudioObjectID,
                         address: UnsafePointer<AudioObjectPropertyAddress>,
                         dataSize: UInt32,
                         data: UnsafeRawPointer) throws
    {
        let err = AudioObjectSetPropertyData(id, address, 0, nil, dataSize, data)
        if err != noErr
        {
            Logger.hardware.info("AudioObjectSetPropertyData failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func addPropertyListener(to id: AudioObjectID,
                             address: UnsafePointer<AudioObjectPropertyAddress>,
                             handler: @escaping AudioObjectPropertyListenerBlock) throws
    {
        let err = AudioObjectAddPropertyListenerBlock(id, address, .main, handler)
        if err != noErr
        {
            Logger.hardware.info("AudioObjectAddPropertyListenerBlock failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func removePropertyListener(to id: AudioObjectID,
                                address: UnsafePointer<AudioObjectPropertyAddress>,
                                handler: @escaping AudioObjectPropertyListenerBlock) throws
    {
        let err = AudioObjectRemovePropertyListenerBlock(id, address, .main, handler)
        if err != noErr, err != kAudioHardwareBadObjectError
        {
            Logger.hardware.info("AudioObjectRemovePropertyListenerBlock failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func createAggregateDevice(description: CFDictionary,
                               outDeviceID: UnsafeMutablePointer<AudioObjectID>) throws
    {
        let err = AudioHardwareCreateAggregateDevice(description, outDeviceID)
        if err != noErr || outDeviceID.pointee == 0
        {
            Logger.hardware.info("AudioHardwareCreateAggregateDevice failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func destroyAggregateDevice(id: AudioObjectID) throws
    {
        let err = AudioHardwareDestroyAggregateDevice(id)
        if err != noErr
        {
            Logger.hardware.info("AudioHardwareDestroyAggregateDevice failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func createIOProcID(id: AudioObjectID,
                        handler: @escaping AudioDeviceIOProc,
                        clientData: UnsafeMutableRawPointer?,
                        handlerID: UnsafeMutablePointer<AudioDeviceIOProcID?>) throws
    {
        let err = AudioDeviceCreateIOProcID(id, handler, clientData, handlerID)
        if err != noErr
        {
            Logger.hardware.info("AudioDeviceCreateIOProcID failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func destroyIOProcID(id: AudioObjectID,
                         handlerID: @escaping AudioDeviceIOProcID) throws
    {
        let err = AudioDeviceDestroyIOProcID(id, handlerID)
        if err != noErr, err != kAudioHardwareBadDeviceError
        {
            Logger.hardware.info("AudioDeviceDestroyIOProcID failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func start(id: AudioObjectID,
               handlerID: AudioDeviceIOProcID?) throws
    {
        let err = AudioDeviceStart(id, handlerID)
        if err != noErr
        {
            Logger.hardware.info("AudioDeviceStart failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }

    func stop(id: AudioObjectID,
              handlerID: AudioDeviceIOProcID?) throws
    {
        let err = AudioDeviceStop(id, handlerID)
        if err != noErr, err != kAudioHardwareBadDeviceError
        {
            Logger.hardware.info("AudioDeviceStop failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }
    
    func reset() throws
    {
        let err = AudioHardwareUnload()
        if err != noErr
        {
            Logger.hardware.info("AudioHardwareUnload failed: \(NSFileTypeForHFSTypeCode(OSType(err)))")
            throw error(for: err)
        }
    }
}
