//
//  SampleRange.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation

public struct SampleRange: RawRepresentable, Codable, Hashable, Sendable
{
    public var rawValue: Range<Int>
    
    public init(rawValue: Range<Int>)
    {
        self.rawValue = rawValue
    }
    
    public init()
    {
        rawValue = 0 ..< 0
    }
    
    public init(from decoder: Decoder) throws
    {
        var values = try decoder.unkeyedContainer()
        
        let lowerBound = try values.decode(Int.self)
        let upperBound = try values.decode(Int.self)
        
        rawValue = lowerBound ..< upperBound
    }
    
    public init(start: SamplePosition = .zero, end: SamplePosition)
    {
        rawValue = start.rawValue ..< end.rawValue
    }
    
    public init(start: SamplePosition = .zero, count: SampleCount)
    {
        rawValue = start.rawValue ..< start.rawValue + count.rawValue
    }
    
    public init(range: TimeRange, sampleRate: Hertz = .samplingRate)
    {
        rawValue = Int((range.start.rawValue * sampleRate.rawValue).rounded()) ..< Int((range.end.rawValue * sampleRate.rawValue).rounded())
    }
    
    public func encode(to encoder: Encoder) throws
    {
        var values = encoder.unkeyedContainer()
        
        try values.encode(rawValue.lowerBound)
        try values.encode(rawValue.upperBound)
    }
    
    public static var empty: SampleRange { SampleRange() }
}

public extension SampleRange
{
    var start: SamplePosition
    {
        SamplePosition(rawValue.lowerBound)
    }
    
    var end: SamplePosition
    {
        SamplePosition(rawValue.upperBound)
    }
    
    var isEmpty: Bool
    {
        rawValue.isEmpty
    }
    
    var count: SampleCount
    {
        SampleCount(rawValue.count)
    }
    
    var mid: SamplePosition
    {
        SamplePosition(rawValue.lowerBound + ((rawValue.upperBound - rawValue.lowerBound) / 2))
    }

    func contains(_ position: SamplePosition) -> Bool
    {
        rawValue.contains(position.rawValue)
    }
    
    mutating func advance(by count: SampleCount)
    {
        rawValue = rawValue.lowerBound + count.rawValue ..< rawValue.upperBound + count.rawValue
    }
    
    func advanced(by count: SampleCount) -> SampleRange
    {
        var new = self
        new.advance(by: count)
        return new
    }
    
    mutating func recede(by count: SampleCount)
    {
        rawValue = rawValue.lowerBound - count.rawValue ..< rawValue.upperBound - count.rawValue
    }
    
    func receded(by count: SampleCount) -> SampleRange
    {
        var new = self
        new.recede(by: count)
        return new
    }
    
    mutating func clamp(to limits: SampleRange)
    {
        rawValue = rawValue.clamped(to: limits.rawValue)
    }
    
    func clamped(to limits: SampleRange) -> SampleRange
    {
        var new = self
        new.clamp(to: limits)
        return new
    }
    
    func overlaps(_ other: SampleRange) -> Bool
    {
        rawValue.overlaps(other.rawValue)
    }
    
    func intersection(_ other: SampleRange) -> SampleRange?
    {
        guard rawValue.overlaps(other.rawValue) else { return nil }
        return Swift.max(start, other.start) ..< Swift.min(end, other.end)
    }
    
    func relative(to other: SamplePosition) -> SampleRange
    {
        start.relative(to: other) ..< end.relative(to: other)
    }
    
    func converted(fromBase: SamplePosition, toBase: SamplePosition) -> SampleRange
    {
        start.converted(fromBase: fromBase, toBase: toBase) ..< end.converted(fromBase: fromBase, toBase: toBase)
    }
}

public func ..< (lhs: SamplePosition, rhs: SamplePosition) -> SampleRange
{
    SampleRange(start: lhs, end: rhs)
}

public prefix func ..< (rhs: SamplePosition) -> SampleRange
{
    SampleRange(start: .zero, end: rhs)
}

public prefix func ..< (rhs: SampleCount) -> SampleRange
{
    SampleRange(start: .zero, count: rhs)
}

extension SampleRange: Equatable
{
    public static func == (lhs: SampleRange, rhs: SampleRange) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension SampleRange: Sequence
{
    public func makeIterator() -> Iterator
    {
        Iterator(range: rawValue)
    }
    
    public struct Iterator: IteratorProtocol
    {
        private var iterator: Range<Int>.Iterator
        
        init(range: Range<Int>)
        {
            iterator = range.makeIterator()
        }
        
        public mutating func next() -> SamplePosition?
        {
            guard let nextValue = iterator.next() else {
                return nil
            }
            
            return SamplePosition(nextValue)
        }
    }
}
