//
//  Bus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/31/24.
//

import Foundation

public protocol Bus: AnyObject
{
    var identifier: String { get }
    var processor: StreamProcessor? { get set }
    var connectedBus: Bus? { get set }
    var taps: [Bus] { get set }
}
