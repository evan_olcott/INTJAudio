//
//  TimeRange.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import AVFoundation
import Foundation

public struct TimeRange: Codable, Hashable, Sendable
{
    public var rawValue: ClosedRange<TimeInterval>
    
    public init(rawValue: ClosedRange<TimeInterval>)
    {
        self.rawValue = rawValue
    }
    
    public init()
    {
        rawValue = 0 ... 0
    }
    
    public init(from decoder: Decoder) throws
    {
        var values = try decoder.unkeyedContainer()
        
        let lowerBound = try values.decode(TimeInterval.self)
        let upperBound = try values.decode(TimeInterval.self)
        
        rawValue = lowerBound ... upperBound
    }
    
    public init(range: SampleRange, sampleRate: Hertz = .samplingRate)
    {
        let lower = Double(range.start) / sampleRate.rawValue
        let upper = Double(range.end) / sampleRate.rawValue
        
        rawValue = lower ... upper
    }
    
    public init(start: TimeLocation = .zero, end: TimeLocation)
    {
        rawValue = start.rawValue ... end.rawValue
    }
    
    public init(start: TimeLocation = .zero, length: TimeLength)
    {
        rawValue = start.rawValue ... start.rawValue + length.rawValue
    }
    
    public func encode(to encoder: Encoder) throws
    {
        var values = encoder.unkeyedContainer()
        
        try values.encode(rawValue.lowerBound)
        try values.encode(rawValue.upperBound)
    }
    
    public static var empty: TimeRange { TimeRange() }
}

public extension TimeRange
{
    var start: TimeLocation
    {
        TimeLocation(rawValue: rawValue.lowerBound)
    }

    var end: TimeLocation
    {
        TimeLocation(rawValue: rawValue.upperBound)
    }
    
    var isEmpty: Bool
    {
        rawValue.upperBound == rawValue.lowerBound
    }

    var length: TimeLength
    {
        TimeLength(rawValue: rawValue.upperBound - rawValue.lowerBound)
    }
    
    var mid: TimeLocation
    {
        TimeLocation(rawValue.lowerBound + ((rawValue.upperBound - rawValue.lowerBound) / 2))
    }
    
    func contains(_ location: TimeLocation) -> Bool
    {
        rawValue.contains(location.rawValue)
    }
    
    func normal(for location: TimeLocation) -> Double
    {
        guard rawValue.lowerBound < rawValue.upperBound else { return 0 }
        return (location.rawValue - rawValue.lowerBound) / (rawValue.upperBound - rawValue.lowerBound)
    }
    
    func location(from normal: Double) -> TimeLocation
    {
        TimeLocation(rawValue: rawValue.lowerBound + ((rawValue.upperBound - rawValue.lowerBound) * normal))
    }
}

public extension TimeRange
{
    mutating func advance(by amount: TimeLength)
    {
        rawValue = rawValue.lowerBound + amount.rawValue ... rawValue.upperBound + amount.rawValue
    }
    
    func advanced(by amount: TimeLength) -> TimeRange
    {
        var new = self
        new.advance(by: amount)
        return new
    }

    mutating func recede(by amount: TimeLength)
    {
        rawValue = rawValue.lowerBound - amount.rawValue ... rawValue.upperBound - amount.rawValue
    }
    
    func receded(by amount: TimeLength) -> TimeRange
    {
        var new = self
        new.recede(by: amount)
        return new
    }
    
    mutating func expand(by amount: Double, anchor: TimeLocation? = nil)
    {
        let anchor = (anchor ?? mid).rawValue
        let start = anchor - (anchor - rawValue.lowerBound) * amount
        let end = anchor + (rawValue.upperBound - anchor) * amount

        rawValue = start ... end
    }
    
    func expanded(by amount: Double, anchor: TimeLocation? = nil) -> TimeRange
    {
        var new = self
        new.expand(by: amount, anchor: anchor)
        return new
    }
    
    mutating func clamp(to other: TimeRange)
    {
        rawValue = rawValue.clamped(to: other.rawValue)
    }
    
    func clamped(to other: TimeRange) -> TimeRange
    {
        var new = self
        new.clamp(to: other)
        return new
    }
}

public func ... (lhs: TimeLocation, rhs: TimeLocation) -> TimeRange
{
    TimeRange(start: lhs, end: rhs)
}

public prefix func ... (rhs: TimeLocation) -> TimeRange
{
    TimeRange(start: .zero, end: rhs)
}

public prefix func ... (rhs: TimeLength) -> TimeRange
{
    TimeRange(start: .zero, length: rhs)
}

extension TimeRange: Equatable
{
    public static func == (lhs: TimeRange, rhs: TimeRange) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

public extension CMTimeRange
{
    init(_ range: TimeRange)
    {
        self.init(start: CMTime(range.start), end: CMTime(range.end))
    }
}
