//
//  FourierTransform.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/28/24.
//

import Accelerate
import Foundation

public class FourierTransform
{
    let resolution: Resolution
    let setup: vDSP.FFT<DSPSplitComplex>
    var complex: DSPSplitComplex
    
    public init(resolution: Resolution)
    {
        self.resolution = resolution

        setup = vDSP.FFT(log2n: vDSP_Length(resolution),
                         radix: .radix2,
                         ofType: DSPSplitComplex.self)!
        
        let count = Int(resolution.sampleCount.half)
        complex = DSPSplitComplex(realp: UnsafeMutablePointer<Float>.allocate(capacity: count),
                                  imagp: UnsafeMutablePointer<Float>.allocate(capacity: count))
    }

    deinit
    {
        complex.realp.deallocate()
        complex.imagp.deallocate()
    }

    public func forward(_ input: AudioBuffer, result: ComplexBuffer)
    {
        assert(input.sampleCount == resolution.sampleCount,
               "FourierTransform.forward: AudioBuffer has invalid size")
        assert(result.resolution == resolution,
               "FourierTransform.forward: ComplexBuffer has invalid size")

        let complexArray = Array(UnsafeBufferPointer(
                start: input.rawValue.baseAddress!.withMemoryRebound(to: DSPComplex.self, capacity: 1) { $0 },
                count: Int(resolution.sampleCount.half))
            )
        
        vDSP.convert(interleavedComplexVector: complexArray,
                     toSplitComplexVector: &complex)

        var splitComplex = DSPSplitComplex(result)
        setup.forward(input: complex, output: &splitComplex)
    }

    public func inverse(_ input: ComplexBuffer, result: AudioBuffer)
    {
        assert(input.resolution == resolution,
               "FourierTransform.inverse: ComplexBuffer has invalid size")
        assert(result.sampleCount == resolution.sampleCount,
               "FourierTransform.inverse: AudioBuffer has invalid size")

        let splitComplex = DSPSplitComplex(input)
        setup.inverse(input: splitComplex, output: &complex)
                
        let count = Int(resolution.sampleCount)
        var complexArray = Array(repeating: DSPComplex(), count: count)

        vDSP.convert(splitComplexVector: complex,
                     toInterleavedComplexVector: &complexArray)
        
        complexArray.withUnsafeBufferPointer { complexPtr in
            complexPtr.baseAddress!.withMemoryRebound(to: Sample.self, capacity: 1) { floatPtr in
                result.copy(from: UnsafeBufferPointer(start: floatPtr, count: count))
            }
        }
        
        result.scale(by: Scalar(1.0 / Float(count * 2)))
    }
}
