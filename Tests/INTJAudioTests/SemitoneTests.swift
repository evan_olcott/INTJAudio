//
//  SemitoneTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

@testable import INTJAudio
import XCTest

class SemitoneTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(Semitone(rawValue: 4.207).rawValue, 4.207)
    }
    
    func testInitAsDouble()
    {
        XCTAssertEqual(Semitone(4.207).rawValue, 4.207)
    }

    func testInitAsHertz()
    {
        XCTAssertEqual(Semitone(Hertz(880)).rawValue, 81)
    }

    func testQuantize()
    {
        XCTAssertEqual(Semitone(4.207).quantized().rawValue, 4)
        XCTAssertEqual(Semitone(4.907).quantized().rawValue, 5)
    }
    
    func testOffset()
    {
        XCTAssertEqual(Semitone(4.207).offset.rawValue, 21)
        XCTAssertEqual(Semitone(4.907).offset.rawValue, -9)
    }

    func testDistance()
    {
        XCTAssertEqual(Semitone(60.0).distance(to: 60.4).rawValue, 40)
        XCTAssertEqual(Semitone(60.4).distance(to: 60.0).rawValue, -40)
    }

    func testAdvance()
    {
        XCTAssertEqual(Semitone(60.0).advanced(by: 40).rawValue, 60.4)
        XCTAssertEqual(Semitone(60.4).advanced(by: -40).rawValue, 60.0)
    }

    func testEquality()
    {
        let count1 = Semitone(62.0)
        let count2 = Semitone(65.0)
        
        XCTAssertTrue(count1 == count1)
        XCTAssertFalse(count1 == count2)
    }
    
    func testComparable()
    {
        let count1 = Semitone(62.0)
        let count2 = Semitone(65.0)

        XCTAssertTrue(count1 < count2)
        XCTAssertFalse(count1 > count2)
        XCTAssertTrue(count2 > count1)
    }
}
