//
//  HertzTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 6/12/24.
//

@testable import INTJAudio
import Testing

struct HertzTests
{
    @Test func initialization() async throws
    {
        #expect(Hertz(rawValue: 22050).rawValue == 22050.0)
        #expect(Hertz(Double(22050)).rawValue == 22050.0)
        #expect(Hertz(Float(22050)).rawValue == 22050.0)
        #expect(Hertz(Semitone(60.0)).rawValue - 261.625 < 0.001)
        #expect(Hertz("44100")?.rawValue == 44100)
        #expect(Hertz.samplingRate.rawValue == 48000.0)
    }

    @Test func equatable() async throws
    {
        let length1 = Hertz(22050)
        let length2 = Hertz(44100)
        
        #expect(length1 == length1)
        #expect(length1 != length2)
    }
    
    @Test func comparable() async throws
    {
        let length1 = Hertz(22050)
        let length2 = Hertz(44100)

        #expect(length1 < length2)
        #expect(length2 > length1)
    }
    
    @Test func casting() async throws
    {
        #expect(Double(Hertz(22050)) == 22050)
        #expect(String(describingForTest: Hertz(22050)) == "22050.0")
    }
}
