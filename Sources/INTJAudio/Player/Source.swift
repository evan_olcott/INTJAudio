//
//  Source.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

// swiftlint:disable discouraged_optional_collection

public protocol SourceProtocol
{
    var end: SamplePosition { get }

    func prepare(for dimensions: AudioDimensions)
    func read(into output: [AudioBuffer], from range: SampleRange, to cursor: SamplePosition)
}

/// Gives read access to audio samples either in memory or in raw audio files

public final class Source: SourceProtocol
{
    private let handles: [FileHandle]?
    private let inMemory: [AudioBuffer]?
    private var map: ChannelMap?
    
    /// Create a `Source` referencing files at URLs
    ///
    /// - Parameter urls: The audio file URLs
    ///
    /// The format of the files must be in single-channel little-endian 32-bit floating point values at 48kHz
    
    public init(with urls: [URL]) throws
    {
        handles = try urls.map { try FileHandle(forReadingFrom: $0) }
        inMemory = nil
    }
    
    /// Create a `Source` referencing in-memory `AudioBuffer`s
    ///
    /// - Parameter channels: The `AudioBuffer`s containing the audio data
    
    public init(with channels: [AudioBuffer])
    {
        handles = nil
        self.inMemory = channels
    }
    
    deinit
    {
        handles?.forEach { try? $0.close() }
    }
    
    /// The number of frames
    
    public var frameCount: SampleCount
    {
        if let handles
        {
            return handles[0].sampleCount
        }
        else if let inMemory
        {
            return inMemory.frameCount
        }
        
        return .zero
    }
    
    /// The position beyond the last frame
    
    public var end: SamplePosition
    {
        .zero.advanced(by: frameCount)
    }
    
    /// Prepares the resources necessary for playback
    ///
    /// - Parameters:
    ///   - dimensions: The number of frames and channels that will be requested to render.

    public func prepare(for dimensions: AudioDimensions)
    {
        if map != nil { return }
        
        let channelCount = ChannelCount(handles?.count ?? inMemory?.count ?? 0)
        
        map = ChannelMap(forInChannelCount: channelCount,
                         outChannelCount: dimensions.channelCount)
    }
    
    /// Asks the receiver to copy the audio contents into the provided buffers
    ///
    /// - Parameters:
    ///   - buffer: The buffer to render the audio contents into
    ///   - from: The range of the audio samples to copy from the `Source`
    ///   - to: The position in the `buffer` to place the first sample

    public func read(into output: [AudioBuffer], from range: SampleRange, to cursor: SamplePosition)
    {
        guard let map else { return }
        
        map.forEach
        {
            if let handles
            {
                output[$1].read(from: handles[Int($0)],
                                range: range,
                                to: cursor)
            }
            else if let inMemory
            {
                output[$1].copy(from: inMemory[$0],
                                range: range,
                                to: cursor)
            }
        }
    }
}
