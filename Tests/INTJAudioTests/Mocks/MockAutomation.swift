//
//  MockAutomation.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 11/4/22.
//

import Foundation
import INTJAudio

final class MockAutomation: MockObject, AutomationProtocol
{
    func value(at location: TimeLocation) -> Double?
    {
        addCall(to: #function, with: [ "at": location ]) as? Double
    }

    func values(for range: TimeRange) -> [(TimeLocation, Double)]
    {
        let result = addCall(to: #function, with: [ "for": range ])
        return result as? [(TimeLocation, Double)] ?? []
    }

    func setValue(_ value: Double, at location: TimeLocation)
    {
        addCall(to: #function, with: [ "value": value, "at": location ])
    }
}
