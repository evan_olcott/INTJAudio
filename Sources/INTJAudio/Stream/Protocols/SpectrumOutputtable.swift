//
//  SpectrumOutputtable.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/31/24.
//

import Foundation

public protocol SpectrumOutputtable: StreamProcessor
{
    var spectrumOutputBuses: [SpectrumOutputBus] { get }

    func prepareForSpectrumRendering(with: SpectrumDimensions, state: StreamState)

    func renderSpectrum(into: [ComplexBuffer], for: SpectrumOutputBus, state: StreamState) -> Bool
}
