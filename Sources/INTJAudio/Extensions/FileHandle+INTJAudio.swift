//
//  FileHandle+INTJAudio.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation
import OSLog

public extension FileHandle
{
    var sampleCount: SampleCount
    {
        let offset = offsetInFile
        let count = SampleCount(byteCount: seekToEndOfFile())
        seek(toFileOffset: offset)

        return count
    }
    
    var position: SamplePosition
    {
        get
        {
            let offset = SampleCount(byteCount: offsetInFile)
            return SamplePosition.zero.advanced(by: offset)
        }
        
        set
        {
            let count = SamplePosition.zero.distance(to: newValue)
            seek(toFileOffset: UInt64(count.byteCount))
        }
    }
    
    @discardableResult
    func readFrames(into buffer: AudioBuffer,
                    at position: SamplePosition = .zero,
                    count: SampleCount? = nil) -> SampleCount
    {
        Logger.stream.debug("FileHandle.readFrames(into:at:count:)")
        
        assert(position < buffer.end, "Position out of range")

        let end = count.flatMap { SamplePosition.zero.advanced(by: $0) } ?? buffer.end
        assert(end <= buffer.end, "Frame count out of range")

        let data = readData(ofLength: position.distance(to: end).byteCount)
        let samplesRead = SampleCount(byteCount: data.count)

        data.withUnsafeBytes
        { bytes in

            let samples = bytes.bindMemory(to: Sample.self)
            buffer.copy(from: samples,
                        range: ..<samplesRead,
                        to: position)
        }

        return samplesRead
    }
}
