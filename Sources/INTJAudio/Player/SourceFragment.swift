//
//  SourceFragment.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/21/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

struct SourceFragment
{
    let trackRange: SampleRange
    let source: SourceProtocol
    let sourceStart: SamplePosition
    
    func prepare(for dimensions: AudioDimensions)
    {
        source.prepare(for: dimensions)
    }

    func render(into output: [AudioBuffer], for position: SamplePosition)
    {
        var renderRange = position ..< position.advanced(by: output.frameCount)

        guard renderRange.overlaps(trackRange) else { return }
        
        var writePosition: SamplePosition = .zero
        
        if renderRange.start < trackRange.start
        {
            writePosition.advance(by: renderRange.start.distance(to: trackRange.start))
            renderRange = trackRange.start ..< renderRange.end
        }
        
        if renderRange.end > trackRange.end
        {
            renderRange = renderRange.start ..< trackRange.end
        }
        
        let sourceRange = renderRange.converted(fromBase: trackRange.start, toBase: sourceStart)
        
        source.read(into: output, from: sourceRange, to: writePosition)
    }
}
