//
//  AggregateDeviceTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 5/28/24.
//

import CoreAudio
@testable import INTJAudio
import XCTest

class AggregateDeviceTests: XCTestCase
{
    func testAggregateDevice() throws
    {
        let mockInterface = MockAudioObjectInterface()
        var latestConfiguration: [String: Any]?
                
        mockInterface.didCallHandler = { call in
            if call.function.hasPrefix("setPropertyData"),
               let data = call.arguments["data"] as? UnsafeRawPointer
            {
                let config = data.assumingMemoryBound(to: CFDictionary.self).pointee
                latestConfiguration = [String: Any](_immutableCocoaDictionary: config)
            }
                    
            return nil
        }
        
        let mockDevice1 = MockDevice()
        mockDevice1.id = 17
        mockDevice1.title = "Device 17"
        mockDevice1.identifier = "Device-17"
        mockDevice1.inputChannelTitles = [ "Device 17" ]
        mockDevice1.outputChannelTitles = [ "Left", "Right" ]
        mockDevice1.frameCount = 512

        let mockDevice2 = MockDevice()
        mockDevice2.id = 23
        mockDevice2.title = "Device 23"
        mockDevice2.identifier = "Device-23"
        mockDevice2.inputChannelTitles = [ "Left", "Right" ]
        mockDevice2.outputChannelTitles = [ "Left", "Right" ]
        mockDevice2.frameCount = 512

        let mockDevice3 = MockDevice()
        mockDevice3.id = 39
        mockDevice3.title = "Device 39"
        mockDevice3.identifier = "Device-39"
        mockDevice3.inputChannelTitles = [ "Left", "Right" ]
        mockDevice3.outputChannelTitles = [ "1", "2", "3", "4" ]
        mockDevice3.frameCount = 512

        mockInterface.mockDevices = [ mockDevice1, mockDevice2, mockDevice3 ]
        
        let manager = DeviceManager(with: mockInterface)
        let aggregate = try manager.createAggregateDevice(title: "INTJ Aggregate Device")
            
        XCTAssertEqual(aggregate.id, 99)
        XCTAssertEqual(aggregate.title, "INTJ Aggregate Device")
        XCTAssertEqual(aggregate.identifier, "INTJ-Aggregate-Device")
        XCTAssertEqual(aggregate.subdeviceIdentifiers?.count, 0)
        
        aggregate.subdeviceIdentifiers = [
            mockDevice1.identifier,
            mockDevice2.identifier,
            mockDevice3.identifier
        ]

        let config1 = try XCTUnwrap(latestConfiguration)
        XCTAssertEqual(config1["clock"] as? String, "Device-17")
        XCTAssertTrue(config1["private"] as! Bool)
        XCTAssertFalse(config1["stacked"] as! Bool)
        XCTAssertEqual(config1["uid"] as? String, "INTJ-Aggregate-Device")
        XCTAssertEqual(config1["name"] as? String, "INTJ Aggregate Device")
        
        let subdevices1 = latestConfiguration?["subdevices"] as! [[String: Any]]
        XCTAssertEqual(subdevices1.count, 3)
        
        XCTAssertEqual(subdevices1[0]["name"] as! String, "Device 17")
        XCTAssertEqual(subdevices1[0]["uid"] as! String, "Device-17")
        XCTAssertEqual(subdevices1[0]["channels-in"] as! Int, 1)
        XCTAssertEqual(subdevices1[0]["channels-out"] as! Int, 2)
        
        XCTAssertEqual(subdevices1[1]["name"] as! String, "Device 23")
        XCTAssertEqual(subdevices1[1]["uid"] as! String, "Device-23")
        XCTAssertEqual(subdevices1[1]["channels-in"] as! Int, 2)
        XCTAssertEqual(subdevices1[1]["channels-out"] as! Int, 2)
        XCTAssertEqual(subdevices1[1]["drift"] as! Int, 1)
        XCTAssertEqual(subdevices1[1]["drift quality"] as! UInt32, kAudioSubDeviceDriftCompensationMediumQuality)
        
        XCTAssertEqual(subdevices1[2]["name"] as! String, "Device 39")
        XCTAssertEqual(subdevices1[2]["uid"] as! String, "Device-39")
        XCTAssertEqual(subdevices1[2]["channels-in"] as! Int, 2)
        XCTAssertEqual(subdevices1[2]["channels-out"] as! Int, 4)
        XCTAssertEqual(subdevices1[2]["drift"] as! Int, 1)
        XCTAssertEqual(subdevices1[2]["drift quality"] as! UInt32, kAudioSubDeviceDriftCompensationMediumQuality)

        aggregate.subdeviceIdentifiers = []
        
        let config2 = try XCTUnwrap(latestConfiguration)
        let subdevices2 = config2["subdevices"] as! [[String: Any]]
        XCTAssertEqual(subdevices2.count, 0)
        XCTAssertNil(config2["clock"] as? String)
    }
}

