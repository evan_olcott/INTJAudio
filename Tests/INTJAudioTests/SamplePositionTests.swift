//
//  SamplePositionTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class SamplePositionTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(SamplePosition(rawValue: 4).rawValue, 4)
    }
    
    func testInitAsInt()
    {
        XCTAssertEqual(SamplePosition(4).rawValue, 4)
    }
    
    func testInitAsInt64()
    {
        let position = SamplePosition(Int64.max)
        XCTAssertEqual(position.rawValue, Int(Int64.max))
    }
    
    func testInitAsDouble()
    {
        XCTAssertEqual(SamplePosition(4.0).rawValue, 4)
    }
    
    func testInitAsCGFloat()
    {
        XCTAssertEqual(SamplePosition(CGFloat(4.0)).rawValue, 4)
    }
    
    func testWithTimeLocation()
    {
        XCTAssertEqual(SamplePosition(location: 2.0, sampleRate: Hertz(100)).rawValue, 200)
        XCTAssertEqual(SamplePosition(location: 2.001, sampleRate: Hertz(100)).rawValue, 200)
        XCTAssertEqual(SamplePosition(location: 2.006, sampleRate: Hertz(100)).rawValue, 201)
    }
    
    func testZero()
    {
        XCTAssertEqual(SamplePosition.zero.rawValue, 0)
    }
    
    func testAdvancedBy()
    {
        XCTAssertEqual(SamplePosition(150).advanced(by: SampleCount(120)).rawValue, 270)
    }
    
    func testRecededBy()
    {
        XCTAssertEqual(SamplePosition(150).receded(by: SampleCount(120)).rawValue, 30)
    }
    
    func testDistanceTo()
    {
        XCTAssertEqual(SamplePosition(150).distance(to: SamplePosition(275)).rawValue, 125)
        XCTAssertEqual(SamplePosition(150).distance(to: SamplePosition(125)).rawValue, -25)
    }
    
    func testAbsoluteDistanceTo()
    {
        XCTAssertEqual(SamplePosition(150).absoluteDistance(to: SamplePosition(275)).rawValue, 125)
        XCTAssertEqual(SamplePosition(150).absoluteDistance(to: SamplePosition(125)).rawValue, 25)
    }
    
    func testQuantizedTo()
    {
        XCTAssertEqual(SamplePosition(3118).quantized(to: SampleCount(120)).rawValue, 3000)
    }
    
    func testClampedTo()
    {
        XCTAssertEqual(SamplePosition(99).clamped(to: 100 ..< 200).rawValue, 100)
        XCTAssertEqual(SamplePosition(100).clamped(to: 100 ..< 200).rawValue, 100)
        XCTAssertEqual(SamplePosition(200).clamped(to: 100 ..< 200).rawValue, 200)
        XCTAssertEqual(SamplePosition(201).clamped(to: 100 ..< 200).rawValue, 200)
    }
    
    func testRelativeTo()
    {
        XCTAssertEqual(SamplePosition(30).relative(to: SamplePosition(50)).rawValue, -20)
    }

    func testNormalizedRelativeTo()
    {
        XCTAssertEqual(SamplePosition(30).normalizedRelative(to: 20 ..< 40), 0.5)
    }

    func testEquality()
    {
        let length1 = SamplePosition(2)
        let length2 = SamplePosition(5)
        
        XCTAssertTrue(length1 == length1)
        XCTAssertFalse(length1 == length2)
    }
    
    func testComparable()
    {
        let length1 = SamplePosition(2)
        let length2 = SamplePosition(5)
        
        XCTAssertTrue(length1 < length2)
        XCTAssertFalse(length1 > length2)
        XCTAssertTrue(length2 > length1)
    }
    
    func testStringValue()
    {
        XCTAssertNil(SamplePosition("ABCDE"))
        XCTAssertEqual(SamplePosition("12345")?.rawValue, 12345)
        XCTAssertEqual(String(SamplePosition(173)), "173")
    }
    
    func testCasting()
    {
        let position = SamplePosition(5756)
        
        XCTAssertEqual(Int(position), 5756)
        XCTAssertEqual(Float(position), 5756.0)
        XCTAssertEqual(Double(position), 5756.0)
        XCTAssertEqual(CGFloat(position), 5756.0)
        XCTAssertEqual(Int64(position), 5756)
        XCTAssertEqual(UInt64(position), 5756)
    }
}
