//
//  PlayerTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 11/7/22.
//

import AVFoundation
@testable import INTJAudio
import XCTest

class PlayerTests: XCTestCase
{
    var mockArrangement: MockArrangement?
    let outputDeviceID = AudioObjectID(12)
    var player: Player?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        mockArrangement = MockArrangement()
                
        player = Player(with: try XCTUnwrap(mockArrangement))
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        player = nil
        mockArrangement = nil
    }
    
    func testPositionLocation() async throws
    {
        let player = try XCTUnwrap(player)
        
        player.position = 24000
        XCTAssertEqual(player.location, 0.5)
        
        player.location = 1.0
        XCTAssertEqual(player.position, 48000)
    }
        
    func testRender() async throws
    {
        let player = try XCTUnwrap(player)
        let arrangement = try XCTUnwrap(mockArrangement)
        let buffer = AudioBuffer(count: 512)
        
        player.position = 20480
        player.render(into: [buffer])
        
        XCTAssertEqual(arrangement.calls.count, 1)
        XCTAssertEqual(arrangement.calls[0].function, "render(into:for:)")
        XCTAssertEqual(player.position, 20992)
    }
    
    func testRenderStop() async throws
    {
        let player = try XCTUnwrap(player)
        let arrangement = try XCTUnwrap(mockArrangement)
        let buffer = AudioBuffer(count: 512)
        let expectation = expectation(description: "didStop")
        
        let landmark = Landmark(position: 20500, action: .stop)
        arrangement.landmarks.append(landmark)
                
        player.didStopHandler = {
            expectation.fulfill()
        }
        
        player.position = 20480
        player.render(into: [buffer])
        await fulfillment(of: [expectation], timeout: 2.0)
        
        player.render(into: [buffer])

        XCTAssertEqual(arrangement.calls.count, 1)
        XCTAssertEqual(arrangement.calls[0].function, "render(into:for:)")
        XCTAssertEqual((arrangement.calls[0].arguments["into"] as? [INTJAudio.AudioBuffer])?.frameCount, 20)
        XCTAssertEqual(arrangement.calls[0].arguments["for"] as? SamplePosition, 20480)
        XCTAssertEqual(player.position, 20500)
    }
    
    func testRenderJump() async throws
    {
        let player = try XCTUnwrap(player)
        let arrangement = try XCTUnwrap(mockArrangement)
        let buffer = AudioBuffer(count: 512)
        
        let destination = Landmark(position: 32000, action: .none)
        let jump = Landmark(position: 20500, action: .jump(destination))
        arrangement.landmarks.append(contentsOf: [ jump, destination ])
        
        player.position = 20480
        player.render(into: [buffer])
        
        XCTAssertEqual(arrangement.calls.count, 2)
        XCTAssertEqual(arrangement.calls[0].function, "render(into:for:)")
        XCTAssertEqual((arrangement.calls[0].arguments["into"] as? [INTJAudio.AudioBuffer])?.frameCount, 20)
        XCTAssertEqual(arrangement.calls[0].arguments["for"] as? SamplePosition, 20480)
        XCTAssertEqual(arrangement.calls[1].function, "render(into:for:)")
        XCTAssertEqual((arrangement.calls[1].arguments["into"] as? [INTJAudio.AudioBuffer])?.frameCount, 492)
        XCTAssertEqual(arrangement.calls[1].arguments["for"] as? SamplePosition, 32000)

        XCTAssertEqual(player.position, 32492)
    }
}
