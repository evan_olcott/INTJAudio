//
//  Locked.swift
//  INTJAudio
//
//  Created by Evan Olcott on 1/5/25.
//

import Foundation

@propertyWrapper
public struct Locked<Value>: ~Copyable
{
    private var value: Value
    private var mutex: UnsafeMutablePointer<pthread_mutex_t>
    
    public init(wrappedValue: Value)
    {
        self.value = wrappedValue
        
        mutex = UnsafeMutablePointer.allocate(capacity: 1)
        pthread_mutex_init(mutex, nil)
    }
    
    deinit
    {
        pthread_mutex_destroy(mutex)
        mutex.deallocate()
    }
    
    public var wrappedValue: Value
    {
        get
        {
            pthread_mutex_lock(mutex)
            defer { pthread_mutex_unlock(mutex) }
            
            return value
        }
        
        set
        {
            pthread_mutex_lock(mutex)
            value = newValue
            pthread_mutex_unlock(mutex)
        }
    }
}
