//
//  WindowBuffer.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/28/24.
//

import Accelerate
import Foundation

public class WindowBuffer: AudioBuffer
{
    public enum Sequence: String, Codable, Sendable
    {
        case rectangular
        case hanning
        case hamming
        case gaussian
        case bartlett
        case triangular
        case bartlettHanning
        case blackman
        case nuttall
        case blackmanHarris
        case blackmanNuttall
        case flatTop
        case bohman
    }

    public required init(rawValue value: UnsafeMutableBufferPointer<Sample>)
    {
        super.init(rawValue: value)
    }
        
    public convenience init(usingSequence sequence: Sequence, resolution: Resolution)
    {
        self.init(usingSequence: sequence, count: resolution.sampleCount)
    }
    
    public init(usingSequence sequence: Sequence, count: SampleCount)
    {
        super.init(count: count)
        
        guard count != .zero else { return }
        
        let w = Float(count)
        let h = w / 2
        var WF: ((Float) -> Float)
        
        switch sequence
        {
        case .hanning:
            WF = { 0.5 * (1 - cos((2 * Float.pi * $0) / w)) }
        case .hamming:
            WF = { 0.53836 - (0.46164 * cos((2 * Float.pi * $0) / w)) }
        case .gaussian:
            WF = {
                let m = 2.5 * (($0 - h) / h)
                return pow(exp(1), -0.5 * m * m)
            }
        case .bartlett:
            WF = {
                let m = w - 1
                return (2.0 / m) * ((m / 2) - abs($0 - (m / 2)))
            }
        case .triangular:
            WF = { (2.0 / w) * (h - abs($0 - ((w - 1) / 2))) }
        case .bartlettHanning:
            WF = {
                let a = 0.48 * abs(($0 / w) - 0.5)
                let b = 0.38 * cos((2 * Float.pi * $0) / w)
                return 0.62 - a - b
            }
        case .blackman:
            WF = {
                let a = 0.5 * cos((2 * Float.pi * $0) / w)
                let b = 0.08 * cos((4 * Float.pi * $0) / w)
                return 0.42 - a + b
            }
        case .nuttall:
            WF = {
                let a = 0.487396 * cos((2 * Float.pi * $0) / w)
                let b = 0.144232 * cos((4 * Float.pi * $0) / w)
                let c = 0.012604 * cos((6 * Float.pi * $0) / w)
                return 0.355768 - a + b - c
            }
        case .blackmanHarris:
            WF = {
                let a = 0.48829 * cos((2 * Float.pi * $0) / w)
                let b = 0.14128 * cos((4 * Float.pi * $0) / w)
                let c = 0.011168 * cos((6 * Float.pi * $0) / w)
                return 0.35875 - a + b - c
            }
        case .blackmanNuttall:
            WF = {
                let a = 0.4891775 * cos((2 * Float.pi * $0) / w)
                let b = 0.1365995 * cos((4 * Float.pi * $0) / w)
                let c = 0.0106411 * cos((6 * Float.pi * $0) / w)
                return 0.3635819 - a + b - c
            }
        case .flatTop:
            WF = {
                let a = 1.93 * cos((2 * Float.pi * $0) / w)
                let b = 1.29 * cos((4 * Float.pi * $0) / w)
                let c = 0.388 * cos((6 * Float.pi * $0) / w)
                let d = 0.0322 * cos((8 * Float.pi * $0) / w)
                return 1 - a + b - c + d
            }
        case .bohman:
            WF = {
                let m = abs($0 - h) / h
                return ((1.0 - m) * cos(Float.pi * m)) + (sin(Float.pi * m) / Float.pi)
            }
        case .rectangular:
            WF = { _ in 1 }
        }
        
        for n in SampleRange(count: count)
        {
            self[n] = WF(Float(n))
        }
    }
}
