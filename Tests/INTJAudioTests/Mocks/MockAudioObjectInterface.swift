//
//  MockAudioObjectInterface.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/9/22.
//

import Collections
import CoreAudio
import Foundation
import INTJAudio
import OSLog

class MockAudioObjectInterface: MockObject, AudioObjectInterfaceProtocol
{
    var mockDevices = [MockDevice]()
    
    func hasProperty(with ID: AudioObjectID,
                     address: UnsafePointer<AudioObjectPropertyAddress>) -> Bool
    {
        _ = addCall(to: #function, with: [ "with": ID, "address": address ])
        
        switch address.pointee.mSelector
        {
        case kAudioObjectPropertyName,
        kAudioDevicePropertyDeviceUID,
        kAudioDevicePropertyStreamConfiguration,
        kAudioDevicePropertyBufferFrameSize,
        kAudioObjectPropertyElementName:
            return true
            
        case kAudioAggregateDevicePropertyComposition:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            return device.isAggregate
            
        default:
            fatalError("Unhandled mSelector")
        }
    }
    
    func propertyDataSize(of ID: AudioObjectID,
                          address: UnsafePointer<AudioObjectPropertyAddress>,
                          outDataSize: UnsafeMutablePointer<UInt32>) throws
    {
        _ = try addCallThrowing(to: #function, with: [ "of": ID, "address": address, "outDataSize": outDataSize ])
        
        var size: Int = 0
        switch address.pointee.mSelector
        {
        case kAudioHardwarePropertyDevices:
            size = MemoryLayout<AudioDeviceID>.size * mockDevices.count
            
        case kAudioObjectPropertyName,
            kAudioObjectPropertyElementName,
        kAudioDevicePropertyDeviceUID:
            size = MemoryLayout<CFString>.size
            
        case kAudioDevicePropertyStreamConfiguration:
            size = MemoryLayout<AudioBufferList>.size
            
        case kAudioDevicePropertyBufferFrameSize:
            size = MemoryLayout<Int32>.size
            
        default:
            fatalError("Unhandled mSelector")
        }
        outDataSize.pointee = UInt32(size)
    }
    
    func propertyData(of ID: AudioObjectID,
                      address: UnsafePointer<AudioObjectPropertyAddress>,
                      dataSize: UnsafeMutablePointer<UInt32>,
                      outData: UnsafeMutableRawPointer) throws
    {
        _ = try addCallThrowing(to: #function, with: [ "of": ID, "address": address, "dataSize": dataSize, "outData": outData ])
        
        switch address.pointee.mSelector
        {
        case kAudioHardwarePropertyDevices:
            dataSize.pointee = UInt32(MemoryLayout<AudioDeviceID>.size * mockDevices.count)
            let typed = outData.bindMemory(to: AudioDeviceID.self, capacity: mockDevices.count)
            for i in 0 ..< mockDevices.count { typed[i] = mockDevices[i].id }
            
        case kAudioHardwarePropertyDefaultInputDevice:
            dataSize.pointee = UInt32(MemoryLayout<AudioDeviceID>.size)
            let typed = outData.bindMemory(to: AudioDeviceID.self, capacity: 1)
            typed[0] = mockDevices.first { $0.isDefaultInput }?.id ?? 0

        case kAudioHardwarePropertyDefaultOutputDevice:
            dataSize.pointee = UInt32(MemoryLayout<AudioDeviceID>.size)
            let typed = outData.bindMemory(to: AudioDeviceID.self, capacity: 1)
            typed[0] = mockDevices.first { $0.isDefaultOutput }?.id ?? 0

        case kAudioObjectPropertyName:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            dataSize.pointee = UInt32(MemoryLayout<CFString>.size)
            let typed = outData.bindMemory(to: CFString.self, capacity: 1)
            typed[0] = device.title as CFString
            
        case kAudioDevicePropertyDeviceUID:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            dataSize.pointee = UInt32(MemoryLayout<CFString>.size)
            let typed = outData.bindMemory(to: CFString.self, capacity: 1)
            typed[0] = device.identifier as CFString
            
        case kAudioDevicePropertyStreamConfiguration:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            dataSize.pointee = UInt32(MemoryLayout<AudioBufferList>.size)
            var channelCount = 0
            if address.pointee.mScope == kAudioObjectPropertyScopeInput {
                channelCount = device.inputChannelTitles.count
            } else if address.pointee.mScope == kAudioObjectPropertyScopeOutput {
                channelCount = device.outputChannelTitles.count
            } else {
                fatalError("Unhandled scope")
            }
            
            let list = AudioBufferList(mNumberBuffers: 1,
                                       mBuffers: AudioBuffer(mNumberChannels: UInt32(channelCount),
                                                             mDataByteSize: 0,
                                                             mData: nil))
            let typed = outData.bindMemory(to: AudioBufferList.self, capacity: 1)
            typed[0] = list
            
        case kAudioDevicePropertyBufferFrameSize:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            dataSize.pointee = UInt32(MemoryLayout<Int32>.size)
            let typed = outData.bindMemory(to: Int32.self, capacity: 1)
            typed[0] = Int32(device.frameCount)
            
        case kAudioObjectPropertyElementName:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            dataSize.pointee = UInt32(MemoryLayout<CFString>.size)
            let typed = outData.bindMemory(to: CFString.self, capacity: 1)
            var name = ""
            if address.pointee.mScope == kAudioObjectPropertyScopeInput {
                name = device.inputChannelTitles[Int(address.pointee.mElement - 1)]
            } else if address.pointee.mScope == kAudioObjectPropertyScopeOutput {
                name = device.outputChannelTitles[Int(address.pointee.mElement - 1)]
            } else {
                fatalError("Unhandled scope")
            }

            typed[0] = name as CFString

        case kAudioAggregateDevicePropertyComposition:
            guard let device = mockDevices.first(where: { $0.id == ID }) else { fatalError("Bad AudioObjectID") }
            dataSize.pointee = UInt32(MemoryLayout<CFDictionary>.size)
            let dict = try aggregateDeviceDescription(title: device.title,
                                                      identifier: device.identifier,
                                                      subdeviceIdentifiers: Array(device.subdeviceIdentifiers!))
            let typed = outData.bindMemory(to: CFDictionary.self, capacity: 1)
            typed[0] = dict as CFDictionary

        default:
            fatalError("Unhandled mSelector")
        }
    }
    
    func setPropertyData(of ID: AudioObjectID,
                         address: UnsafePointer<AudioObjectPropertyAddress>,
                         dataSize: UInt32,
                         data: UnsafeRawPointer) throws
    {
        try addCallThrowing(to: #function, with: [ "of": ID, "address": address, "dataSize": dataSize, "data": data ])
    }
    
    func addPropertyListener(to ID: AudioObjectID,
                             address: UnsafePointer<AudioObjectPropertyAddress>,
                             handler: @escaping AudioObjectPropertyListenerBlock) throws
    {
        try addCallThrowing(to: #function, with: [ "to": ID, "address": address, "handler": handler ])
    }
    
    func removePropertyListener(to ID: AudioObjectID,
                                address: UnsafePointer<AudioObjectPropertyAddress>,
                                handler: @escaping AudioObjectPropertyListenerBlock) throws
    {
        try addCallThrowing(to: #function, with: [ "to": ID, "address": address, "handler": handler ])
    }
    
    func createAggregateDevice(description: CFDictionary, outDeviceID: UnsafeMutablePointer<AudioObjectID>) throws
    {
        _ = try addCallThrowing(to: #function, with: [ "description": description, "outDeviceID": outDeviceID ])
        
        guard let desc = description as? [String: Any] else { fatalError("Invalid description") }
        
        let aggregate = MockDevice()
        aggregate.id = 99
        aggregate.identifier = desc[kAudioAggregateDeviceUIDKey] as! String
        aggregate.title = desc[kAudioAggregateDeviceNameKey] as! String
        aggregate.isAggregate = true
        let subdevices = desc[kAudioAggregateDeviceSubDeviceListKey] as! [[String: Any]]
        aggregate.subdeviceIdentifiers = OrderedSet<String>(subdevices.map { $0[kAudioSubDeviceUIDKey] as! String })
        mockDevices.append(aggregate)
        
        outDeviceID.pointee = aggregate.id
    }
    
    func destroyAggregateDevice(id: AudioObjectID) throws
    {
        try addCallThrowing(to: #function, with: [ "id": id ])
    }
    
    //
    // MARK: - Running
    //
    
    func createIOProcID(id: AudioObjectID,
                        handler: @escaping AudioDeviceIOProc,
                        clientData: UnsafeMutableRawPointer?,
                        handlerID: UnsafeMutablePointer<AudioDeviceIOProcID?>) throws
    {
        try addCallThrowing(to: #function, with: [ "id": id, "handlerID": handlerID ])
    }
    
    func destroyIOProcID(id: AudioObjectID,
                         handlerID: @escaping AudioDeviceIOProcID) throws
    {
        try addCallThrowing(to: #function, with: [ "id": id, "handlerID": handlerID ])
    }
    
    func start(id: AudioObjectID,
               handlerID: AudioDeviceIOProcID?) throws
    {
        try addCallThrowing(to: #function, with: [ "id": id, "handlerID": handlerID ])
    }
    
    func stop(id: AudioObjectID,
              handlerID: AudioDeviceIOProcID?) throws
    {
        try addCallThrowing(to: #function, with: [ "id": id, "handlerID": handlerID ])
    }
    
    func reset() throws
    {
        try addCallThrowing(to: #function)
    }
}
