//
//  SampleRangeTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class SampleRangeTests: XCTestCase
{
    func testInit()
    {
        let range = SampleRange()
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 0)
    }
    
    func testInitRawValue()
    {
        let range = SampleRange(rawValue: 100 ..< 200)
        XCTAssertEqual(range.rawValue.lowerBound, 100)
        XCTAssertEqual(range.rawValue.upperBound, 200)
    }
    
    func testInitSamplePositionRange()
    {
        let range = SamplePosition(5) ..< SamplePosition(7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitSamplePosition()
    {
        let range = ..<SamplePosition(7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitSampleCount()
    {
        let range = ..<SampleCount(7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testEmpty()
    {
        XCTAssertEqual(SampleRange.empty.rawValue, 0 ..< 0)
    }
    
    func testMid()
    {
        XCTAssertEqual(SampleRange(start: 400, end: 700).mid, 550)
    }
    
    func testEncode() throws
    {
        let range = SampleRange()
        let data = try JSONEncoder().encode(range)
        let string = String(data: data, encoding: .utf8)
        XCTAssertEqual(string, "[0,0]")
    }
    
    func testDecode() throws
    {
        let string = "[100,200]"
        let data = string.data(using: .utf8)!
        let range = try JSONDecoder().decode(SampleRange.self, from: data)
        XCTAssertEqual(range.rawValue.lowerBound, 100)
        XCTAssertEqual(range.rawValue.upperBound, 200)
    }
    
    func testDecodeFailure()
    {
        let string = "[200]"
        let data = string.data(using: .utf8)!
        XCTAssertThrowsError(try JSONDecoder().decode(SampleRange.self, from: data))
    }
    
    func testInitWithTimeRange()
    {
        let range = SampleRange(range: 5.0 ... 7.0, sampleRate: Hertz(100))
        
        XCTAssertEqual(range.rawValue.lowerBound, 500)
        XCTAssertEqual(range.rawValue.upperBound, 700)
    }
    
    func testInitWithStartEnd()
    {
        let range = SampleRange(start: 5, end: 7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testInitWithEnd()
    {
        let range = SampleRange(end: 7)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testWithStartCount()
    {
        let range = SampleRange(start: 5, count: 2)
        
        XCTAssertEqual(range.rawValue.lowerBound, 5)
        XCTAssertEqual(range.rawValue.upperBound, 7)
    }
    
    func testWithCount()
    {
        let range = SampleRange(count: 2)
        
        XCTAssertEqual(range.rawValue.lowerBound, 0)
        XCTAssertEqual(range.rawValue.upperBound, 2)
    }
    
    func testStart()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertEqual(range.start, SamplePosition(500))
    }
    
    func testEnd()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertEqual(range.end, SamplePosition(700))
    }
    
    func testIsEmpty()
    {
        let range: SampleRange = 500 ..< 500
        XCTAssertTrue(range.isEmpty)
    }
    
    func testCount()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertEqual(range.count, SampleCount(200))
    }
    
    func testClampedTo()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertEqual(range.clamped(to: 400 ..< 650).rawValue, 500 ..< 650)
        XCTAssertEqual(range.clamped(to: 600 ..< 750).rawValue, 600 ..< 700)
        XCTAssertEqual(range.clamped(to: 400 ..< 900).rawValue, 500 ..< 700)
    }
    
    func testOverlaps()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertFalse(range.overlaps(300 ..< 500))
        XCTAssertTrue(range.overlaps(300 ..< 550))
        XCTAssertTrue(range.overlaps(550 ..< 650))
        XCTAssertTrue(range.overlaps(650 ..< 750))
        XCTAssertFalse(range.overlaps(750 ..< 900))
    }
    
    func testIntersection()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertNil(range.intersection(300 ..< 500))
        XCTAssertEqual(range.intersection(300 ..< 550), 500 ..< 550)
        XCTAssertEqual(range.intersection(550 ..< 650), 550 ..< 650)
        XCTAssertEqual(range.intersection(650 ..< 750), 650 ..< 700)
        XCTAssertNil(range.intersection(750 ..< 900))
    }
    
    func testRelativeTo()
    {
        let range: SampleRange = 500 ..< 700
        XCTAssertEqual(range.relative(to: 200), 300 ..< 500)
    }
    
    func testContains()
    {
        let range: SampleRange = 500 ..< 700
        
        XCTAssertFalse(range.contains(SamplePosition(499)))
        XCTAssertTrue(range.contains(SamplePosition(500)))
        XCTAssertTrue(range.contains(SamplePosition(501)))
        
        XCTAssertTrue(range.contains(SamplePosition(699)))
        XCTAssertFalse(range.contains(SamplePosition(700)))
        XCTAssertFalse(range.contains(SamplePosition(701)))
    }
    
    func testEquality()
    {
        let range1 = SampleRange(start: 5, end: 7)
        let range2 = SampleRange(start: 6, end: 9)
        
        XCTAssertTrue(range1 == range1)
        XCTAssertFalse(range1 == range2)
    }
    
    func testAdvanced()
    {
        XCTAssertEqual(SampleRange(start: 400, end: 700).advanced(by: 350), SampleRange(start: 750, end: 1050))
    }
    
    func testReceded()
    {
        XCTAssertEqual(SampleRange(start: 400, end: 700).receded(by: 350), SampleRange(start: 50, end: 350))
    }
    
    func testConverted()
    {
        let range = SampleRange(start: 1400, end: 1500)
        let converted = range.converted(fromBase: 1300, toBase: 200)
        XCTAssertEqual(converted, SampleRange(start: 300, end: 400))
    }
}
