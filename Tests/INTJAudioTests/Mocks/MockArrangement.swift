//
//  MockArrangement.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 11/7/22.
//

import Foundation
import INTJAudio

final class MockArrangement: MockObject, ArrangementProtocol
{
    var tracks = [TrackProtocol]()
    var landmarks = [Landmark]()
        
    func prepare(for dimensions: AudioDimensions) throws
    {
        try addCallThrowing(to: #function, with: [ "for": dimensions ])
    }
    
    func render(into buffer: [AudioBuffer], for position: SamplePosition)
    {
        addCall(to: #function, with: [ "into": buffer, "for": position ])
    }
}
