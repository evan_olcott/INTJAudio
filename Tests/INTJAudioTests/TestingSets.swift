//
//  TestingSets.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/27/24.
//

@testable import INTJAudio
import Foundation

var TestingTones: [Hertz]
{
    [
        400,
        630,
        1_000,
        2_500,
        4_000,
        6_300,
        10_000,
        12_500
    ]
}

var TestingResolutions: [Resolution]
{
    [ 10, 11, 12, 13, 14, 15 ]
}

var TestingDensities: [(Decimator.Density, Int)]
{
    [
        (.oneOctave, 15),
        (.thirdOctave, 44),
        (.sixthOctave, 87),
        (.twelfthOctave, 174),
        (.twentyFourthOctave, 348),
        (.fourtyEighthOctave, 697),
        (.ISO15, 15),
        (.ISO31, 31),
        (.critical, 25)
    ]
}

extension AudioBuffer
{
    convenience init(tone: Hertz, count: SampleCount)
    {
        self.init(count: count)
        
        let m = Float((.pi * 2) * tone.rawValue / Hertz.samplingRate.rawValue)
        var phase: Float = 0
        
        for i in SampleRange(count: count)
        {
            self[i] = sin(phase)
            
            phase += m
            phase.formTruncatingRemainder(dividingBy: .pi * 2)
        }
    }
}
