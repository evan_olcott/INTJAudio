//
//  MockDeviceManager.swift
//  INTJAudio
//
//  Created by Evan Olcott on 5/28/24.
//

@testable import INTJAudio
import XCTest

final class MockDeviceManager: MockObject, DeviceManagerProtocol
{    
    var devices: [DeviceProtocol]
    {
        addCall(to: #function, with: [:]) as? [DeviceProtocol] ?? []
    }
}
