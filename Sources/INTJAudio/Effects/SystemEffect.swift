//
//  SystemEffect.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/7/22.
//

import AudioToolbox
import AVFoundation
import CoreAudioKit
import Foundation
import OSLog

public final class SystemEffect: Effect
{
    public var isEnabled = true
    
    let avAudioUnit: AVAudioUnit
    private var timestamp = AudioTimeStamp()
    private var renderBuffer: AVAudioPCMBuffer?
    // swiftlint:disable discouraged_optional_collection
    private var processBuffer: [AudioBuffer]?
    // swiftlint:enable discouraged_optional_collection

    private let interface: AudioUnitInterfaceProtocol
    var windowController: NSWindowController?

    public convenience init(with description: AudioComponentDescription) async throws
    {
        try await self.init(with: description, in: AudioUnitInterface())
    }

    init(with description: AudioComponentDescription,
         in interface: AudioUnitInterfaceProtocol) async throws
    {
        self.interface = interface
        
        avAudioUnit = try await interface.instantiate(with: description, options: [ ])
        
        let vSelf = UnsafeMutableRawPointer(Unmanaged.passRetained(self).toOpaque())
        var myCallback = AURenderCallbackStruct()
        myCallback.inputProc = audioUnitCallback
        myCallback.inputProcRefCon = vSelf

        try setValue(of: kAudioUnitProperty_SetRenderCallback,
                     in: kAudioUnitScope_Output,
                     to: myCallback)
    }
    
    public var displayName: String
    {
        avAudioUnit.name
    }
    
    public func prepare(for dimensions: AudioDimensions) throws
    {
        let unit = avAudioUnit.auAudioUnit
        
        if interface.renderResourcesAllocated(for: unit) {
            interface.deallocateRenderResources(for: unit)
        }

        let format = AVAudioFormat(standardFormatWithChannelCount: dimensions.channelCount)
        try interface.setFormat(of: unit, onInputBus: 0, to: format)
        try interface.setFormat(of: unit, onOutputBus: 0, to: format)
        try interface.setMaximumFramesToRender(of: unit, to: AUAudioFrameCount(dimensions.frameCount))

        try interface.allocateRenderResources(for: unit)

        renderBuffer = AVAudioPCMBuffer(pcmFormat: format,
                                        frameCapacity: AVAudioFrameCount(dimensions.frameCount))
    }
    
    public func process(_ buffer: [AudioBuffer], for position: SamplePosition)
    {
        guard isEnabled, let renderBuffer else { return }

        // setup argument values
        
        var flags = AudioUnitRenderActionFlags()
        timestamp.mSampleTime = Float64(position)

        processBuffer = buffer
        renderBuffer.frameLength = AVAudioFrameCount(buffer.frameCount)

        // ask AudioUnit to render into renderBuffer
        
        _ = interface.render(avAudioUnit.audioUnit,
                             flags: &flags,
                             timestamp: &timestamp,
                             bus: 0,
                             frameCount: UInt32(buffer.frameCount),
                             buffer: renderBuffer.mutableAudioBufferList)

        // copy renderBuffer back into buffer
        
        for c in 0 ..< buffer.count
        {
            guard let renderedData = renderBuffer.floatChannelData?[c] else { continue }
            let renderedBuffer = UnsafeBufferPointer<Float>(start: renderedData, count: Int(buffer.frameCount))
            buffer[c].copy(from: renderedBuffer)
        }
    }
    
    private let audioUnitCallback: AURenderCallback =
    { (inRefCon: UnsafeMutableRawPointer,
        ioActionFlags: UnsafeMutablePointer<AudioUnitRenderActionFlags>,
        _: UnsafePointer<AudioTimeStamp>,
        _: UInt32,
        _: UInt32,
        ioData: UnsafeMutablePointer<AudioBufferList>?) -> OSStatus in

        guard
            ioActionFlags.pointee.contains(.unitRenderAction_PreRender) == false,
            ioActionFlags.pointee.contains(.unitRenderAction_PostRender) == false,
            let mutableOutOutputData = UnsafeMutablePointer<AudioBufferList>(mutating: ioData)
        else { return noErr }

        let myself = Unmanaged<SystemEffect>.fromOpaque(inRefCon).takeUnretainedValue()

        myself.pullInput(into: UnsafeMutableAudioBufferListPointer(mutableOutOutputData))

        return noErr
    }

    private func pullInput(into bufferList: UnsafeMutableAudioBufferListPointer)
    {
        guard let processBuffer else { return }
                
        bufferList.copy(from: processBuffer)
    }
    
    //
    // MARK: - Set/Get
    //
    
    func firstValue<T>(of property: AudioObjectPropertySelector,
                       in scope: AudioObjectPropertyScope = kAudioObjectPropertyScopeGlobal,
                       at element: AudioObjectPropertyElement = kAudioObjectPropertyElementMain,
                       as type: T.Type) throws -> T
    {
        let values = try value(of: property,
                               in: scope,
                               at: element,
                               as: type)
        guard let value = values.first else { throw AudioUnitInterface.Error.badProperty }
        return value
    }

    func value<T>(of property: AudioUnitPropertyID,
                  in scope: AudioUnitScope = kAudioUnitScope_Global,
                  at element: AudioUnitElement = 0,
                  as type: T.Type) throws -> [T]
    {
        let data = try data(of: property, in: scope, at: element)
        
        return data.withUnsafeBytes
        { bytes -> [T] in

            [T](bytes.bindMemory(to: type))
        }
    }

    func data(of property: AudioUnitPropertyID,
              in scope: AudioUnitScope = kAudioUnitScope_Global,
              at element: AudioUnitElement = 0) throws -> Data
    {
        var size = UInt32(0)
        
        try interface.propertyDataInfo(of: avAudioUnit.audioUnit,
                                       property: property,
                                       scope: scope,
                                       element: element,
                                       outDataSize: &size,
                                       outWritable: nil)

        var data = Data(count: Int(size))

        try data.withUnsafeMutableBytes
        { bytes in

            try interface.propertyData(of: avAudioUnit.audioUnit,
                                       property: property,
                                       scope: scope,
                                       element: element,
                                       outData: bytes.baseAddress!,
                                       ioDataSize: &size)
        }

        return data
    }

    func setValue<T>(of property: AudioUnitPropertyID,
                     in scope: AudioUnitScope = kAudioUnitScope_Global,
                     at element: AudioUnitElement = 0,
                     to value: T) throws
    {
        let size = UInt32(MemoryLayout<T>.size)
        var v = value
        
        try withUnsafePointer(to: &v) { vp in
            try interface.setPropertyData(of: avAudioUnit.audioUnit,
                                          property: property,
                                          scope: scope,
                                          element: element,
                                          inData: vp,
                                          inDataSize: size)
       }
    }
}
