//
//  RampTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

@testable import INTJAudio
import XCTest

class RampTests: XCTestCase
{
    func testRampDimensions()
    {
        let ramp = Ramp(start: 0.75, end: 0.25)
        
        XCTAssertEqual(ramp.delta, -0.5)
        XCTAssertEqual(ramp.increment(over: 100), -0.005)
    }
    
    func testCombineRamps()
    {
        let ramp1 = Ramp(start: 1.0, end: 2.0)
        let ramp2 = Ramp(start: 2.0, end: 0.0)
        let combined = ramp1.combine(with: ramp2)
        
        XCTAssertEqual(combined.start, 2.0)
        XCTAssertEqual(combined.end, 0.0)
    }
}
