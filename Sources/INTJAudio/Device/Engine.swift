//
//  Engine.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/1/22.
//

import Accelerate
import CoreAudio
import Foundation
import OSLog

public protocol EngineDelegate: AnyObject
{
    /// Notifies the `EngineDelegate` that the `prepare` functions are about to be called.
    ///
    /// Commonly used to do additional internal preparation before constructing with `prepare` calls.

    func willPrepare() throws
    
    /// Prepares the `EngineDelegate` for the audio stream inputs
    ///
    /// - Parameter dimensions: The number of channels and frame count expected for each process call
    
    func prepareForAudioInput(with dimensions: AudioDimensions) throws
    
    /// Prepares the `EngineDelegate` for the audio stream outputs
    ///
    /// - Parameter dimensions: The number of channels and frame count expected for each render call
    
    func prepareForAudioRendering(with dimensions: AudioDimensions) throws

    /// Handle the input audio buffer
    ///
    /// - Parameters:
    ///   - buffer: The buffer of audio samples
    ///   - frame: The absolute sample time as reported by the hardware

    func handleAudioInput(_ buffer: [AudioBuffer], frame: SamplePosition)
    
    /// Replace the contents of the buffer with audio
    ///
    /// - Parameters:
    ///   - buffer: The buffer of audio samples
    ///   - frame: The absolute sample time as reported by the hardware
    
    func renderAudio(into buffer: [AudioBuffer], frame: SamplePosition)
    
    /// Clean up after the audio stream has stopped

    func cleanup() throws
}

public final class Engine
{
    public var processingLoad: Double {
        vDSP.mean(loadBuffer)
    }
    
    private let device: DeviceProtocol
    private weak var delegate: EngineDelegate?
    private let interface: AudioObjectInterfaceProtocol
    
    private var handlerID: AudioDeviceIOProcID?
    private var inputAudio: [AudioBuffer]
    private let outputAudio: [AudioBuffer]
    
    public convenience init(device: DeviceProtocol,
                            delegate: EngineDelegate) throws
    {
        try self.init(device: device,
                      delegate: delegate,
                      interface: AudioObjectInterface())
    }
    
    init(device: DeviceProtocol,
         delegate: EngineDelegate,
         interface: AudioObjectInterfaceProtocol) throws
    {
        Logger.lifecycle.info("Engine.init")
        
        self.device = device
        self.delegate = delegate
        self.interface = interface
        
        inputAudio = [AudioBuffer]()
        outputAudio = [AudioBuffer](with: device.outputDimensions)
    }
            
    //
    // MARK: - Stream control
    //
    
    private var expectedDuration: Duration = .zero
    private var loadBuffer = [Double]()
    private var loadBufferIndex = 0
    
    public func start() throws
    {
        Logger.lifecycle.info("Engine.start")

        expectedDuration = Duration(.init(count: device.frameCount))
        loadBuffer = [Double](repeating: 0, count: Int(Hertz.samplingRate.rawValue) / device.frameCount.rawValue)
        loadBufferIndex = 0
        
        try delegate?.willPrepare()
        try delegate?.prepareForAudioInput(with: device.inputDimensions)
        try delegate?.prepareForAudioRendering(with: device.outputDimensions)
        
        let myself = Unmanaged.passUnretained(self).toOpaque()
        try interface.createIOProcID(id: device.id,
                                     handler: deviceCallback,
                                     clientData: myself,
                                     handlerID: &handlerID)

        try interface.start(id: device.id, handlerID: handlerID)
    }
    
    public func stop() throws
    {
        Logger.lifecycle.info("Engine.stop")

        guard let handlerID else { return }
        try interface.stop(id: device.id, handlerID: handlerID)
        try interface.destroyIOProcID(id: device.id, handlerID: handlerID)
        self.handlerID = nil
        
        try delegate?.cleanup()
        expectedDuration = .zero
        loadBuffer = []
        loadBufferIndex = 0
    }

    private let deviceCallback: AudioDeviceIOProc =
    { (_: AudioObjectID,
        timestamp: UnsafePointer<AudioTimeStamp>,
        inInputData: UnsafePointer<AudioBufferList>,
        _: UnsafePointer<AudioTimeStamp>,
        outOutputData: UnsafeMutablePointer<AudioBufferList>,
        _: UnsafePointer<AudioTimeStamp>,
        inClientData: UnsafeMutableRawPointer?) -> OSStatus in

        let myself = Unmanaged<Engine>.fromOpaque(inClientData!).takeUnretainedValue()
        let mutableInInputData = UnsafeMutablePointer<AudioBufferList>(mutating: inInputData)
        let mutableOutOutputData = UnsafeMutablePointer<AudioBufferList>(mutating: outOutputData)
        
        myself.process(inputBuffer: UnsafeMutableAudioBufferListPointer(mutableInInputData),
                       outputBuffer: UnsafeMutableAudioBufferListPointer(mutableOutOutputData),
                       timestamp: timestamp.pointee)
        
        return noErr
    }
    
    private func process(inputBuffer: UnsafeMutableAudioBufferListPointer,
                         outputBuffer: UnsafeMutableAudioBufferListPointer,
                         timestamp: AudioTimeStamp)
    {
        let duration = ContinuousClock().measure
        {
            inputAudio = [AudioBuffer](inputBuffer)
            outputAudio.forEach { $0.clear() }

            let position = SamplePosition(timestamp.mSampleTime)
            delegate?.handleAudioInput(inputAudio, frame: position)
            delegate?.renderAudio(into: outputAudio, frame: position)
            
            outputBuffer.copy(from: outputAudio)
        }
        
        loadBuffer[loadBufferIndex] = duration / expectedDuration
        loadBufferIndex = (loadBufferIndex + 1) % loadBuffer.count
    }
}
