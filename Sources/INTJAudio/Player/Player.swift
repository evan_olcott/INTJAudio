//
//  Player.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/18/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import AVFoundation
import Foundation
import OSLog

// TODO: Convert this to an Outputtable Processor

/// An object that plays `Arrangement`s through an audio output

public final class Player
{
    private let arrangement: ArrangementProtocol
    
    /// The current playback position in samples
    
    public var position: SamplePosition = .zero
    
    /// The current playback position in seconds
    
    public var location: TimeLocation
    {
        get { TimeLocation(position: position) }
        set { position = SamplePosition(location: newValue) }
    }
    
    /// Handler that will be called immediately before audio rendering begins
    
    public var preRenderHandler: (() -> Void)?
    
    /// Handler that will be called immediately after audio rendering has finished.
    ///
    /// The current playback position will have been advanced when this is called.
    
    public var postRenderHandler: (() -> Void)?
    
    /// Handler that will be called when the `Player` stops as a result of a `Landmark`
    ///
    /// Will be called on a background thread.
    
    public var didStopHandler: (() -> Void)?
    
    private var isStopping = false
    
    /// Create a new `Player`
    ///
    /// - Parameters:
    ///   - arrangement: The `Arrangement` that defines the playback layout
    
    public init(with arrangement: ArrangementProtocol)
    {
        Logger.lifecycle.debug("Player.init")
        
        self.arrangement = arrangement
    }
    
    public func prepare(for dimensions: AudioDimensions) throws
    {
        Logger.lifecycle.debug("Player.prepare")

        try arrangement.prepare(for: dimensions)
    }
    
    /// Requests the receiver to fill the buffer with audio samples for the current value of `position`
    ///
    /// - Parameters:
    ///   - output: The `AudioBuffer` objects to fill
    
    public func render(into output: [AudioBuffer])
    {
        if isStopping { return }
        
        preRenderHandler?()
        
        let renderRange = position ..< position.advanced(by: output.frameCount)
        
        for landmark in arrangement.landmarks where renderRange.contains(landmark.position) && landmark.isEnabled
        {
            switch landmark.action
            {
            case .stop:
                let partialCount = position.distance(to: landmark.position)
                let partial = (0 ..< output.count).map { _ in AudioBuffer(count: partialCount) }
                arrangement.render(into: partial, for: position)
                
                for (outputChannel, partialChannel) in zip(output, partial) {
                    outputChannel.copy(from: partialChannel)
                }
                
                position = landmark.position
                
                isStopping = true
                didStopHandler?()
                
                return
            case .jump(let destinationLandmark):
                let preCount = position.distance(to: landmark.position)
                let preBuffer = (0 ..< output.count).map { _ in AudioBuffer(count: preCount) }
                arrangement.render(into: preBuffer, for: position)
                
                for (outputChannel, partialChannel) in zip(output, preBuffer) {
                    outputChannel.copy(from: partialChannel)
                }
                
                position = destinationLandmark.position
                
                let postCount = output.frameCount.decreased(by: preCount)
                let postBuffer = (0 ..< output.count).map { _ in AudioBuffer(count: postCount) }
                arrangement.render(into: postBuffer, for: position)

                let bufferPosition = SamplePosition.zero.advanced(by: preCount)
                for (outputChannel, partialChannel) in zip(output, postBuffer) {
                    outputChannel.copy(from: partialChannel, to: bufferPosition)
                }
                
                position.advance(by: postCount)
                return
            case .none:
                break
            }
        }
        
        arrangement.render(into: output, for: position)
        position.advance(by: output.frameCount)
        
        postRenderHandler?()
    }
}
