//
//  AudioObjectInterface+Convenience.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import CoreAudio
import Foundation

extension AudioObjectInterfaceProtocol
{
    //
    // MARK: - Property access
    //
    
    func firstValue<T>(of property: AudioObjectPropertySelector,
                       in scope: AudioObjectPropertyScope = kAudioObjectPropertyScopeGlobal,
                       at element: AudioObjectPropertyElement = kAudioObjectPropertyElementMain,
                       on objectID: AudioObjectID,
                       as type: T.Type) throws -> T
    {
        let values = try value(of: property,
                               in: scope,
                               at: element,
                               on: objectID,
                               as: type)
        guard let value = values.first else { throw AudioObjectInterface.Error.unsupportedProperty }
        return value
    }
    
    func value<T>(of property: AudioObjectPropertySelector,
                  in scope: AudioObjectPropertyScope = kAudioObjectPropertyScopeGlobal,
                  at element: AudioObjectPropertyElement = kAudioObjectPropertyElementMain,
                  on objectID: AudioObjectID,
                  as type: T.Type) throws -> [T]
    {
        let data = try data(of: property, in: scope, at: element, on: objectID)
        
        return data.withUnsafeBytes
        { bytes -> [T] in
            
            [T](bytes.bindMemory(to: type))
        }
    }
    
    func data(of property: AudioObjectPropertySelector,
              in scope: AudioObjectPropertyScope = kAudioObjectPropertyScopeGlobal,
              at element: AudioObjectPropertyElement = kAudioObjectPropertyElementMain,
              on objID: AudioObjectID) throws -> Data
    {
        var addr = AudioObjectPropertyAddress(mSelector: property,
                                              mScope: scope,
                                              mElement: element)
        
        guard hasProperty(with: objID, address: &addr) else { throw AudioObjectInterface.Error.unsupportedProperty }
        
        var size = UInt32(0)
        
        try propertyDataSize(of: objID,
                             address: &addr,
                             outDataSize: &size)
        
        var data = Data(count: Int(size))
        
        try data.withUnsafeMutableBytes
        { bytes in
            
            try propertyData(of: objID,
                             address: &addr,
                             dataSize: &size,
                             outData: bytes.baseAddress!)
        }
        
        return data
    }
    
    func setValue<T>(of property: AudioObjectPropertySelector,
                     in scope: AudioObjectPropertyScope = kAudioObjectPropertyScopeGlobal,
                     at element: AudioObjectPropertyElement = kAudioObjectPropertyElementMain,
                     to value: T,
                     on objID: AudioObjectID) throws
    {
        var addr = AudioObjectPropertyAddress(mSelector: property,
                                              mScope: scope,
                                              mElement: element)
        
        let size = UInt32(MemoryLayout<T>.size)
        var v = value
        
        try withUnsafePointer(to: &v) { vp in
            try setPropertyData(of: objID,
                                address: &addr,
                                dataSize: size,
                                data: vp)
        }
    }
    
    //
    // MARK: - Channels
    //
    
    internal var deviceIDMap: [String: AudioObjectID]
    {
        get throws
        {
            // get list of all existing AudioObjectIDs
            
            var address = AudioObjectPropertyAddress(mSelector: kAudioHardwarePropertyDevices,
                                                     mScope: kAudioObjectPropertyScopeGlobal,
                                                     mElement: kAudioObjectPropertyElementMain)
            
            var size = UInt32(0)
            
            try propertyDataSize(of: AudioObjectID(kAudioObjectSystemObject),
                                 address: &address,
                                 outDataSize: &size)
            
            var data = Data(count: Int(size))
            
            try data.withUnsafeMutableBytes
            { bytes in
                
                let deviceID = bytes.bindMemory(to: AudioDeviceID.self).baseAddress!
                try propertyData(of: AudioObjectID(kAudioObjectSystemObject),
                                 address: &address,
                                 dataSize: &size,
                                 outData: deviceID)
            }
            
            let numberOfDevices = Int(size) / MemoryLayout<AudioDeviceID>.size
            var deviceIDs = [AudioDeviceID](repeating: AudioDeviceID(0), count: numberOfDevices)
            
            deviceIDs.withUnsafeMutableBufferPointer
            { deviceIDBytes in
                
                _ = data.copyBytes(to: deviceIDBytes)
            }
            
            // for each deviceID, pair it with it's UID into a dictionary
            
            return try deviceIDs.reduce(into: [String: AudioObjectID]()) { result, deviceID in
                let uid = try firstValue(of: kAudioDevicePropertyDeviceUID, on: deviceID, as: CFString.self) as String
                result[uid] = deviceID
            }
        }
    }
    
    internal func channelTitles(id: AudioObjectID,
                                scope: AudioObjectPropertyScope) throws -> [String]
    {
        if isAggregateDevice(id: id)
        {
            var address = AudioObjectPropertyAddress(mSelector: kAudioAggregateDevicePropertyComposition,
                                                     mScope: kAudioObjectPropertyScopeGlobal,
                                                     mElement: kAudioObjectPropertyElementMain)
            var composition: CFDictionary?
            var size = UInt32(MemoryLayout<CFDictionary>.size)
            
            try withUnsafeMutablePointer(to: &composition) { ptr in
                try propertyData(of: id,
                                 address: &address,
                                 dataSize: &size,
                                 outData: ptr)
            }
            
            guard
                let structure = composition as? [String: Any],
                let subdevices = structure["subdevices"] as? [[String: Any]]
            else { throw AudioObjectInterface.Error.illegalOperation }
            
            let map = try deviceIDMap
            var titles = [String]()
            
            for subdevice in subdevices
            {
                guard
                    let identifier = subdevice["uid"] as? String,
                    let id = map[identifier],
                    let name = subdevice["name"] as? String
                else { continue }
                
                let deviceTitles = try channelTitles(id: id, scope: scope)
                if deviceTitles.count == 1 {
                    titles.append(name)
                } else {
                    titles.append(contentsOf: deviceTitles.map {
                        "\(name): \($0)"
                    })
                }
            }
            
            return titles
        }
        else
        {
            var data = try data(of: kAudioDevicePropertyStreamConfiguration,
                                in: scope,
                                on: id)
            var numberOfChannels = ChannelCount.none
            
            data.withUnsafeMutableBytes
            {
                let audioBufferList = $0.bindMemory(to: AudioBufferList.self)
                let bufferList = UnsafeMutableAudioBufferListPointer(audioBufferList.baseAddress!)
                
                numberOfChannels = bufferList.channelCount
            }
            
            return try numberOfChannels.map { index in
                
                var title = try firstValue(of: kAudioObjectPropertyElementName,
                                           in: scope,
                                           at: UInt32(index.rawValue + 1),
                                           on: id,
                                           as: CFString.self) as String
                
                if title.isEmpty
                {
                    if numberOfChannels == .mono && index == .first
                    {
                        if scope == kAudioObjectPropertyScopeInput
                        {
                            title = String(localized: "Input",
                                           comment: "Name: audio input")
                        }
                        else if scope == kAudioObjectPropertyScopeOutput
                        {
                            title = String(localized: "Output",
                                           comment: "Name: audio output")
                        }
                    }
                    else if numberOfChannels == .stereo
                    {
                        if index == .first
                        {
                            title = String(localized: "Left",
                                           comment: "Name: left audio channel")
                        }
                        else
                        {
                            title = String(localized: "Right",
                                           comment: "Name: right audio channel")
                        }
                    }
                    else
                    {
                        title = String(localized: "Channel \(index.rawValue + 1)",
                                       comment: "Name: audio channel")
                    }
                }
                
                return title
            }
        }
    }
    
    //
    // MARK: - Aggregate device
    //
    
    internal func isAggregateDevice(id: AudioObjectID) -> Bool
    {
        var address = AudioObjectPropertyAddress(mSelector: kAudioAggregateDevicePropertyComposition,
                                                 mScope: kAudioObjectPropertyScopeGlobal,
                                                 mElement: kAudioObjectPropertyElementMain)
        return hasProperty(with: id, address: &address)
    }
    
    public func aggregateDeviceDescription(title: String,
                                           identifier: String,
                                           subdeviceIdentifiers: [String]) throws -> [String: Any]
    {
        let library = try deviceIDMap
        
        // construct the basics of the description
        
        var description: [String: Any] =
        [
            kAudioAggregateDeviceUIDKey: identifier,
            kAudioAggregateDeviceNameKey: title,
            kAudioAggregateDeviceIsStackedKey: 0,
            kAudioAggregateDeviceIsPrivateKey: 1
        ]
        
        // loop through the requested subdeviceIdentifiers
        
        try description[kAudioAggregateDeviceSubDeviceListKey] = subdeviceIdentifiers.compactMap
        { subdeviceIdentifier -> [String: Any]? in
            
            // get the AudioObjectID for the identifier (if it exists)
            
            guard let id = library[subdeviceIdentifier] else { return nil }
            
            func numberOfChannels(in scope: AudioObjectPropertyScope) throws -> Int
            {
                var data = try data(of: kAudioDevicePropertyStreamConfiguration,
                                    in: scope,
                                    on: id)
                var numberOfChannels = ChannelCount.none
                
                data.withUnsafeMutableBytes
                {
                    let audioBufferList = $0.bindMemory(to: AudioBufferList.self)
                    let bufferList = UnsafeMutableAudioBufferListPointer(audioBufferList.baseAddress!)
                    
                    numberOfChannels = bufferList.channelCount
                }
                
                return Int(numberOfChannels)
            }
            
            var subdevice: [String: Any] = [
                kAudioSubDeviceUIDKey: subdeviceIdentifier,
                kAudioSubDeviceNameKey: try firstValue(of: kAudioObjectPropertyName, on: id, as: CFString.self) as String,
                kAudioSubDeviceInputChannelsKey: try numberOfChannels(in: kAudioObjectPropertyScopeInput),
                kAudioSubDeviceOutputChannelsKey: try numberOfChannels(in: kAudioObjectPropertyScopeOutput)
            ]
            
            if description[kAudioAggregateDeviceClockDeviceKey] == nil
            {
                description[kAudioAggregateDeviceClockDeviceKey] = subdeviceIdentifier
            }
            else
            {
                subdevice[kAudioSubDeviceDriftCompensationKey] = 1
                subdevice[kAudioSubDeviceDriftCompensationQualityKey] = kAudioSubDeviceDriftCompensationMediumQuality
            }
            
            return subdevice
        }
        
        return description
    }
    
}
