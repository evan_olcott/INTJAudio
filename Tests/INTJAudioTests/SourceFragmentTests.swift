//
//  SourceFragmentTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

@testable import INTJAudio
import XCTest

class SourceFragmentTests: XCTestCase
{
    var fragment: SourceFragment?
    var mockSource: MockSource?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        mockSource = MockSource()
        
        fragment = SourceFragment(trackRange: 2048 ..< 4096,
                                  source: try XCTUnwrap(mockSource),
                                  sourceStart: 1024)
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        fragment = nil
        mockSource = nil
    }
    
    func testPrepare() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        
        fragment.prepare(for: AudioDimensions(frameCount: 512, channelCount: 1))
        
        XCTAssertEqual(source.calls.count, 1)
        XCTAssertEqual(source.calls[0].function, "prepare(for:)")
    }
    
    func testRenderBefore() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        let buffer = AudioBuffer(count: 512)
        
        fragment.render(into: [buffer], for: 0)
        
        XCTAssertEqual(source.calls.count, 0)
    }
    
    func testRenderAcrossStart() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        let buffer = AudioBuffer(count: 512)
        
        fragment.render(into: [buffer], for: 1792)
        
        XCTAssertEqual(source.calls.count, 1)
        XCTAssertEqual(source.calls[0].function, "read(into:from:to:)")
        XCTAssertEqual((source.calls[0].arguments["from"] as? SampleRange), 1024 ..< 1280)
        XCTAssertEqual((source.calls[0].arguments["to"] as? SamplePosition), 256)
    }
    
    func testRenderWithin() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        let buffer = AudioBuffer(count: 512)
        
        fragment.render(into: [buffer], for: 3072)
        
        XCTAssertEqual(source.calls.count, 1)
        XCTAssertEqual(source.calls[0].function, "read(into:from:to:)")
        XCTAssertEqual((source.calls[0].arguments["from"] as? SampleRange), 2048 ..< 2560)
        XCTAssertEqual((source.calls[0].arguments["to"] as? SamplePosition), 0)
    }
    
    func testRenderAcrossEnd() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        let buffer = AudioBuffer(count: 512)
        
        fragment.render(into: [buffer], for: 3840)
        
        XCTAssertEqual(source.calls.count, 1)
        XCTAssertEqual(source.calls[0].function, "read(into:from:to:)")
        XCTAssertEqual((source.calls[0].arguments["from"] as? SampleRange), 2816 ..< 3072)
        XCTAssertEqual((source.calls[0].arguments["to"] as? SamplePosition), 0)
    }
    
    func testRenderAfter() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        let buffer = AudioBuffer(count: 512)
        
        fragment.render(into: [buffer], for: 4096)
        
        XCTAssertEqual(source.calls.count, 0)
    }
    
    func testRenderEncompass() async throws
    {
        let fragment = try XCTUnwrap(fragment)
        let source = try XCTUnwrap(mockSource)
        let buffer = AudioBuffer(count: 3072)
        
        fragment.render(into: [buffer], for: 1536)
        
        XCTAssertEqual(source.calls.count, 1)
        XCTAssertEqual(source.calls[0].function, "read(into:from:to:)")
        XCTAssertEqual((source.calls[0].arguments["from"] as? SampleRange), 1024 ..< 3072)
        XCTAssertEqual((source.calls[0].arguments["to"] as? SamplePosition), 512)
    }
}
