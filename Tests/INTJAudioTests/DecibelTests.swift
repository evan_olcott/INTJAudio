//
//  DecibelTests.swift
//  INTJAudioTests
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

@testable import INTJAudio
import XCTest

class DecibelTests: XCTestCase
{
    func testInit()
    {
        XCTAssertEqual(Decibel(rawValue: 4).rawValue, 4.0)
    }
    
    func testInitAsInt()
    {
        XCTAssertEqual(Decibel(Int(4)).rawValue, 4.0)
    }
    
    func testInitAsFloat()
    {
        XCTAssertEqual(Decibel(Float(4)).rawValue, 4.0)
    }
    
    func testInitAsDouble()
    {
        XCTAssertEqual(Decibel(4.0).rawValue, 4.0)
    }
    
    func testInitWithScalar()
    {
        XCTAssertEqual(Decibel(Scalar(0.5)).rawValue, -6.02, accuracy: 0.01)
    }
    
    func testInitWithString()
    {
        XCTAssertEqual(Decibel("-6.2")?.rawValue, -6.2)
    }
    
    func testAsString()
    {
        XCTAssertEqual(String(describing: Decibel(-4.215)), "-4.21")
        XCTAssertEqual(String(describing: Decibel(-4.255)), "-4.25")
        XCTAssertEqual(String(describing: Decibel(4.84)), "+4.84")
    }
    
    func testIncreased()
    {
        XCTAssertEqual(Decibel(-9).increased(by: 3), Decibel(-6))
    }
    
    func testDifference()
    {
        XCTAssertEqual(Decibel(-9).difference(to: -12), Decibel(-3))
    }
    
    func testEquality()
    {
        let db1 = Decibel(3)
        let db2 = Decibel(4)
        
        XCTAssertTrue(db1 == db1)
        XCTAssertFalse(db1 == db2)
    }

    func testComparable()
    {
        let db1 = Decibel(3)
        let db2 = Decibel(4)
        
        XCTAssertTrue(db1 < db2)
        XCTAssertFalse(db2 < db1)
        XCTAssertTrue(db2 > db1)
    }
}
