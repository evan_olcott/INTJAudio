//
//  Accumulator.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation
import OSLog

/// A buffer that collects samples and calls a closure when a threshold is reached.

public class Accumulator: AudioBuffer
{
    private var handler: ((Accumulator) -> (Bool))?
    private var interval: SampleCount
    private var outputCursor: SamplePosition = .zero

    required init(rawValue value: UnsafeMutableBufferPointer<Sample>)
    {
        Logger.stream.debug("Accumulator.init(rawValue:)")
        
        interval = SampleCount(value.count)
        super.init(rawValue: value)
    }
    
    /// Creates a new buffer.
    ///
    /// The handler will be passed the `Accumulator` instance as an argument, and expects a Boolean in return.
    /// If the return value is true and the interval is the same as the buffer count, the buffer will be cleared.
    ///
    /// - Parameters:
    ///   - count: The number of samples.
    ///   - interval: When this number of samples has been added, the handler will be called. Default is the size of the buffer.
    ///   - handler: The handler to call when the threshold has been met.
    
    public init(count: SampleCount,
                interval: SampleCount? = nil,
                handler: ((Accumulator) -> Bool)? = nil)
    {
        Logger.stream.debug("Accumulator.init(count:interval:handler)")

        self.interval = interval ?? count
        self.handler = handler

        assert(self.interval <= count, "Interval cannot be larger than the buffer")
        
        super.init(count: count)
        
        outputCursor = end.receded(by: self.interval)
    }
    
    /// Append samples to the buffer
    ///
    /// The `handler` call in this function is given precedence over the global `handler`.
    /// The handler may be called multiple times or not called at all when this function is called.
    ///
    /// - Parameters:
    ///   - input: The entire contents of this buffer will be added.
    ///   - handler: The local handler to call when the threshold has been met.

    public func append(_ input: AudioBuffer,
                       handler: ((Accumulator) -> Bool)? = nil)
    {
        Logger.stream.debug("Accumulator.append")

        let inputRange = ..<input.sampleCount
        var inputCursor: SamplePosition = 0

        while inputRange.contains(inputCursor)
        {
            let framesToRead = min(inputCursor.distance(to: inputRange.end),
                                   outputCursor.distance(to: end))

            copy(from: input,
                 range: inputCursor ..< inputCursor.advanced(by: framesToRead),
                 to: outputCursor)

            outputCursor.advance(by: framesToRead)
            inputCursor.advance(by: framesToRead)

            if outputCursor == end
            {
                let handlerToCall = handler ?? self.handler
                let shouldClearBuffer = handlerToCall?(self)

                let cursor = end.receded(by: interval)
                if cursor > 0
                {
                    let intervalPosition = SamplePosition.zero.advanced(by: interval)
                    move(intervalPosition ..< end, to: .zero)
                }
                else if shouldClearBuffer ?? true
                {
                    clear()
                }

                outputCursor = cursor
            }
        }
    }
    
    /// The number of samples appended but not sent to the handler.
    
    public var accumulatedCount: SampleCount
    {
        return SamplePosition.zero.distance(to: outputCursor)
    }
}
