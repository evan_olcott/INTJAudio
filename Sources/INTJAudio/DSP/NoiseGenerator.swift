//
//  NoiseGenerator.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/21/24.
//

import Accelerate
import Foundation

public class NoiseGenerator
{
    private var generator: BNNSRandomGenerator?
    
    public init()
    {
        generator = BNNSCreateRandomGeneratorWithSeed(BNNSRandomGeneratorMethodAES_CTR,
                                                      mach_absolute_time(),
                                                      nil)
    }
    
    deinit
    {
        BNNSDestroyRandomGenerator(generator)
        generator = nil
    }
    
    public func render(into buffer: AudioBuffer)
    {
        var descriptor = BNNSNDArrayDescriptor(flags: BNNSNDArrayFlags(0),
                                               layout: BNNSDataLayoutVector,
                                               size: (Int(buffer.sampleCount), 0, 0, 0, 0, 0, 0, 0),
                                               stride: (0, 0, 0, 0, 0, 0, 0, 0),
                                               data: buffer.pointer,
                                               data_type: BNNSDataType.float,
                                               table_data: nil,
                                               table_data_type: BNNSDataType.float,
                                               data_scale: 1, data_bias: 0)

        BNNSRandomFillUniformFloat(generator, &descriptor, -1, 1)
    }
    
}
