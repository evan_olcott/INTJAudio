//
//  ValueInputBus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 8/25/24.
//

import Foundation

open class ValueInputBus: Bus, Equatable
{
    public let identifier: String
    public var connectedBus: Bus?
    public weak var processor: StreamProcessor?
    public var taps = [Bus]()
    
    public init(for processor: ValueInputtable, identifier: String)
    {
        self.processor = processor
        self.identifier = identifier
    }
    
    public static func == (lhs: ValueInputBus, rhs: ValueInputBus) -> Bool
    {
        lhs.identifier == rhs.identifier
    }

    open func value(state: StreamState) -> Double?
    {
        guard
            let outputBus = connectedBus as? ValueOutputBus
        else { return nil }

        let v = outputBus.value(state: state)
        
        if let taps = taps as? [ValueInputBus]
        {
            taps.forEach { $0.setValue(v, state: state) }
        }
        
        return v
    }

    open func setValue(_ value: Double?, state: StreamState)
    {
        if let taps = taps as? [ValueInputBus]
        {
            taps.forEach { $0.setValue(value, state: state) }
        }
        
        if let processor = processor as? ValueInputtable
        {
            processor.setValue(value, from: self, state: state)
        }
    }
}
