//
//  StreamProcessor.swift
//  INTJAudio
//
//  Created by Evan Olcott on 6/26/24.
//

import Foundation

public protocol StreamProcessor: AnyObject
{
    var supportsNonRealtime: Bool { get }
        
    func cleanup()
}

public extension StreamProcessor
{
    func cleanup() { }
}
