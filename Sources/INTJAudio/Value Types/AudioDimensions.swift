//
//  AudioDimensions.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/2/22.
//

import Foundation

/// Describes a collection of audio buffers in samples and channels

public struct AudioDimensions
{
    public let frameCount: SampleCount
    public let channelCount: ChannelCount
    
    public init(frameCount: SampleCount, channelCount: ChannelCount)
    {
        self.frameCount = frameCount
        self.channelCount = channelCount
    }
    
    public static var zero: AudioDimensions
    {
        .init(frameCount: .zero, channelCount: .none)
    }
}
