//
//  Semitone.swift
//  Atlas
//
//  Created by Evan Olcott on 1/14/21.
//

import Foundation

public struct Semitone: RawRepresentable, Sendable
{
    public var rawValue: Double
    
    public init(rawValue value: Double)
    {
        rawValue = value
    }
    
    public init(_ value: Double)
    {
        rawValue = value
    }
    
    public init(_ frequency: Hertz)
    {
        rawValue = (12 * log2(frequency.rawValue / 440.0)) + 69
    }
    
    public mutating func quantize()
    {
        rawValue.round()
    }
    
    public func quantized() -> Semitone
    {
        var new = self
        new.quantize()
        return new
    }
    
    public var offset: Cent
    {
        Cent((rawValue - rawValue.rounded()) * 100)
    }
    
    public func distance(to other: Semitone) -> Cent
    {
        Cent((other.rawValue - rawValue) * 100)
    }
    
    public func advanced(by cents: Cent) -> Semitone
    {
        Semitone(rawValue + (Double(cents.rawValue) / 100))
    }
    
    public func receded(by cents: Cent) -> Semitone
    {
        Semitone(rawValue - (Double(cents.rawValue) / 100))
    }
}

extension Semitone: Equatable
{
    public static func == (lhs: Semitone, rhs: Semitone) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension Semitone: Comparable
{
    public static func < (lhs: Semitone, rhs: Semitone) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

extension Semitone: ExpressibleByFloatLiteral
{
    public typealias FloatLiteralType = Double
    
    public init(floatLiteral value: Double)
    {
        rawValue = value
    }
}
