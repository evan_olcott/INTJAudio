//
//  FaderEffect.swift
//  INTJAudio
//
//  Created by Evan Olcott on 9/25/22.
//  Copyright © 2022 INTJ Software. All rights reserved.
//

import Foundation

public final class FaderEffect: Effect
{
    public var isEnabled = true
    
    var scalarAutomation: AutomationProtocol = Automation(for: .scalar)
    
    public init() { }
    
    public var displayName = "Fader"

    /// Sets the volume at a location
    ///
    /// - Parameters:
    ///   - volume: The amount of gain applied to the audio
    ///   - location: The location that the gain application begins
    
    public func setVolume(_ volume: Decibel, at location: TimeLocation = .zero)
    {
        let value = Double(Scalar(volume))
        scalarAutomation.setValue(value, at: location)
    }
    
    /// Adds a fade-in automation
    ///
    /// - Parameter range: The range over which the fade-in will be applied
    
    public func addFadeIn(with range: TimeRange)
    {
        setVolume(.negativeInfinity, at: range.start)
        setVolume(.unity, at: range.end)
    }
    
    /// Adds a fade-out automation
    ///
    /// - Parameter range: The range over which the fade-out will be applied
    
    public func addFadeOut(with range: TimeRange)
    {
        setVolume(.unity, at: range.start)
        setVolume(.negativeInfinity, at: range.end)
    }

    //
    // MARK: -
    //
    
    public func prepare(for dimensions: AudioDimensions)
    {
        // nop
    }
    
    public func process(_ buffers: [AudioBuffer], for position: SamplePosition)
    {
        guard isEnabled else { return }
        
        let range = position ..< position.advanced(by: buffers.frameCount)
        let values = scalarAutomation.values(for: range)
        
        guard values.count >= 2 else { return }
        
        for i in 0 ..< values.count - 1
        {
            let ramp = Ramp(start: Scalar(values[i].1),
                            end: Scalar(values[i + 1].1))
            buffers.forEach { $0.scale(with: ramp, range: values[i].0 ..< values[i + 1].0) }
        }
    }
}

extension AutomationKey
{
    static let scalar: AutomationKey = "scalar"
}
