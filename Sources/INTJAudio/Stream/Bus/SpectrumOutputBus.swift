//
//  SpectrumOutputBus.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/31/24.
//

import Foundation

public final class SpectrumOutputBus: Bus, Equatable
{
    public let identifier: String
    public var connectedBus: Bus?
    public weak var processor: StreamProcessor?
    public var taps = [Bus]()

    public init(for processor: SpectrumOutputtable, identifier: String)
    {
        self.processor = processor
        self.identifier = identifier
    }
    
    public static func == (lhs: SpectrumOutputBus, rhs: SpectrumOutputBus) -> Bool
    {
        lhs.identifier == rhs.identifier
    }

    public func handleSpectrumInput(_ buffer: [ComplexBuffer], state: StreamState)
    {
        if let taps = taps as? [SpectrumInputBus]
        {
            taps.forEach { $0.handleSpectrumInput(buffer, state: state) }
        }
        
        if let inputBus = connectedBus as? SpectrumInputBus
        {
            inputBus.handleSpectrumInput(buffer, state: state)
        }
    }
    
    public func renderSpectrum(into buffer: [ComplexBuffer], state: StreamState) -> Bool
    {
        var didCalculate = false
        
        if let processor = processor as? SpectrumOutputtable
        {
            didCalculate = processor.renderSpectrum(into: buffer, for: self, state: state)
        }

        if didCalculate, let taps = taps as? [SpectrumInputBus]
        {
            taps.forEach { $0.handleSpectrumInput(buffer, state: state) }
        }
        
        return didCalculate
    }
}
