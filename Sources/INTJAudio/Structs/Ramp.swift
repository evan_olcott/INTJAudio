//
//  Ramp.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation

/// An object that describes a change in a value

public struct Ramp: Codable
{
    public enum Shape: Codable
    {
        case linear
        case power
    }
    
    public let start: Scalar
    public let end: Scalar
    public let shape: Shape
    
    public init(shape: Shape = .linear)
    {
        start = .unity
        end = .unity
        self.shape = shape
    }
    
    public init(start: Scalar, end: Scalar, shape: Shape = .linear)
    {
        self.start = start
        self.end = end
        self.shape = shape
    }
    
    public init(scalar: Scalar, shape: Shape = .linear)
    {
        self.start = scalar
        self.end = scalar
        self.shape = shape
    }
    
    public static var unity: Ramp { Ramp(scalar: .unity) }
    public static var fadeIn: Ramp { Ramp(start: .silence, end: .unity) }
    public static var fadeOut: Ramp { Ramp(start: .unity, end: .silence) }
    
    /// The difference between the start and end scalars
    
    public var delta: Scalar
    {
        Scalar(end.rawValue - start.rawValue)
    }
    
    /// The amount to apply to a scalar for every sample in order to reach the end value from the start value
    ///
    /// - Parameter count: The number of samples to ramp
    /// - Returns: The amount increase the sample scalar
    
    public func increment(over count: SampleCount) -> Scalar
    {
        Scalar((end.rawValue - start.rawValue) / Float(count))
    }
    
    /// Combine two ramps.
    ///
    /// - Parameter other: A `Ramp` to be combined with the receiver
    /// - Returns:A `Ramp` that is the result of combining the other `Ramp` with the receiver
    
    public func combine(with other: Ramp) -> Ramp
    {
        Ramp(start: Scalar(start.rawValue * other.start.rawValue),
             end: Scalar(end.rawValue * other.end.rawValue),
             shape: shape)
    }
}
