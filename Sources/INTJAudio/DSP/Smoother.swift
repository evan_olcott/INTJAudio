//
//  Smoother.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/27/24.
//

import Accelerate
import Foundation

/// A class that smooths changes in values over time, using industry-standard rise/fall times.

public class Smoother
{
    private let method: Smoother.Method
    private let count: Int

    private var newValues: [Float]
    private var oldValues: [Float]
    private var currentTime: TimeInterval = 0
    private var peakTimes: [TimeInterval]
    
    /// Creates an instance of `Smoother`
    ///
    /// - Parameters:
    ///   - method: The behavior of the smoothing response.
    ///   - count: The expected number of distinct `Float` values that will be continuously added via the `add` functions..

    public init(method: Smoother.Method,
                count: Int)
    {
        self.method = method
        self.count = count

        self.newValues = [Float](repeating: 0, count: count)
        self.oldValues = [Float](repeating: 0, count: count)
        self.peakTimes = [TimeInterval](repeating: 0, count: count)
    }
    
    /// Resets the internal memory

    public func clear()
    {
        newValues = [Float](repeating: 0, count: count)
        oldValues = [Float](repeating: 0, count: count)
        peakTimes = [TimeInterval](repeating: 0, count: count)

        currentTime = 0
    }
    
    /// Updates the values of `output` given a `TimeLength` since the previous call to `advance`
    /// 
    /// This is intended to be called regularly, usually from within the audio stream, whether calls to the `add` functions have been made or not.
    /// 
    /// - Parameter interval: The length of time since the previous call to `advance`.
    /// - Returns: The smoothed values
    
    public func advance(with interval: TimeLength) -> [Float]
    {
        currentTime += interval.seconds
        
        for i in 0 ..< count
        {
            oldValues[i] = valueFor(oldValue: oldValues[i],
                                    newValue: newValues[i],
                                    interval: interval.seconds,
                                    peakTime: &peakTimes[i])
        }
        
        return oldValues
    }
    
    /// Adds the singular new value
    ///
    /// - Parameter input: The value to add

    public func add(_ input: Float)
    {
        add([input])
    }
    
    /// Adds a set of new values
    ///
    /// The number of elements in the array must match the value of `count` set at initialization
    ///
    /// - Parameter input: The values to add

    public func add(_ input: [Float])
    {
        guard input.count == count else { fatalError() }
        self.newValues = input
    }

    private func valueFor(oldValue: Float,
                          newValue: Float,
                          interval: TimeInterval,
                          peakTime: inout TimeInterval) -> Float
    {
        var resultValue: Float

        if newValue >= oldValue
        {
            if method.riseTime == 0
            {
                resultValue = newValue
            }
            else
            {
                let E = interval * (1.0 / method.riseTime)
                let M = pow(method.riseAmount, E)
                resultValue = newValue + (Float(M) * (oldValue - newValue))
            }

            peakTime = currentTime
        }
        else
        {
            if currentTime > peakTime + method.holdTime
            {
                if method.fallTime == 0
                {
                    resultValue = newValue
                }
                else
                {
                    let E = interval * (1.0 / method.fallTime)
                    let M = pow(method.fallAmount, E)
                    resultValue = newValue + (Float(M) * (oldValue - newValue))
                }
            }
            else
            {
                resultValue = oldValue
            }
        }

        return resultValue
    }
}

extension Smoother
{
    public enum Method: String, Codable
    {
        case digital
        case PPMTypeI
        case PPMTypeII
        case VU
        case nordic
        case BBCEBU
        case peakHoldShort
        case peakHoldLong
        case peakHoldInfinite
        
        var riseAmount: Double
        {
            switch self
            {
            case .digital, .PPMTypeI, .PPMTypeII:
                1.0 / 3
                
            case .VU:
                0.01

            case .nordic, .BBCEBU:
                0.2
                
            case .peakHoldShort, .peakHoldLong, .peakHoldInfinite:
                0.001
            }
        }
        
        var riseTime: TimeInterval
        {
            switch self
            {
            case .digital, .PPMTypeI, .PPMTypeII:
                0.0017
                
            case .VU:
                0.3
                
            case .nordic, .BBCEBU:
                0.005

            default:
                0
            }
        }
        
        var holdTime: TimeInterval
        {
            switch self
            {
            case .PPMTypeII:
                0.1

            case .peakHoldShort:
                0.7
                
            case .peakHoldLong:
                2.0
                
            case .peakHoldInfinite:
                .infinity
                
            default:
                0
            }
        }
        
        var fallAmount: Double
        {
            switch self
            {
            case .digital, .PPMTypeI, .nordic:
                0.1
                
            case .PPMTypeII, .BBCEBU:
                pow(10, -24 / 20)
                
            case .VU:
                0.01
                
            case .peakHoldShort, .peakHoldLong, .peakHoldInfinite:
                0.001
            }
        }
        
        var fallTime: TimeInterval
        {
            switch self
            {
            case .digital:
                0.85
                
            case .PPMTypeI:
                1.7
                
            case .PPMTypeII:
                2.8
                
            case .VU:
                0.3
                
            case .nordic:
                1.5
                
            case .BBCEBU:
                2.85
                
            case .peakHoldShort, .peakHoldLong, .peakHoldInfinite:
                0.3
            }
        }
    }
}
