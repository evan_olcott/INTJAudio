//
//  MockTrack.swift
//  INTJAudioTests
//
//  Created by Evan Olcott on 11/3/22.
//

import Foundation
import INTJAudio

final class MockTrack: MockObject, TrackProtocol
{
    func prepare(for dimensions: AudioDimensions) throws
    {
        try addCallThrowing(to: #function, with: [ "for": dimensions ])
    }
    
    func render(into output: [AudioBuffer], for position: SamplePosition)
    {
        addCall(to: #function, with: [ "into": output, "for": position ])
    }
}
