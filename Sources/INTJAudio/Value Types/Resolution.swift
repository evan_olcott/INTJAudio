//
//  Resolution.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/29/24.
//

import Accelerate
import Foundation

public struct Resolution: RawRepresentable, Codable, Hashable, Sendable
{
    public var rawValue: Int
    
    public init(rawValue: Int)
    {
        self.rawValue = rawValue
    }
    
    public init()
    {
        self.rawValue = 10
    }
    
    public var sampleCount: SampleCount
    {
        SampleCount(rawValue: 1 << rawValue)
    }
}

extension Resolution: ExpressibleByIntegerLiteral
{
    public init(integerLiteral value: Int)
    {
        self.rawValue = value
    }
}

public extension vDSP_Length
{
    init(_ resolution: Resolution)
    {
        self.init(resolution.rawValue)
    }
}
