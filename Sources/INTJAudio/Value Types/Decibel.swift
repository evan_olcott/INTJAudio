//
//  Decibel.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Foundation

public struct Decibel: RawRepresentable, Codable, LosslessStringConvertible, Sendable
{
    public var rawValue: Double

    public init(rawValue value: Double)
    {
        rawValue = value
    }

    public init(_ value: Int)
    {
        rawValue = Double(value)
    }

    public init(_ value: Float)
    {
        rawValue = Double(value)
    }
    
    public init(_ value: Double)
    {
        rawValue = value
    }
    
    public init(_ value: Scalar)
    {
        rawValue = Double(20 * log10(value.rawValue))
    }
    
    public init?(_ value: String)
    {
        guard let doubleValue = Double(value) else { return nil }
        rawValue = doubleValue
    }

    public var description: String
    {
        String(format: "%+0.2f", rawValue)
    }
    
    public static var unity: Decibel { Decibel(0) }
    public static var negativeInfinity: Decibel { Decibel(-Double.infinity) }
    
    public mutating func increase(by other: Decibel)
    {
        rawValue += other.rawValue
    }
    
    public func increased(by other: Decibel) -> Decibel
    {
        var new = self
        new.increase(by: other)
        return new
    }
        
    public func difference(to other: Decibel) -> Decibel
    {
        Decibel(other.rawValue - rawValue)
    }
}

extension Decibel: Equatable
{
    public static func == (lhs: Decibel, rhs: Decibel) -> Bool
    {
        lhs.rawValue == rhs.rawValue
    }
}

extension Decibel: Comparable
{
    public static func < (lhs: Decibel, rhs: Decibel) -> Bool
    {
        lhs.rawValue < rhs.rawValue
    }
}

extension Decibel: ExpressibleByIntegerLiteral
{
    public typealias IntegerLiteralType = Int
    
    public init(integerLiteral value: Int)
    {
        self.rawValue = Double(value)
    }
}

extension Decibel: ExpressibleByFloatLiteral
{
    public typealias FloatLiteralType = Double
    
    public init(floatLiteral value: Double)
    {
        self.rawValue = value
    }
}

public extension Double
{
    init(_ value: Decibel)
    {
        self = value.rawValue
    }
}

public extension Float32
{
    init(_ value: Decibel)
    {
        self = Float32(value.rawValue)
    }
}
