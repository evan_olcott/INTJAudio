//
//  AudioBuffer.swift
//  INTJAudio
//
//  Created by Evan Olcott
//  Copyright © 2022 INTJ Software, LLC. All rights reserved.
//

import Accelerate
import AudioToolbox
import AVFoundation
import CoreAudio
import Foundation

public typealias Sample = Float

/// A collection of contiguous audio samples.

open class AudioBuffer: RawRepresentable
{
    public var rawValue: UnsafeMutableBufferPointer<Sample>
    private var owns = false
    
    /// Creates a new buffer referencing the contents of another buffer.
    ///
    /// - Parameter value: The buffer of audio samples.
    ///
    /// - Attention: Contents are not copied from the buffer, does not take ownership of the buffer, and will not deallocate it when this object is deallocated.
    
    public required init(rawValue value: UnsafeMutableBufferPointer<Sample>)
    {
        Logger.stream.debug("AudioBuffer.init(rawValue:)")

        rawValue = value
    }
    
    /// Creates a new, initialized buffer able to hold `count` samples.
    ///
    /// - Parameter count: The number of samples.
    
    public init(count: SampleCount)
    {
        Logger.stream.debug("AudioBuffer.init(count:)")

        rawValue = UnsafeMutableBufferPointer<Sample>.allocate(capacity: Int(count))
        rawValue.initialize(repeating: 0)
        owns = true
    }
    
    /// Creates a new, initialized buffer able to hold `length` seconds of samples.
    ///
    /// - Parameter length: The number of seconds.
    
    public convenience init(length: TimeLength)
    {
        Logger.stream.debug("AudioBuffer.init(length:)")

        self.init(count: SampleCount(length: length))
    }
    
    /// Creates a new buffer with the contents of a `Data` instance.
    ///
    /// - Parameter data: The `Data` object containing audio samples.
    ///
    /// - Attention: The contents are copied from the `Data` object.

    public convenience init(_ data: Data)
    {
        Logger.stream.debug("AudioBuffer.init(_: Data)")

        self.init(count: SampleCount(byteCount: data.count))

        data.withUnsafeBytes { copy(from: $0.bindMemory(to: Sample.self)) }
        owns = true
    }
    
    /// Creates a new buffer with the contents of another buffer.
    ///
    /// - Parameter other: The `AudioBuffer`containing the audio samples.
    ///
    /// - Attention: The contents are copied from the other `Samples` object.

    public convenience init(_ other: AudioBuffer)
    {
        Logger.stream.debug("AudioBuffer.init(_: AudioBuffer)")

        self.init(count: other.sampleCount)

        copy(from: other, range: ..<other.end)
        owns = true
    }

    deinit
    {
        Logger.stream.debug("AudioBuffer.deinit")
        
        if owns { rawValue.deallocate() }
    }
    
    /// The number of samples.
    
    public var sampleCount: SampleCount
    {
        SampleCount(rawValue.count)
    }
    
    /// A Boolean value indicating whether the buffer is empty.
    
    public var isEmpty: Bool
    {
        rawValue.isEmpty
    }
    
    /// The position immediately after the last sample in the buffer.
    
    public var end: SamplePosition
    {
        SamplePosition(rawValue.count)
    }

    public subscript(index: SamplePosition) -> Sample
    {
        get
        {
            return rawValue[index.rawValue]
        }

        set
        {
            rawValue[index.rawValue] = newValue
        }
    }
    
    /// The value of the last sample in the buffer.
    
    public var last: Sample
    {
        rawValue.last ?? 0
    }
    
    /// A pointer to the beginning of the buffer.
    
    public var pointer: UnsafeMutablePointer<Sample>
    {
        rawValue.baseAddress!
    }
    
    /// A pointer to a sample in the buffer.
    ///
    /// - Parameter position: The position of the sample in the buffer.
    ///
    /// - Returns: A pointer to the sample at `position`.
    
    public func pointer(to position: SamplePosition) -> UnsafeMutablePointer<Sample>
    {
        assert(position >= .zero, "position out of bounds")
        assert(position < end, "position out of bounds")
        
        return rawValue.baseAddress!.advanced(by: Int(position))
    }

    //
    // MARK: - Process
    //
    
    /// Sets the values of all the samples in the buffer to 0 (silence).
    
    public func clear()
    {
        Logger.stream.debug("AudioBuffer.clear()")

        memset(rawValue.baseAddress!, 0, MemoryLayout<Sample>.size * rawValue.count)
    }
    
    /// Sets the values of a range of samples in the buffer to 0 (silence).
    ///
    /// - Parameter range: The range of samples in the buffer.
    
    public func clear(_ range: SampleRange)
    {
        Logger.stream.debug("AudioBuffer.clear(_:)")

        let safeRange = range.clamped(to: ..<end)
        memset(rawValue.baseAddress!.advanced(by: Int(safeRange.start)),
               0,
               MemoryLayout<Sample>.size * Int(safeRange.count))
    }
    
    /// Moves a range of samples in the buffer to another position in the buffer.
    ///
    /// - Parameters:
    ///   - sourceRange: The range of samples in the buffer to move.
    ///   - position: The position in the buffer where the first sample in the range will be moved to.
    ///
    /// - Note: Samples values outside of the range will be set to 0.
    
    public func move(_ sourceRange: SampleRange,
                     to position: SamplePosition)
    {
        Logger.stream.debug("AudioBuffer.move(_:to:)")

        let temp = AudioBuffer(self)
        
        clear()
        copy(from: temp, range: sourceRange, to: position)
    }
    
    /// Safely copies samples from another buffer
    ///
    /// - Parameters:
    ///   - other: The buffer to copy samples from
    ///   - sourceRange: The range of samples in the other buffer to copy to the receiver. This range can include samples outside of the buffer's range. The default is the range of all the samples in the other buffer.
    ///   - position: The position in the receiver where the range of copied samples will be placed. This value can be outside of the receiver's buffer range. The default is the beginning of the buffer.
    ///   - mix: Mix the copied samples with existing samples. The default will not mix, but replace the samples.
    ///   - ramp: A ramp to apply to the copied samples before replacing or mixing.
    ///
    /// - Note: Samples that are referenced but are outside ranges of either buffer are ignored.
    
    public func copy(from other: AudioBuffer,
                     range sourceRange: SampleRange? = nil,
                     to position: SamplePosition = .zero,
                     mix: Bool = false,
                     scaleWith ramp: Ramp = .unity)
    {
        assert(other !== self, "Do not use 'copy' within the same Sample content. Use 'move' instead.")

        copy(from: UnsafeBufferPointer<Sample>(other.rawValue),
             range: sourceRange,
             to: position,
             mix: mix,
             scaleWith: ramp)
    }

    /// Safely copies samples from an `UnsafeBufferPointer<Sample>`
    ///
    /// - Parameters:
    ///   - other: The buffer to copy samples from
    ///   - sourceRange: The range of samples in the buffer to copy to the receiver. This range can include samples outside of the buffer's range. The default is the range of all the samples in the buffer.
    ///   - position: The position in the receiver where the range of copied samples will be placed. This value can be outside of the receiver's buffer range. The default is the beginning of the buffer.
    ///   - mix: Mix the copied samples with existing samples. The default will not mix, but replace the samples.
    ///   - ramp: A ramp to apply to the copied samples before replacing or mixing.
    ///
    /// - Note: Samples that are referenced but are outside ranges of the receiver or the buffer are ignored.

    public func copy(from other: UnsafeBufferPointer<Sample>,
                     range sourceRange: SampleRange? = nil,
                     to position: SamplePosition = .zero,
                     mix: Bool = false,
                     scaleWith ramp: Ramp = .unity)
    {
        Logger.stream.debug("AudioBuffer.copy(_:range:to:mix:scaleWith:)")

        let otherEnd = SamplePosition(other.count)
        let sRange = sourceRange ?? ..<otherEnd

        var sStart = sRange.start
        var sEnd = sRange.end

        let dStart = position
        let dEnd = position.advanced(by: sRange.count)
        let dSafeRange = (dStart ..< dEnd).clamped(to: ..<end)

        // clip to limit allowed to write

        sEnd = min(sEnd, sStart.advanced(by: dSafeRange.count))

        // shift positions if position < 0

        if position < .zero
        {
            let shift = dStart.distance(to: dSafeRange.start)
            sStart.advance(by: shift)
            sEnd.advance(by: shift)
        }

        // clamp to reality

        sStart.clamp(to: ..<otherEnd)
        sEnd.clamp(to: ..<otherEnd)

        guard sEnd > sStart else { return }
        
        let readRange = sStart ..< sEnd
        let offset = sRange.start.distance(to: position)
        let writeRange = readRange.start.advanced(by: offset) ..< readRange.end.advanced(by: offset)

        // copy/mix/scale

        if mix == false
        {
            vDSP.clear(&rawValue[writeRange.rawValue])
        }

        var startValue = ramp.start.rawValue
        var increment = Sample(ramp.increment(over: writeRange.count))
                
        vDSP_vrampmuladd(other.baseAddress!.advanced(by: Int(readRange.start)), 1,
                         &startValue,
                         &increment,
                         pointer(to: writeRange.start), 1,
                         vDSP_Length(writeRange.count))
    }

    /// Safely copies samples from a raw audio file
    ///
    /// - Parameters:
    ///   - fileHandle: The handle reference to a file containing raw audio samples
    ///   - sourceRange: The range of samples in the file to copy to the receiver. This range can include samples outside of the file's range. The default is the range of all the samples in the file.
    ///   - position: The position in the receiver where the range of copied samples will be placed. This value can be outside of the receiver's buffer range. The default is the beginning of the buffer.
    ///   - mix: Mix the copied samples with existing samples. The default will not mix, but replace the samples.
    ///   - ramp: A ramp to apply to the copied samples before replacing or mixing.
    ///
    /// - Note: Samples that are referenced but are outside ranges of the receiver or the file are ignored.

    public func read(from fileHandle: FileHandle,
                     range sourceRange: SampleRange? = nil,
                     to position: SamplePosition = .zero,
                     mix: Bool = false,
                     scaleWith ramp: Ramp = .unity)
    {
        Logger.stream.debug("AudioBuffer.read(from:range:to:mix:scaleWith:)")

        let fileRange = SampleRange(start: .zero, count: fileHandle.sampleCount)
        let sRange = sourceRange ?? fileRange

        var sStart = sRange.start
        var sEnd = sRange.end

        let dStart = position
        let dEnd = position.advanced(by: sRange.count)
        let dSafeRange = (dStart ..< dEnd).clamped(to: ..<end)

        // clip to limit allowed to write

        sEnd = min(sEnd, sStart.advanced(by: dSafeRange.count))

        // shift positions if position < 0

        if position < .zero
        {
            let shift = dStart.distance(to: dSafeRange.start)
            sStart.advance(by: shift)
            sEnd.advance(by: shift)
        }

        // clip to reality

        sStart.clamp(to: ..<fileRange.end)
        sEnd.clamp(to: ..<fileRange.end)

        let readRange = sStart ..< sEnd
        let offset = sRange.start.distance(to: position)
        let writeRange = readRange.start.advanced(by: offset)
        ..< readRange.end.advanced(by: offset)

        // copy

        autoreleasepool
        {
            fileHandle.position = readRange.start
            let data = fileHandle.readData(ofLength: writeRange.count.byteCount)

            if data.isEmpty == false
            {
                data.withUnsafeBytes
                { bytes in

                    let sourceSamples = bytes.bindMemory(to: Sample.self)

                    // copy/mix/scale

                    if mix == false
                    {
                        vDSP.clear(&rawValue[writeRange.rawValue])
                    }

                    var startValue = ramp.start.rawValue
                    var increment = Sample(ramp.increment(over: writeRange.count))
                    
                    vDSP_vrampmuladd(sourceSamples.baseAddress!, 1,
                                     &startValue,
                                     &increment,
                                     pointer(to: writeRange.start), 1,
                                     vDSP_Length(writeRange.count))
                }
            }
        }
    }
    
    /// Change the level of samples in the buffer.
    ///
    /// - Parameters:
    ///   - decibels: The amount of change in decibels.
    ///   - range: The range of samples affected by the change. The default is the range of all the samples in the buffer.
    
    public func scale(by decibels: Decibel, range: SampleRange? = nil)
    {
        scale(by: Scalar(decibels), range: range)
    }

    /// Change the level of samples in the buffer.
    ///
    /// - Parameters:
    ///   - scalar: The amount of change given by a `Scalar` value.
    ///   - range: The range of samples affected by the change. The default is the range of all the samples in the buffer

    public func scale(by scalar: Scalar, range: SampleRange? = nil)
    {
        scale(with: Ramp(start: scalar, end: scalar), range: range)
    }
    
    /// Change the level of samples in the buffer.
    ///
    /// - Parameters:
    ///   - ramp: The amount of change over the `range` of samples
    ///   - range: The range of samples affected by the change. The default is the range of all the samples in the buffer
    
    public func scale(with ramp: Ramp, range: SampleRange? = nil)
    {
        Logger.stream.debug("AudioBuffer.scale(with:range:)")

        let scaleRange = range ?? ..<self.end

        guard
            scaleRange.start >= .zero,
            scaleRange.end <= self.end
        else { return }

        if ramp.shape == .linear
        {
            var startValue = ramp.start.rawValue
            let increment = Sample(ramp.increment(over: scaleRange.count))
            
            vDSP.formRamp(withInitialValue: &startValue,
                          multiplyingBy: rawValue[scaleRange.rawValue],
                          increment: increment,
                          result: &rawValue[scaleRange.rawValue])
        }
        else if ramp.shape == .power
        {
            let start = Sample(ramp.start)
            let length = Int(scaleRange.count)
            let delta = Sample(ramp.delta)
            
            let map = UnsafeMutableBufferPointer<Sample>.allocate(capacity: length)
            map.initialize(repeating: 0)
            defer { map.deallocate() }
            
            for i in 0 ..< length
            {
                let n = Sample(i) / Sample(length)
                let v = delta < 0 ? 1 - pow(1 - n, 0.5) : pow(n, 0.5)
                
                map[i] = start + (v * delta)
            }

            vDSP.multiply(rawValue[scaleRange.rawValue],
                          map,
                          result: &rawValue[scaleRange.rawValue])
        }
    }
    
    /// Applies the `Window`
    ///
    /// Will assert if the window size doesn't match the receiver's size.
    
    public func applyWindow(_ window: WindowBuffer)
    {
        Logger.stream.debug("AudioBuffer.applyWindow(_:)")
        
        assert(window.sampleCount == sampleCount, "AudioBuffer is different size from Window being applied to it")
                
        vDSP.multiply(rawValue, window.rawValue, result: &rawValue)
    }
    
    /// Negate the value of samples in the buffer.

    public func invert()
    {
        vDSP.negative(rawValue, result: &rawValue)
    }

    //
    // MARK: - Analyze
    //
    
    /// The maximum value of the samples in the buffer.
    
    public var maximum: Scalar
    {
        return Scalar(vDSP.maximum(rawValue))
    }
    
    /// The maximum value of a range of samples in the buffer.
    ///
    /// - Parameter range: The range of samples
    /// - Returns: The maximum value of the samples within the range in the buffer.
    
    public func maximum(of range: SampleRange) -> Scalar
    {
        let safeRange = range.clamped(to: ..<end)
        return Scalar(vDSP.maximum(rawValue[safeRange.rawValue]))
    }

    /// The minimum value of the samples in the buffer.

    public var minimum: Scalar
    {
        return Scalar(vDSP.minimum(rawValue))
    }

    /// The minimum value of a range of samples in the buffer.
    ///
    /// - Parameter range: The range of samples
    /// - Returns: The minimum value of the samples within the range in the buffer.
    
    public func minimum(of range: SampleRange) -> Scalar
    {
        let safeRange = range.clamped(to: ..<end)
        return Scalar(vDSP.minimum(rawValue[safeRange.rawValue]))
    }

    /// The maximum magnitude of the samples in the buffer.

    public var magnitude: Scalar
    {
        return Scalar(vDSP.maximumMagnitude(rawValue))
    }

    /// The maximum magnitude of a range of samples in the buffer.
    ///
    /// - Parameter range: The range of samples
    /// - Returns: The maximum magnitude of the samples within the range in the buffer.
    
    public func magnitude(of range: SampleRange) -> Scalar
    {
        let safeRange = range.clamped(to: ..<end)
        return Scalar(vDSP.maximumMagnitude(rawValue[safeRange.rawValue]))
    }

    /// The RMS value of the samples in the buffer.

    public var rms: Scalar
    {
        return Scalar(vDSP.rootMeanSquare(rawValue))
    }

    /// The RMS value of a range of samples in the buffer.
    ///
    /// - Parameter range: The range of samples
    /// - Returns: The RMS value of the samples within the range in the buffer.
    
    public func rms(of range: SampleRange) -> Scalar
    {
        let safeRange = range.clamped(to: ..<end)
        return Scalar(vDSP.rootMeanSquare(rawValue[safeRange.rawValue]))
    }

    /// The average value of the samples in the buffer.

    public var average: Scalar
    {
        return Scalar(vDSP.meanMagnitude(rawValue))
    }

    /// The average value of a range of samples in the buffer.
    ///
    /// - Parameter range: The range of samples
    /// - Returns: The average value of the samples within the range in the buffer.
    
    public func average(of range: SampleRange) -> Scalar
    {
        let safeRange = range.clamped(to: ..<end)
        return Scalar(vDSP.meanMagnitude(rawValue[safeRange.rawValue]))
    }

    /// The energy value (sum of the squares) of the samples in the buffer.

    public var energy: Scalar
    {
        return Scalar(vDSP.sumOfSquares(rawValue))
    }

    /// The energy value (sum of the squares) of a range of samples in the buffer.
    ///
    /// - Parameter range: The range of samples
    /// - Returns: The energy value of the samples within the range in the buffer.
    
    public func energy(of range: SampleRange) -> Scalar
    {
        let safeRange = range.clamped(to: ..<end)
        return Scalar(vDSP.sumOfSquares(rawValue[safeRange.rawValue]))
    }
    
    /// Returns the `SamplePosition` of the first sample ithat satisfies the given predicate.
    ///
    /// - Parameter predicate: A closure that takes a `Scalar` sample value as its argument and returns a Boolean value that indicates whether the passed sample represents a match.
    /// - Returns: The index of the first sample for which predicate returns true. If no samples in the collection satisfy the given predicate, returns nil.
    
    public func first(where predicate: (Scalar) -> Bool) -> SamplePosition?
    {
        guard let index = rawValue.firstIndex(where: { predicate(Scalar($0)) }) else { return nil }
        return SamplePosition(index)
    }

    /// Returns the `SamplePosition` of the last sample ithat satisfies the given predicate.
    ///
    /// - Parameter predicate: A closure that takes a `Scalar` sample value as its argument and returns a Boolean value that indicates whether the passed sample represents a match.
    /// - Returns: The index of the last sample for which predicate returns true. If no samples in the collection satisfy the given predicate, returns nil.
    
    public func last(where predicate: (Scalar) -> Bool) -> SamplePosition?
    {
        guard let index = rawValue.lastIndex(where: { predicate(Scalar($0)) }) else { return nil }
        return SamplePosition(index)
    }
}

public extension Data
{
    /// Creates a new data buffer with the contents of an `AudioBuffer`.
    ///
    /// - Parameter audioBuffer: The buffer to copy from.
    
    init(audioBuffer: AudioBuffer)
    {
        Logger.stream.debug("Data.init(audioBuffer:)")

        self.init(buffer: audioBuffer.rawValue)
    }
    
    /// Creates a new data buffer with the range of samples of an `AudioBuffer`.
    ///
    /// - Parameters:
    ///   - audioBuffer: The buffer to copy from.
    ///   - range: The range of samples to copy.
    
    init(audioBuffer: AudioBuffer, range: SampleRange)
    {
        Logger.stream.debug("Data.init(audioBuffer:range:)")

        let slice = audioBuffer.rawValue[range.start.rawValue ..< range.end.rawValue]
        let sliceBuffer = UnsafeBufferPointer<Sample>(rebasing: slice)
        self.init(buffer: sliceBuffer)
    }
    
    /// Creates a new data buffer with the interleaved samples of the `AudioBuffer` instances
    ///
    /// - Parameter audioBuffers: The `AudioBuffer`s to interleave
    
    init(interleaving audioBuffers: [AudioBuffer])
    {
        let frames = Int(audioBuffers.frameCount)
        let channels = Int(audioBuffers.channelCount.rawValue)
        let sampleCount = frames * channels
                
        let buffer = UnsafeMutableBufferPointer<Sample>.allocate(capacity: sampleCount)
        buffer.initialize(repeating: 0)
        defer { buffer.deallocate() }
        
        for c in 0 ..< channels
        {
            let audioBuffer = audioBuffers[c]
            
            for i in 0 ..< frames
            {
                buffer[c + (i * channels)] = audioBuffer[SamplePosition(i)]
            }
        }
        
        self.init(buffer: buffer)
    }
}

//
// MARK: - AudioBuffer array
//

public extension Array where Element == AudioBuffer
{
    /// Creates a new array of buffers (channels)
    ///
    /// - Parameters:
    ///   - dimensions: The buffer dimensions (number of frames and number of channels)
    
    init(with dimensions: AudioDimensions)
    {
        Logger.stream.debug("[AudioBuffer].init(dimensions:)")

        self.init(channelCount: dimensions.channelCount, frameCount: dimensions.frameCount)
    }

    /// Creates a new array of buffers (channels)
    ///
    /// - Parameters:
    ///   - channelCount: The number of channels (buffers) to create
    ///   - frameCount: The number of initialized samples in each channel
    
    init(channelCount: ChannelCount, frameCount: SampleCount)
    {
        Logger.stream.debug("[AudioBuffer].init(channelCount:frameCount:)")

        self = (0 ..< channelCount.rawValue).map { _ in AudioBuffer(count: frameCount) }
    }
    
    /// Creates a new array of buffers containing samples from an `AVAudioBuffer`
    ///
    /// - Parameter buffer: The buffer whose contents will be copied.
    
    init(_ buffer: AVAudioBuffer)
    {
        self.init(UnsafeMutableAudioBufferListPointer(buffer.mutableAudioBufferList))
    }
    
    /// Creates a new array of buffers containing samples from an `UnsafeMutableAudioBufferListPointer`
    ///
    /// - Parameter bufferList: The buffer list whose contents will be copied.

    init(_ bufferList: UnsafeMutableAudioBufferListPointer)
    {
        Logger.stream.debug("[AudioBuffer].init(_:)")

        self.init(channelCount: bufferList.channelCount, frameCount: bufferList.frameCount)
        
        copy(bufferList)
    }
    
    subscript(index: ChannelIndex) -> AudioBuffer
    {
        get
        {
            return self[Int(index.rawValue)]
        }

        set
        {
            self[Int(index.rawValue)] = newValue
        }
    }
    
    /// The number of `AudioBuffer` instances.
    
    var channelCount: ChannelCount
    {
        ChannelCount(count)
    }
    
    /// The index of the last `AudioBuffer`.
    
    var lastChannelIndex: ChannelIndex
    {
        ChannelIndex(count - 1)
    }

    /// The number of frames, or number of samples in each `AudioBuffer` instance.

    var frameCount: SampleCount
    {
        first?.sampleCount ?? 0
    }
    
    /// The position immediately after the last sample in the buffers.

    var frameEnd: SamplePosition
    {
        first?.end ?? 0
    }
    
    /// The dimensions of the instance
    
    var dimensions: AudioDimensions
    {
        AudioDimensions(frameCount: frameCount, channelCount: channelCount)
    }
    
    /// Erases the contents of the buffers
    
    func clear()
    {
        self.forEach { $0.clear() }
    }
    
    /// Copies samples from an `UnsafeMutableAudioBufferListPointer` into the `AudioBuffer`s
    ///
    /// The dimensions of the buffers must be identical
    ///
    /// - Parameter bufferList: The buffer list whose contents will be copied.

    func copy(_ bufferList: UnsafeMutableAudioBufferListPointer)
    {
        Logger.stream.debug("[AudioBuffer].copy(_:)")
        
        assert(channelCount == bufferList.channelCount, "Channel count mismatch")
        assert(frameCount == bufferList.frameCount, "Frame count mismatch")

        var channelIndex = 0
        for inputBufferIndex in 0 ..< bufferList.count
        {
            let numberOfChannels = Int(bufferList[inputBufferIndex].mNumberChannels)
            for inputBufferChannelIndex in 0 ..< numberOfChannels
            {
                guard let inputBufferData = bufferList[inputBufferIndex].mData else { return }

                let bufferSamples = inputBufferData
                    .advanced(by: inputBufferChannelIndex * MemoryLayout<Sample>.size)
                    .assumingMemoryBound(to: Sample.self)

                if channelIndex < count
                {
                    let input = self[channelIndex]
                    
                    vDSP.clear(&input.rawValue)

                    vDSP_vadd(bufferSamples, vDSP_Stride(numberOfChannels),
                              input.pointer, 1,
                              input.pointer, 1,
                              vDSP_Length(frameCount))

                    channelIndex += 1
                }
            }
        }
    }

    /// The maximum value of the samples in the buffers.

    var maximum: Scalar
    {
        return reduce(0) { Swift.max($0, $1.maximum) }
    }

    /// The minimum value of the samples in the buffers.

    var minimum: Scalar
    {
        return reduce(1) { Swift.min($0, $1.minimum) }
    }

    /// The maximum magnitude of the samples in the buffers.

    var magnitude: Scalar
    {
        return reduce(0) { Swift.max($0, $1.magnitude) }
    }

    /// The RMS value of the samples in the buffers.

    var rms: Scalar
    {
        let squares = reduce(0) { $0 + ($1.rms.rawValue * $1.rms.rawValue) }
        return Scalar(sqrt(squares / Sample(count)))
    }

    /// The average value of the samples in the buffers.

    var average: Scalar
    {
        let sum = reduce(0) { $0 + $1.average.rawValue }
        return Scalar(sum / Sample(count))
    }
}

extension AudioBuffer: AccelerateBuffer
{
    public typealias Element = Float
    
    public var count: Int
    {
        rawValue.count
    }
    
    public func withUnsafeBufferPointer<R>(_ body: (UnsafeBufferPointer<Sample>) throws -> R) rethrows -> R
    {
        try body(UnsafeBufferPointer(rawValue))
    }
}

extension AudioBuffer: AccelerateMutableBuffer
{
    public func withUnsafeMutableBufferPointer<R>(_ body: (inout UnsafeMutableBufferPointer<Sample>) throws -> R) rethrows -> R
    {
        try body(&rawValue)
    }
}
