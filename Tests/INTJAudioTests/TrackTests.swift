//
//  TrackTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 11/8/22.
//

@testable import INTJAudio
import XCTest

class TrackTests: XCTestCase
{
    var track: Track?
    var mockSource: MockSource?
    var mockSubTrack: MockTrack?
    var mockEffect: MockEffect?
    
    override func setUp() async throws
    {
        try await super.setUp()
        
        mockSource = MockSource()
        mockSubTrack = MockTrack()
        mockEffect = MockEffect()
        
        mockSource?.end = 96000
        
        track = Track()
        track?.addSource(try XCTUnwrap(mockSource))
        track?.subtracks.append(try XCTUnwrap(mockSubTrack))
        track?.effects.append(try XCTUnwrap(mockEffect))
    }
    
    override func tearDown() async throws
    {
        try await super.tearDown()
        
        track = nil
        mockSource = nil
        mockSubTrack = nil
        mockEffect = nil
    }
    
    func testPrepare() async throws
    {
        let track = try XCTUnwrap(track)
        let source = try XCTUnwrap(mockSource)
        let subtrack = try XCTUnwrap(mockSubTrack)
        let effect = try XCTUnwrap(mockEffect)
        
        let buffer = AudioBuffer(count: 512)
        try track.prepare(for: AudioDimensions(frameCount: 512, channelCount: 2))
        track.render(into: [buffer], for: 2048)
        
        XCTAssertEqual(source.calls.count, 2)
        XCTAssertEqual(source.calls[0].function, "prepare(for:)")
        XCTAssertEqual(source.calls[1].function, "read(into:from:to:)")

        XCTAssertEqual(subtrack.calls.count, 2)
        XCTAssertEqual(subtrack.calls[0].function, "prepare(for:)")
        XCTAssertEqual(subtrack.calls[1].function, "render(into:for:)")

        XCTAssertEqual(effect.calls.count, 2)
        XCTAssertEqual(effect.calls[0].function, "prepare(for:)")
        XCTAssertEqual(effect.calls[1].function, "process(_:for:)")
    }
}
