//
//  PolarBuffer.swift
//  INTJAudio
//
//  Created by Evan Olcott on 1/3/25.
//

import Accelerate
import Foundation

/// A collection of magnitude and phase value pairs representing the spectrum of an audio signal

open class PolarBuffer
{
    public let resolution: Resolution
    public let samplingRate: Hertz

    public var magnitudes: UnsafeMutableBufferPointer<Float> // Scalar
    public var phases: UnsafeMutableBufferPointer<Float> // Radians
    
    private let binSpacing: Hertz
    
    public init(resolution: Resolution,
                samplingRate: Hertz = .samplingRate)
    {
        Logger.stream.debug("PolarBuffer.init(resolution:samplingRate)")

        self.resolution = resolution
        self.samplingRate = samplingRate

        let count = Int(resolution.sampleCount.half)
        magnitudes = UnsafeMutableBufferPointer<Float>.allocate(capacity: count)
        phases = UnsafeMutableBufferPointer<Float>.allocate(capacity: count)
        
        magnitudes.initialize(repeating: 0)
        phases.initialize(repeating: 0)
        
        binSpacing = Hertz(Double(samplingRate) / Double(resolution.sampleCount))
    }

    public convenience init(_ complex: ComplexBuffer)
    {
        self.init(resolution: complex.resolution, samplingRate: complex.samplingRate)
        
        let count = Int(resolution.sampleCount.half)
        let splitComplex = DSPSplitComplex(complex)
        vDSP.absolute(splitComplex, result: &magnitudes)
        vDSP.divide(magnitudes, Float(count), result: &magnitudes)
        vDSP.phase(splitComplex, result: &phases)
    }
    
    public convenience init(_ other: PolarBuffer)
    {
        self.init(resolution: other.resolution,
                  samplingRate: other.samplingRate)
        
        let n = Int(resolution.sampleCount.half) * MemoryLayout<Float>.size
        memcpy(magnitudes.baseAddress!, other.magnitudes.baseAddress!, n)
        memcpy(phases.baseAddress!, other.phases.baseAddress!, n)
    }
    
    public func bin(for frequency: Hertz) -> Double
    {
        frequency.rawValue / Double(binSpacing)
    }
    
    public func frequency(for bin: Int) -> Hertz
    {
        Hertz(Double(bin) * Double(binSpacing))
    }
    
    // uses linear interpolation
    
    func magnitude(at F: Hertz) -> Float
    {
        let B = bin(for: F)
        let BS = Int(B.rounded(.down))
        let BE = Int(B.rounded(.up))
        
        let BSm = magnitudes[BS]
        let BEm = magnitudes[BE]
        
        return BSm + ((BEm - BSm) * (Float(B) - Float(BS)))
    }

    /// Returns the magnitude at the given frequency
    ///
    /// Uses a sinc function to approximate the value
    ///
    /// - Parameter F: The desired frequency
    /// - Returns: The magnitude of the given frequency as a Scalar
/*
    func magnitude(at F: Hertz) -> Float
    {
        let sincWindow = 3
        let B = Int(bin(for: F).rounded())
        let BR = max(B - sincWindow, 0) ..< min(B + sincWindow, magnitudes.count)
        let BS = Double(binSpacing)
        var output: Float = 0
        
        for b in BR
        {
            let Bf = Double(frequency(for: b))
            let x = Double.pi * (Double(F) - Bf) / BS
            let sincValue = x == 0 ? 1.0 : sin(x) / x
            
            output += magnitudes[b] * Float(sincValue)
        }
        
        return max(output, 0)
    }
 */
}
