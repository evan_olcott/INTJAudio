//
//  SpectralProcessorTests.swift
//  INTJAudio
//
//  Created by Evan Olcott on 12/22/24.
//

import Accelerate
import Foundation
@testable import INTJAudio
import Testing

struct FourierTransformTests
{
    @Test(arguments: TestingTones, TestingResolutions)
    func round_trip_transform(frequency: Hertz, resolution: Resolution) throws
    {
        // create sine wave sample
        
        let sine = AudioBuffer(tone: frequency, count: resolution.sampleCount)
        
        // do forward FFT
        
        let complexBuffer = ComplexBuffer(resolution: resolution)

        let fft = FourierTransform(resolution: resolution)
        fft.forward(sine, result: complexBuffer)
        
        // check peak bins via autospectrum
        
        let polarBuffer = PolarBuffer(complexBuffer)
        let magnitudes = polarBuffer.magnitudes

        let maxBin = polarBuffer.bin(for: frequency)
        let bin = maxBin.rounded()
        let nextBin = maxBin - bin > 0 ? bin + 1 : bin - 1
        
        var max = magnitudes.max() ?? 0
        #expect(magnitudes.firstIndex(of: max) == Int(bin))

        magnitudes[Int(bin)] = 0
        max = magnitudes.max() ?? 0
        #expect(magnitudes.firstIndex(of: max) == Int(nextBin))
        
        // do inverse FFT
        
        let transformed = AudioBuffer(count: resolution.sampleCount)
        
        fft.inverse(complexBuffer, result: transformed)
        
        // compare with original signal
        
        var difference = vDSP.subtract(sine, transformed)
        vDSP.convert(amplitude: difference, toDecibels: &difference, zeroReference: 1)
        #expect(difference.filter { $0.isFinite }.max()! < -120)
    }

}
